<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#t1">1. Make a new documentation for Django API</a>
<ul>
<li><a href="#t11">1.1. Basic env:</a></li>
<li><a href="#t12">1.2. Static files:</a></li>
<li><a href="#t13">1.3. XNAT 操作:</a>
<ul>
<li><a href="#t131">1.3.1. XNAT 生成war文件</a></li>
<li><a href="#t132">1.3.2. XNAT 部署</a></li>
<li><a href="#t133">1.3.3. 数据库操作</a></li>
</ul>
<li><a href="#t4">1.4. API docs:</a>
</li>
</ul>
</li>
</ul>
</div>
</div>


<a id="t1"></a>

# Make a new documentation for XNAT


<a id="t11"></a>

## 1.1 Basic env:

-   postgresql


<a id="t12"></a>

## 1.2 Static files:

static files path is: 

<a id="t13"></a>

## XNAT操作

<a id="t131"></a>

### 生成war文件

<a id="t132"></a>

### XNAT部署
	1. set localIp="59.110.52.133" in env.js
	2. Build/Build Artifacts -> Gradle:...war -> build
    3. 上传war文件到阿里云,例如:
		cd /home/zmt/Desktop/xnat-web/build/libs/
		scp xnat-web-1.7.3-SNAPSHOT.war zmt@59.110.52.133:~/
	4. 复制文件到指定目录下:
		sudo cp xnat-web-1.7.3-SNAPSHOT.war /home/zzh/data/apache-tomcat-7.0.75/webapps/ROOT.war
	5. webapps下查看项目是否导入，导入后会自动在webapps的ROOT下生成对应文件,查看时间是否更新
		cd /home/zzh/data/apache-tomcat-7.0.75/webapps/
		ll

<a id="t133"></a>

### 数据库操作
	xnat-data-models中的xnat.xsd，包含了大部分表，需要字段可以先在该文件中查看，具体表的数据进入本地数据库，新增表按陈老师之前给的文档进行操作即可
	进入查看：$ sudo su postgres
	进入数据库$ \psql
	进入XNAT数据库$\c xnat

 	关于xnat数据库中的表结构以及常用的表的信息整理如下:

	1. Xnat:projectdata:project（课题）的主表，主键为id，创建课题时由用户定义。
			关于project其他的信息存储在相应的表中：
				xnat:publicationResource
				xnat:abstractResource
				xnat:abstractProtocol
				xnat:investigatorData

	2. Xnat:subjectdata：Subject（受试者）主表，主键为id，形如“XNAT_S-”,系统自动生成
			一个subject可共享于其他课题，通过表xnat:projectParticipant来存储相关信息。
			Subject表与其它表的联系：
				"share"——"xnat:projectParticipant" ：记录共享课题的subject，
				"experiment" ——"xnat:subjectAssessorData"：subject对应的影像（id-id）
				"investigator" ——"xnat:investigatorData"
				"demographics"——"xnat:abstractDemographicData"

	3. Xnat: imageSessionData：影像数据库中所有的影像文件及其在文件系统中的具体位置，所属扫描类别等，主键形如“XNAT_E-”,

	4. Xnat:imageScanData： 每个影像的扫描数据（scan）

<a id="t14"></a>

## API docs:

	1. 获取所有实验及其scan信息：/images
	2. 获取某个实验某组scan下的所有图片列表：/imginfos/{IMG_ID}/{SCAN_ID}
	3. 获取图片的标注信息：/labelinfos/{IMG_ID}/{SCAN_ID}/{IMG_NAME}
	4. 插入图片的标注信息：/labelinsert（该API使用了post方法，参数定义在post函数中）
	5. 获取某个病人的信息：/subinfo/{SUBJECT_ID}
	6. 获取某个课题下该病人所对应的影像数据：/subimages/{PROJECT_ID}/{SUBJECT_ID}
	7. 获取某次实验（experiment）下所有scan信息： http://59.110.52.133:8081/data/scansinfo/{EXPERIMENT_ID}?format=json
		eg:http://59.110.52.133:8081/data/scansinfo/XNAT_E00129?format=json
		返回结果如下：
				a.XNAT_E00129可以看作一个人某一次的影像，ResultSet中count表示该次影像共有13组scan;
				b.scan具体分组信息在content的result中，每一项代表一种scan_type,可能对应多组scan;
				c.total_number表示总的dcm数，frames表示系统认为有意义的dcm数;
				d.image_uri取的是该组scan中间那张dcm。
	8. 返回单个dicom文件的相应头信息： /dicomheader/{EXPERIMENT_ID}/{SCAN_ID}/{IMG_NAME}
		eg:http://59.110.52.133:8081/data/dicomheader/XNAT_E00139/101/1.2.392.200036.9125.2.138612190166.20160718002012-101-1-cnyz6i.dcm?format=json
	9. 下载该dicom文件: /getdicom/{EXPERIMENT_ID}/{SCAN_ID}/{IMG_NAME}
