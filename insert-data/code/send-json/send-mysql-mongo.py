# -*- coding: utf-8 -*-
import os
import json
from StringIO import StringIO
import pycurl
import csv
from pinyin_convertor import pinyin
# from medknowledgedict import *
from stdFunc import *
import sys
reload(sys)
sys.setdefaultencoding('utf8')
table_key_length={ }
key_max_length={ }

retu1=[] 
pinyin_dict = pinyin() 
pinyin_dict.load_pinyin() 

useless_list = ["",u"",None,False," ","  ","   ","    ","     ","-","--","---"]

def insert_record(col,row):
    # print dop['op']
    url = 'http://1.85.37.136:9999/medknowledge/op/'
    url = 'http://202.117.54.88:9999/medknowledge/op/'
    url = 'http://aliyun:9999/medknowledge/op/'
    insert_data = {}
    for i,j in zip(col,row):
        if j not in useless_list:
            insert_data[i] = j
    # myprint(insert_data)
    print 'name',insert_data[u'名称']
    insert_data = json.dumps(insert_data)
    storage = StringIO()
    try:
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER,['Content-Type: application/json'])
        # c.setopt(pycurl.CUSTOMREQUEST,dop['op'])
        # c.setopt(pycurl.CUSTOMREQUEST,'POST')
        # c.setopt(pycurl.CUSTOMREQUEST,'POST')
        c.setopt(pycurl.CUSTOMREQUEST,'DELETE')
        c.setopt(pycurl.POST,1)
        c.setopt(pycurl.CONNECTTIMEOUT, 3) #链接超时
        c.setopt(pycurl.POSTFIELDS,insert_data)
        c.setopt(c.WRITEFUNCTION,storage.write)
        c.perform()
        c.close()
    except:
        print('已断开')
        return 2
    res = storage.getvalue()
    mywritefile('temp.html',res)
    response = json.loads(res)
    print 'res',res
    retu = response['Return']
    # print res
    return retu

def word_to_pinyin_lower(cstr):
        pinyinname = pinyin_dict.convert(string=cstr)
        pinyin_without_tone=''.join([p[:-1] for p in pinyinname]).lower()
        pinyin_first_letter=''.join([p[0] for p in pinyinname]).lower()
        return pinyin_without_tone ,pinyin_first_letter

def get_fid(xid,table):
    fid = 'f'+xid
    if table.lower() == 'medicare':
        fid = 'me' + fid[2:]
    # print xid,fid
    return fid



keyMapDict = {
        'name':'名称',
        'id':'Id',
        'ename':'英文名',
        'oname':'别名',
        'class':'分类',
        'dclass':'分类',
        'fclass':'分类',
        'icd10':'ICD10',
        'year':'年份',
        }

pureSet = set([
    u'Id',
    u'英文名',
    u'ICD10',
    ])

joinSet = set([
    u'oname',
    u'别名',
    u'一级部位',
    u'二级部位',
    u'一级科室',
    u'二级科室',
    ])

if __name__ == '__main__':
    num = 0

    video_set = set([fi.split('.')[0] for fi in os.listdir('../../video/')])
    data_path = '../../result/json/'
    os.system('clear')
    # x = raw_input('input first letter:\n\t').title()
    for folder in sorted(os.listdir(data_path)):
        # if not folder.startswith(x):
        #     continue
        if not folder.startswith('Video'):
            continue
        path = os.path.join(data_path,folder)
        for index,fi in enumerate(sorted(os.listdir(path))):
            # print fi
            if fi.split('.')[0] in video_set:
                continue
            num += 1
            print fi,num
            # continue
            d = myreadjson(os.path.join(path,fi))
            table = folder.split('_')[0].title()
            xid = d['Id']
            col_name=[]
            urow=[]
            col_name.append('Table')
            urow.append(table)
            col_name.append('Id')
            urow.append(xid)
            for l in d['Table']:
                k = l.keys()[0]
                v = l.values()[0]
                pinyin_without_tone,pinyin_first_letter=word_to_pinyin_lower(v)
                col_name.append(k)
                urow.append(v)
                if k not in pureSet:
                    col_name.append(k+'Pinyin')
                    v = pinyin_without_tone[:240] + ' '+ pinyin_first_letter[:14]
                    urow.append(v)
            s = json.dumps(d['Content'])
            col_name.append('Content')
            urow.append(s)
            ret=2    
            while(ret==2):
                # break
                ret = insert_record(col_name,urow)
            print urow[0],"Count: "+str(index)+"   Return: "+str(ret) 
