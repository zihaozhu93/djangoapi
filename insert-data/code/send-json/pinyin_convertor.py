# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 15:03:41 2015

@author: qian
"""

import os.path

class pinyin(object):
    def __init__(self, dict_file='pinyin.data'):
        self.word_dict = {}
        self.dict_file = dict_file


    def load_pinyin(self):
        if not os.path.exists(self.dict_file):
            raise IOError("File Not Found")

        with file(self.dict_file) as f_obj:
            for f_line in f_obj.readlines():
                try:
                    line = f_line.split('    ')
                    self.word_dict[line[0]] = line[1]
                except:
                    line = f_line.split('   ')
                    self.word_dict[line[0]] = line[1]


    def convert(self, string=""):
        result = []
        #print type(string)
        if not isinstance(string, unicode):
            string = string.decode("utf-8")
        #print type(string)
        
        for char in string:
            try:
                key = '%X' % ord(char)
                result.append(self.word_dict.get(key, char).split()[0][:].lower())
            except:
                result.append(char)

        return result


    def convert_str(self, string="", split=""):
        result = self.convert(string=string)
        if split == "":
            return result
        else:
            return split.join(result)
