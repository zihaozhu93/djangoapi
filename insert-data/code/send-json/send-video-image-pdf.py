# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 16:02:34 2015

@author: qian
"""
import os
import json
from StringIO import StringIO
import pycurl
import csv
import msgpack
import sys
import argparse
parser = argparse.ArgumentParser(description='PyTorch Tianchi Detector')
parser.add_argument('--path',
                    '-p',
                    metavar='path of files that needs to be sent',
                    default='',
                    help='path')
parser.add_argument('--ip',
                    '-i',
                    metavar='ip of the server',
                    default='aliyun',
                    help='ip')
parser.add_argument('--port',
                    '-o',
                    metavar='port of the server',
                    type=int,
                    default='9999',
                    help='port')
parser.add_argument('--url',
                    '-u',
                    metavar='url of the API',
                    default='/medknowledge/imageop/',
                    help='ip:port')
args = parser.parse_args()

useless_list = ["",u"",None,False," ","  ","   ","    ","     ","-","--","---"]

def insert_record(col,row):
    # url = 'http://127.0.0.1:8000/medknowledge/imageop/'
    url = 'http://{:s}:{:d}{:s}'.format(args.ip,args.port,args.url)
    insert_data = {}
    for i,j in zip(col,row):
        #if j not in useless_list:
        insert_data[i] = j
    insert_data = msgpack.dumps(insert_data)
    storage = StringIO()
    try:
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER,['Content-Type: application/json'])
        c.setopt(pycurl.CUSTOMREQUEST,'POST')
        c.setopt(pycurl.POST,1)
        c.setopt(pycurl.CONNECTTIMEOUT, 3) #链接超时
        c.setopt(pycurl.POSTFIELDS,insert_data)
        c.setopt(c.WRITEFUNCTION,storage.write)
        c.perform()
        c.close()
    except:
        return 2
    res = storage.getvalue()
    try:
        response = json.loads(res)
        print res
    except:
        with open('/home/cc/tmp/temp.html','w') as f:
            f.write(res)
        return 
    retu = response['Return']
    return retu

def send(path):
    count=0
    for index,fi in enumerate(os.listdir(path)):
        if type(fi) is str:
            fi=fi.decode('utf-8')
            #continue
            with open(path+fi.encode('utf-8'),'r') as f:
                img=f.read() 
            col_name=['Id','Content']
            urow=[fi.split('.')[0],img]
            # print urow[0]
            # continue
            count = count+1
            ret=2
            while(ret==2):
                ret = insert_record(col_name,urow)
            print urow[0],"Count: "+str(index)+"   Return: "+str(ret)
        # break

def get():
    cmd = 'curl http://127.0.0.1:8000/medknowledge/imageop/?q={"Id":"i26173","Table":"Image"}'
    os.system(cmd)

if __name__=='__main__':
    send(args.path)
    # get()
