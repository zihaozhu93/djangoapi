# -*- coding: utf-8 -*-

import os
import json
import traceback
from collections import OrderedDict 
from fuzzywuzzy import fuzz

import bs4
from bs4 import BeautifulSoup
import urllib

import sys
reload(sys)
sys.setdefaultencoding('utf-8')



################################################################################
### pre define function
def mywritejson(save_path,content):
    content = json.dumps(content,indent=4,ensure_ascii=False)
    with open(save_path,'w') as f:
        f.write(content)

def myreadjson(load_path):
    with open(load_path,'r') as f:
        return json.loads(f.read())

def mywritefile(save_path,content):
    with open(save_path,'w') as f:
        f.write(content)

def myreadfile(load_path):
    with open(load_path,'r') as f:
        return f.read()

def myprint(content):
    print json.dumps(content,indent=4,ensure_ascii=False)

def rm(fi):
    os.system('rm ' + fi)

def mystrip(s):
    return ''.join(s.split())

def mysorteddict(d):
    dordered = OrderedDict()
    for k in sorted(d.keys()):
        dordered[k] = d[k]
    return dordered

def mysorteddictfile(src,obj):
    mywritejson(obj,mysorteddict(myreadjson(src)))

def myfuzzymatch(srcs,objs,grade=80):
    matchDict = OrderedDict()
    for src in srcs:
        for obj in objs:
            value = fuzz.partial_ratio(src,obj)
            if value > grade:
                try:
                    matchDict[src].append(obj)
                except:
                    matchDict[src] = [obj]
    return matchDict

def mydumps(x):
    return json.dumps(x,indent=4,ensure_ascii=False)
################################################################################
################################################################################
################################################################################
def get_url_id(url):
    load_path = '../result/url_id_map.json'
    if not os.path.exists(load_path):
        durlid = { }
        durlid['max'] = 0
    else:
        durlid = myreadjson(load_path)
    if url not in  durlid:
        num = durlid['max']
        durlid['max'] = durlid['max'] + 1
        durlid['url_' + str(num)] = url
        durlid[url] = 'url_' + str(num)
        save_path = load_path
        mywritejson(save_path,durlid)
    return durlid[url]

def get_id_url(urlid):
    load_path = '../result/url_id_map.json'
    durlid = myreadjson(load_path)
    if urlid not in  durlid:
        return ''
    else:
        return durlid[urlid]

def get_fi_url(fi):
    urlid = fi.split('.')[0]
    load_path = '../result/url_id_map.json'
    durlid = myreadjson(load_path)
    if urlid not in  durlid:
        return ''
    else:
        return durlid[urlid]

def splitByChar(content,splitChar='：',keyLength=10):
    dnew = OrderedDict()
    key = 'temp'
    value = []
    for line in content.splitlines():
        line = line.strip()
        if len(line) == 0:
            continue
        if splitChar in line:
            numindex = line.index(splitChar)
            if numindex > keyLength:
                value.append([0,line+'\n'])
                continue
            elif len(value):
                dnew[key] = value
            key = line[:numindex].strip()
            value = []
            if len(line[numindex+1:].strip()):
                value.append([0,line[numindex+1:]+'\n'])
        else:
            value.append([0,line+'\n'])
    if len(value):
        dnew[key] = value
    return dnew

def splitByColonBracket(content,splitChar='：',keyLength=6):
    content = content.replace('【','\n')
    content = content.replace('】','：')
    return splitByChar(content,splitChar,keyLength)

def splitByBracket(content,splitChar='】',keyLength=6):
    content = content.replace('【','\n')
    return splitByChar(content,splitChar,keyLength)

def paragraph_parse_to_dict(parent_node,key_names=[],extend_names=[]):
    key_names = set(key_names)
    extend_names = set(extend_names)
    # 先扁平化，然后用h3作为key
    def get_dict(node_list):
        key = 'temp'
        value = []
        d = OrderedDict()
        for ch in node_list:
            if  type(ch) is bs4.NavigableString:
                content = ch.string.strip()
                if len(content):
                    value.append([0,content])
            elif  type(ch) is bs4.element.Comment:
                print ch
                raw_input('comment')
                content = ch.string.strip()
                if len(content):
                    value.append([0,content])
            elif ch.name in key_names:
                if len(key) and len(value):
                    d[key] = value
                key = ch.text.strip().replace(':','').replace('：','').replace('【','').replace('】','')
                # print key
                value = []
            elif ch.name in ['a']:
                text = ch.text
                href = ch.get('href')
                value.append([1,text,href]) 
            elif ch.name in ['img']:
                src = ch.get('src')
                value.append([1,src]) 
            else:
                # raw_input(ch.name)
                value.append([0,ch.text.strip()])
        d[key] = value
        # myprint(d.keys())
        return  d
    def get_node_list(parent_node):
        node_list = []
        for ch in parent_node:
            if  type(ch) is bs4.NavigableString:
                content = ch.string.strip()
                if len(content):
                    node_list.append(ch)
            elif ch.name in extend_names:
                node_list.extend(get_node_list(ch))
            else:
                node_list.append(ch)
        return node_list
    d = get_dict(get_node_list(parent_node))
    return d


def get_spans(all_content):
    spans = all_content.find_all('span') + all_content.find_all('div') 
    spans = [span for span in spans if mystrip(span.text).startswith('共有')]
    return spans

def getFullUrl(url,v='',urlpre=''):
    if 'http' in url:
        return url
    elif 'tcm' in url:
        return 'http://www.pharmnet.com.cn' + url
    elif url.startswith('index'):
        if urlpre[-1] != '/':
            url = '/' + url 
        return urlpre + url
    else:
        if urlpre[-1] != '/' and v[-1] != '/':
            url = '/' + url 
        return 'http://www.pharmnet.com.cn' + v + url

def crawl(url,file_name):
    if os.path.exists(file_name):
        return
    print url
    response = urllib.urlopen(url).read()
    mywritefile(file_name,response)
################################################################################

