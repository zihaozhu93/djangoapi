# -*- coding: utf-8 -*-
from stdFunc import *

################################################################################
#:: ycc define
data_path = '../../result/json/'
result_path = '../../result/es-json/'
if not os.path.exists(result_path):
    os.mkdir(result_path)
################################################################################
### read files
### global variables
### global functions
################################################################################

def gen_name_ids_dict(srcfold):
    idList = []
    nameIdsDict = dict()
    for fold in sorted(os.listdir(srcfold)):
        fold = os.path.join(srcfold,fold)
        for index,fi in enumerate(sorted(os.listdir(fold))):
            if 'z0000000' in fi:
                continue
            print fold,fi,index
            fi = os.path.join(fold,fi)
            d = myreadjson(fi)
            xid = fold.split('/')[-1].lower() + '_' + d['Id']
            idList.append(xid)
            d = { l.keys()[0]:l.values()[0] for l in d['Table'] }
            # print type(d)
            try:
                oname = d[u'别名']
                if type(oname) is list:
                    nameSet = set(oname)
                else:
                    oname = oname.replace(';',',')
                    oname = oname.replace('；',',')
                    oname = oname.replace(u'；',',')
                    nameSet = set(oname.split(','))
            except:
                nameSet = set()
            try:
                nameSet.add(d[u'英文名']) 
            except:
                pass
            nameSet.add(d[u'名称'])
            for name in nameSet:
                try:
                    nameIdsDict[name].add(xid) 
                except:
                    nameIdsDict[name] = set([xid])
    nameIdsDict = mysorteddict(nameIdsDict,key = lambda s:len(s.encode('utf8')))
    for name,ids in nameIdsDict.items():
        nameIdsDict[name] = sorted(ids)
    return nameIdsDict,idList

def gen_id_numlink_dict(srcfile1,srcfile2):
    idList = myreadjson(srcfile2)
    idSet = set(idList)
    idNull = set()
    idNumDict = { }
    for xid in idList:
        idNumDict[xid] = 1
    for index,line in enumerate(open(srcfile1,'r')): 
        if 'property' in line:
            continue
        if index %100000 == 0:
            print index
        line = line.strip().strip(' .')
        data = line.split()
        fid = data[0].split('/')[-2] + '_'+ data[0].split('/')[-1].replace('>','')
        tid = data[2].split('/')[-2] + '_'+  data[2].split('/')[-1].replace('>','')
        for xid in [fid,tid]:
            xid = unicode(xid)
            if xid in idSet:
                try:
                    idNumDict[xid] = idNumDict[xid] + 1 
                except:
                    idNumDict[xid] = 1
            else:
                idNull.add(xid.replace('_',','))
    return idNumDict,sorted(idNull)

def gen_name_num_dict(srcfile1,srcfile2):
    nameNumDict = { }
    nameIdsDict = myreadjson(srcfile1)
    idNumDict = myreadjson(srcfile2)
    for name,ids in nameIdsDict.items():
        if len(name) > 1:
            numList = [idNumDict[xid.encode('utf8')] for xid in ids]
            nameNumDict[name] = max(numList)
    nameNumDict = mysorteddict(nameNumDict,key = lambda s:len(s))
    return nameNumDict


def write_name_search_json(srcfile,srcfold,objfold):
    if not os.path.exists(objfold):
        os.mkdir(objfold)
    idNumDict = myreadjson(srcfile)
    vis = 1
    for fold in sorted(os.listdir(srcfold)):
        if 'Symp' in fold :
            vis = 0
        if vis:
            continue
        result_fold = os.path.join(objfold,fold)
        if not os.path.exists(result_fold):
            os.mkdir(result_fold)
        fold = os.path.join(srcfold,fold)
        for index,fi in enumerate(sorted(os.listdir(fold))):
            if 'z0000000' in fi:
                continue
            print fold,fi,index
            finew  = os.path.join(result_fold,fi)
            fi = os.path.join(fold,fi)
            dfi = myreadjson(fi)
            xid = fold.split('/')[-1].lower() + '_' + dfi['Id']
            num = idNumDict[xid]
            dfi['Num'] = num
            dfi = mysorteddict(dfi)
            content_list = []
            for d in dfi['Content']:
                k = d.keys()[0]
                v = d.values()[0]
                if k.lower() != 'overview':
                    vnew = u''
                    for l in v:
                        if type(l) is dict:
                            continue
                        if l[0] in [2,8,10,11,12,'table']:
                            continue
                        elif l[0] in [0,1,9]:
                            vnew = vnew + l[1]
                        elif l[0] in ['c']:
                            continue
                        else:
                            print fi
                            print l[0],k
                            print dfi['Name']
                            myprint(v)
                            print err
                    if len(vnew.strip()):
                        content_list.append({ k:vnew })
            dfi.pop('Content')
            dfi['Content'] = content_list
            mywritejson(finew,dfi)
    
#:: ycc end 
################################################################################

if __name__  == '__main__':

    # gen name-ids-dict
    '''
    srcfold = data_path
    nameIdsDict,idList = gen_name_ids_dict(srcfold)
    save_path = os.path.join(result_path,'name-idlist-dict.json')
    mywritejson(save_path,nameIdsDict)
    save_path = os.path.join(result_path,'id-list.json')
    mywritejson(save_path,idList)
    # ''' 

    # gen id-num-dict
    ''' 
    srcfile1 = '/home/cc/ycc/yiliao/med_kg/med-kg-baidu/result/merge.nt'
    srcfile2 = os.path.join(result_path,'id-list.json')
    print 'sensure {:s} is right'.format(srcfile1)
    idNumDict,idNull = gen_id_numlink_dict(srcfile1,srcfile2)
    save_path = os.path.join(result_path,'id-num-dict.json')
    mywritejson(save_path,idNumDict)
    save_path = os.path.join(result_path,'id-null-list.json')
    mywritejson(save_path,idNumDict)
    # ''' 

    # gen name-name-dict
    ''' 
    srcfile1 = os.path.join(result_path,'name-idlist-dict.json')
    srcfile2 = os.path.join(result_path,'id-num-dict.json')
    nameNumDict = gen_name_num_dict(srcfile1,srcfile2)
    save_path = os.path.join(result_path,'name-num-dict.json')
    mywritejson(save_path,nameNumDict)
    # ''' 


    # write name-search-json
    # ''' 
    srcfile = os.path.join(result_path,'id-num-dict.json')
    srcfold = data_path
    objfold = os.path.join(result_path,'name-search-json')
    write_name_search_json(srcfile,srcfold,objfold)
    # ''' 
