# -*- coding: utf-8 -*-
from stdFunc import *
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback

################################################################################
#:: ycc define
################################################################################
### read files
with open('/var/www/djangoapi/file/medknowledge-dict.json','r') as f:
    medknowledgeDict = json.loads(f.read())
    for cls,clsDict in medknowledgeDict.items():
        for k,v in clsDict['dict'].items():
            medknowledgeDict[cls]['dict'][v] = k
            # medknowledgeDict[cls]['dict']['-'+v] = '-'+k
keyAllMapDict = myreadjson('../../data/key-map-dict.json')
### global variables
listset = set()
tableKeysetDict = { }
keyAllSet = set()
### global functions

def get_table_keys(root_path):
    for fold in os.listdir(root_path):
        data_path = os.path.join(root_path,fold)
        for folder in sorted(os.listdir(data_path)):
            table = folder.split('_')[0].title()
            print folder.split('_')[0].title()
            for index,fi in enumerate(sorted(os.listdir(os.path.join(data_path,folder)))):
                fi = os.path.join(data_path,folder,fi)
                d = myreadjson(fi)
                for key in d:
                    key = key.strip()
                    keyAllSet.add(key)
                    if key in medknowledgeDict[table]['dict']:
                        continue
                    try:
                        tableKeysetDict[table].add(key)
                    except:
                        tableKeysetDict[table] = set([key])


def reshap_dict(d,table):
    if u'分类' not in d and u'一级分类' in d:
        print d[u'一级分类']
        if type(d[u'一级分类']) is list:
            d[u'分类'] = d[u'一级分类'][0][1]
        elif type(d[u'一级分类']) is str or type(d[u'一级分类']) is unicode:
            d[u'分类'] = d[u'一级分类']
        else:
            print err
    keyMapDict = medknowledgeDict[table]['dict']
    keyList = medknowledgeDict[table]['tablelist']
    dnew = OrderedDict()
    dnew['Id'] = d['Id']
    dnew['Name'] = d[u'名称']
    dnew['Table'] = []
    # dnew['Mongo'] = OrderedDict()
    dnew['Content'] = []
    keyTableSet = set()
    # table
    for k in keyList:
        kcn = keyMapDict[k]
        if kcn in d:
            v = d[kcn]
            if type(v) is list:
                listset.add(k)
                print table
                print k
                v = ','.join(v)
            dnew['Table'].append({ kcn:v })
            keyTableSet.add(kcn)
    # content
    if table == 'Clinicalpath':
        content_list = d['content']
        key = ''
        value = []
        for cont in content_list:
            if cont[0] == 1:
                if len(key) and len(value):
                    dnew['Content'].append({ key:value })
                key = cont[1]
                value = []
            else:
                dkind = { 2 : 9, 3 : 0, 'table': 'table'}
                kind = dkind[cont[0]]
                value.append([kind,cont[1]])
        dnew['Content'].append({ key:value })
        # myprint(dnew)
        # raw_input()
    elif table == 'Zhongyi':
        if d['Id'] == 'z0000000':
            return d
        def chg_linebreak(content_list):
            # if '4325' not in d['Id']:
            #     return content_list
            content_list_new = []
            # myprint(content_list)
            # raw_input()
            for dtemp in content_list:
                dt = {}
                for k,vs in dtemp.items():
                    vs_new = []
                    for v in vs:
                        if v[0] != 0:
                            vs_new.append(v)
                        else:
                            s = v[1]
                            s_list = s.split('\n')
                            for si in s_list:
                                if len(si.strip()):
                                    vs_new.append([0,si])
                                    vs_new.append([10])
                    dt[k] = vs_new
                content_list_new.append(dt)
            return content_list_new
        content_list = d['main']
        # myprint(d.keys())
        if 'overview' in d:
            overview = d['overview']
            content_list = [{ 'overview':overview }] + content_list
        dnew['Content'] = chg_linebreak(content_list)
        # dnew['Content'] = content_list
        # try:
        #     dnew['Content'] = chg_linebreak(content_list)
        # except:
        #     dnew['Content'] = content_list
        # myprint(d)
        # myprint(d.keys())
        # myprint(content_list)
        # raw_input()
    else:
        for kcn in medknowledgeDict[table]['contentlist']:
            if kcn not in d or 'video' == kcn or 'pdf' == kcn:
                continue
            if table == 'Symptom' and kcn == u'可能疾病':
                dpos = dict()
                dpos['PosDis'] = []
                # dnew = []
                for posDis in d[kcn]:
                    if len(posDis['did'].strip()) == 0:
                        continue
                    dtemp = { }
                    dtemp[u'可能疾病'] = { 'Id':'disease/{:s}/'.format(posDis['did']), 'Name':posDis['dname'] }
                    dtemp[u'伴随症状'] = []
                    for sym in posDis[u'伴随症状']:
                        dtemp[u'伴随症状'].append({ 'Id':'symptom/{:s}/'.format(sym['sid']), 'Name':sym['sname'] })
                    if len(dtemp[u'伴随症状']) == 0:
                        continue
                    if type(posDis[u'就诊科室']) is list:
                        dtemp[u'就诊科室'] = '，'.join(posDis[u'就诊科室'])
                    else:
                        dtemp[u'就诊科室'] = ''.join(posDis[u'就诊科室'])
                    dpos['PosDis'].append(dtemp)
                if len(dpos['PosDis']) != 0:
                    # dnew.pop('PosDis')
                    dnew['Content'].append({ kcn:[[8,dpos['PosDis']]]})
                myprint(dpos['PosDis'])
                # raw_input()
            elif table == 'Symptom' and kcn in [ u'常用检查',u'相似症状']:
                lnew = []
                if kcn == u'常用检查':
                    xid = 'lid'
                    xname = 'lname'
                else:
                    xid = 'sid'
                    xname = 'sname'
                for lab in d[kcn]:
                    if len(lab[xid].strip()) != 0:
                        lnew.append([1,lab[xname],'lab/{:s}/'.format(lab[xid])])
                        lnew.append([0,'\n'])
                if len(lnew):
                    dnew['Content'].append({ kcn:lnew })
                    # myprint(lnew)
                    # raw_input()
            elif table in ['Evidence','Medicare' ]: 
                if type(d[kcn]) is int:
                    d[kcn] = str(d[kcn])
                if len(d[kcn].strip()) :
                    dnew['Content'].append({ kcn:[[0,d[kcn]+'<br>']] })
            elif table in ['Research' ]: 
                dnew['Content'].append({ kcn:d[kcn] })
                vs = d[kcn]
                '''
                for i in range(len(vs)):
                    if vs[i][0] == 0:
                        vs[i][1] = vs[i][1] + '<br>'
                    else:
                        print vs[i][0]
                        # print err
                    dnew['Content'].append({ kcn:vs  })
                # '''
            elif kcn == 'ICD9':
                if type(d[kcn]) is list:
                    d[kcn] = [[0,', '.join([x for x in d[kcn] if len(x.strip(' .')) ])]]
                    # dnew['Content'].append({ kcn:d[kcn] })
            else:
                dnew['Content'].append({ kcn:d[kcn] })
    # return dnew
    contentnew = []
    for dtemp in dnew['Content']:
        # break
        k,v  = dtemp.items()[0]
        # print k
        if type(v) is list:
            vnew = []
            contentnew.append({ k:v })
        elif type(v) in [str,unicode]:
            contentnew.append({ k:[[0,v]] })
        elif type(v) is dict and k == 'overview':
            dnew['Overview'] = v
        else:
            myprint(dtemp)
            print type(v)
            # print err
    dnew['Content'] = contentnew
    return dnew




def chg_fmt(root_path,save_path):
    for fold in os.listdir(root_path):
        # print fold
        data_path = os.path.join(root_path,fold)
        for folder in sorted(os.listdir(data_path)):
            # print '\t',folder
            if 'Zhongyi' not in folder.title():
                continue
                pass
            # print folder
            table = folder.split('_')[0].title()
            result_path = os.path.join(save_path,table)
            if not os.path.exists(result_path):
                os.mkdir(result_path)
            print table
            # keyMapDict = medknowledgeDict[table]
            for index,fi in enumerate(sorted(os.listdir(os.path.join(data_path,folder)))):
                if index > 10 and 's12069' not in fi:
                    # break
                    # continue
                    pass
                print folder,index
                # print 'llll'
                xid = fi.split('.')[0]
                fi = os.path.join(data_path,folder,fi)
                d = myreadjson(fi)
                d[u'id'] = xid
                dcn = { }
                for key,value in d.items():
                    try:
                        value = json.loads(value)
                    except:
                        pass
                    key = key.strip()
                    if key not in keyAllMapDict:
                        raw_input(table+'  '+key)
                        continue
                    keycn = keyAllMapDict[key]
                    dcn[keycn] = value
                d = reshap_dict(dcn,table)
                result_path = os.path.join(save_path,table,fi.split('/')[-1])
                # raw_input(result_path)
                mywritejson(result_path,d)


################################################################################
#:: ycc end 
################################################################################

if __name__  == '__main__':
    os.system('clear')
    print 'main start'
    data_path = '../../json/'
    save_path = '../../result/json/'
    # get_table_keys(data_path)
    chg_fmt(data_path,save_path)
    myprint(sorted(listset))
    for k,v in tableKeysetDict.items() :
        tableKeysetDict[k] = [keyAllMapDict[key] for key in sorted(v)]
    save_path = '../../result/content-keylist-dict.json'
    mywritejson(save_path,tableKeysetDict)

    '''
    # myprint(tableKeysetDict)
    save_path = '../../result/key-map-dict.json'
    keyDict = { k:k for k in keyAllSet }
    keyDict = mysorteddict(keyDict)
    mywritejson(save_path,keyDict)
    '''
        




