#########################################################################
# File Name: run_django.sh
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: 2017年10月17日 星期二 09时38分00秒
#########################################################################
#!/bin/bash

# 在 run 这个 djangoapi 之前应该先去按照 /djangoapi/readme.md 设置好相应的数据库
# 参数, 包括mysql里的database: yiliao, mysql和mongo的路径等等

# source 对应的 virtualenv
source ~/env/djangoapi-env/bin/activate

# mysql数据库改变后,要做如下操作
cd ../djangoapi
python manage.py makemigrations
python manage.py migration

# 启动 django
python manage.py runserver
