#########################################################################
# File Name: build_mysql_database.sh
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: 2017年10月17日 星期二 09时32分33秒
#########################################################################
#!/bin/bash

# 安装常见工具
sudo apt-get install git
sudo apt-get install libcurl4-openssl-dev


# 安装pip
sudo apt-get install python-pip

# 配置pip, 换成豆瓣源安装软件更快
cp -r src/.pip ~/

# 安装 mysql-server-5.6
sudo apt-get install mysql-server-5.6
# root 密码设置为 root
sudo apt-get install libmysqlclient-dev
sudo apt-get install python-dev

# 安装 mongdb-3.4
# mongodb 安装网址： https://docs.mongodb.com/manual/administration/install-on-linux/
# mongodb 安装网址： https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
# 导入 mongodb public gpg key
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
# 创建mongodb list 文件
# Ubuntu14.04
# echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
# Ubuntu16.04
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
# reload local package database
sudo apt-get update
# install the Mongodb
sudo apt-get install -y mongodb-org


# 这是一个非常简单的野路子安装方式，推荐上面的官网安装方式
# 参考 http://blog.csdn.net/wlzx120/article/details/52301799
# 下载方式
# if [ ! -f "mongodb-linux-x86_64-3.2.9.tgz" ]; then
# 	curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.9.tgz
# fi
# 解压缩压缩包
# if [ ! -d "mongodb-linux-x86_64-3.2.9" ]; then
# 	tar zxvf mongodb-linux-x86_64-3.2.9.tgz
# fi
# 创建数据库文件夹与日志文件、配置文件：
# if [ ! -d "/usr/local/server/mongodb/data" ]; then
# 	sudo mkdir -p  /usr/local/server/mongodb/data
# 	sudo touch /usr/local/server/mongodb/mongod.log
# 	sudo touch /usr/local/server/mongodb/mongodb.conf
# 	# 将mongodb移动到/usr/local/server/mongdb文件夹：
# 	sudo mv mongodb-linux-x86_64-3.2.9/* /usr/local/server/mongodb/
# fi


# 安装 rdf3x

# 安装 elasticseach
# 配置java环境
# 下载es 解压

# 安装 virtual-env
sudo apt-get install python-virtualenv


# 创建 virtual-env 环境,source进入虚拟环境,安装python包
# 注意下面 env  djangoapi-env 两个文件夹,可以自定义
cd ~/
if [ ! -d "env" ]; then
	mkdir env
fi
cd env
if [ ! -d "django-env" ]; then
	virtualenv djangoapi-env
fi
source djangoapi-env/bin/activate

# 安装 djaongo-1.8.6, 以及一系列其他包
pip install django==1.8.6
pip install django-cors-headers
pip install mysql-python
pip install mongoengine
pip install pycurl
pip install msgpack-python
pip install pyjwt
pip install elasticsearch

