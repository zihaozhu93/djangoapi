# coding=utf8
#########################################################################
# File Name: stats.sh
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue Oct 24 09:58:42 2017
#########################################################################

import os
import py_op
import traceback


"""
此文件用于做知识图谱数据处理的基本操作
"""

def get_sub_pre_obj(line):
    """
    此函数用于分割主谓宾
    输入：nt文件某一行
    输出：主语，谓语，宾语
          若报错返回None
    """
    data = line.strip('\n .').split()
    if len(data) == 3:
        return data
    elif len(data) > 3:
        sub = data[0]
        pre = data[1]
        obj = line.replace(sub,'').replace(pre,'').strip().strip(' .')
        return [sub, pre, obj]
    else:
        return

def get_node_type_id(node):
    """
    此函数用于分割主谓宾
    输入：node的网址
    输出：返回 node_type, node_id
          若报错返回None
    """
    try:
        node_type, relation_type = node.strip('/>').split('/')[-2:]
        return [node_type, relation_type]
    except:
        print node
        node_type, relation_type = node.strip('/>').split('/')[-2:]
        return [node_type, relation_type]
        return

def delete_repeate_line(src_nt, obj_nt=None):
    """
    此函数用于去除nt文件里的重复行
    输入：源文件，目标文件
    输出：无重复的nt文件
          若报错返回None
    """
    print '删除重复行'
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    line_set = set()
    line_list = list()
    for line in open(src_nt,'r'):
        line_space = ' '.join(line.split())
        if line_space not in line_set:
            line_set.add(line_space)
            line_list.append(line)
    if obj_nt:
        wf = open(obj_nt,'w')
        for line in line_list:
            wf.write(line)
        wf.close()
    return line_list

def delete_self_relation(src_nt, obj_nt=None):
    """
    此函数用于去除nt文件里的自相连边
    输入：源文件，目标文件
    输出：无重复的nt文件
          若报错返回None
    """
    print '删除自相连边'
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    line_list = list()
    for line in open(src_nt,'r'):
        sub,pre,obj = get_sub_pre_obj(line)
        if sub != obj:
            line_list.append(line)
    if obj_nt:
        wf = open(obj_nt,'w')
        for line in line_list:
            wf.write(line)
        wf.close()
    return line_list


def add_first_relation(src_nt,obj_nt=None):
    """
    此函数用于增加以及关系(科室，部位)：
    例如有的疾病的科室是二级科室，则疾病也属于对应的以及科室
    增加边之后会产生重复边，注意用 delete_repeate_line 函数
    """
    print '增加一级关系'
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    line_list = []
    for line in open(src_nt,'r'):
        line_list.append(line)
        sub, pre, obj = get_sub_pre_obj(line)
        if 'SecDep' in pre:
            _, sec_dp_id = get_node_type_id(obj)
            fir_dp_id = sec_dp_id[:-2]+'00'
            add_line = line.replace('SecDep','FirDep').replace(sec_dp_id,fir_dp_id)
            line_list.append(add_line)
        elif 'SecBody' in pre:
            _, sec_bp_id = get_node_type_id(obj)
            fir_bp_id = sec_bp_id[:-2]+'00'
            add_line = line.replace('SecBody','FirBody').replace(sec_bp_id,fir_bp_id)
            line_list.append(add_line)
    if obj_nt:
        wf = open(obj_nt,'w')
        for line in line_list:
            wf.write(line)
        wf.close()
    return line_list

def generate_sub_graph(src_nt, node_type_list, obj_nt=None, property_list=['type','name'],pre_relation_list=None):
    """
    此函数用于生成子图
    输入：
        src_nt: 原nt文件
        obj_nt: 目标nt文件
        node_type_list: 需要保留的节点种类列表, 例如 ['disease']
        property_list: 需要保留的属性种类列表，例如['type','name']
    """
    print '生成子图'
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    line_list = []
    for i,line in enumerate(open(src_nt,'r')):
        sub, pre, obj = get_sub_pre_obj(line)
        pre_type, pre_relation = get_node_type_id(pre)
        if pre_type == 'property':
            if pre_relation in property_list:
                sub_type, _ = get_node_type_id(sub)
                if sub_type in node_type_list:
                    line_list.append(line)
        else:
            if pre_relation_list:
                if pre_relation not in pre_relation_list:
                    continue
            sub_type, _ = get_node_type_id(sub)
            obj_type, _ = get_node_type_id(obj)
            if sub_type in node_type_list and obj_type in node_type_list:
                line_list.append(line)
    if obj_nt:
        wf = open(obj_nt,'w')
        for line in line_list:
            wf.write(line)
        wf.close()
    return line_list

def add_back_relation(relation_dict):
    """
    此函数用于增加反向边
    例如有的A疾病的可能症状是B症状，那B症状的可能疾病就是A疾病
    同时保存 A->B B->A 的关系
    """

def delete_back_relation(relation_dict):
    """
    此函数用于删除反向边
    例如有的A疾病的可能症状是B症状，那B症状的可能疾病就是A疾病
    仅仅保存 A->B 的关系
    """

def check_id_name(id,name):
    if id_name_dict[id] == name:
        return True
    else:
        return False


def generate_id_name_dict():
    id_name_dict = { }
    for line in open('../data/initial_data/merge.nt','r'):
        if 'property/name' in line:
            sub, _, obj = get_sub_pre_obj(line)
            _, id = get_node_type_id(sub)
            name = obj.replace('"','')
            id_name_dict[id] = name
    py_op.mywritejson('../data/middle_data/id_name_dict.json',id_name_dict)

def generate_name_id_dict():
    id_name_dict = py_op.myreadjson('../data/middle_data/id_name_dict.json')
    name_id_dict = { }
    for id,name in id_name_dict.items():
        try:
            name_id_dict[name].append(id)
            # 查重
            type_list = [id[0] for id in name_id_dict[name]]
            if len(set(type_list)) < len(type_list):
                raise NameError('有问题')
        except:
            name_id_dict[name] = [id]
    py_op.mywritejson('../data/middle_data/name_id_dict.json', name_id_dict)

def generate_line(fid,sid):
    if fid.startswith('s') and sid.startswith('b'):
        sub = '<http://xianjiaotong.edu/symptom/{:s}>'.format(fid)
        obj = '<http://xianjiaotong.edu/bodypart/{:s}>'.format(sid)
        if obj[-2:] == '00':
            pre = '<http://xianjiaotong.edu/symptom/SymFirBody>'
        else:
            pre = '<http://xianjiaotong.edu/symptom/SymSecBody>'
        return '{:s}\t{:s}\t{:s} .\n'.format(sub,pre,obj)
    elif  fid.startswith('s') and sid.startswith('b'):
        pass
    return


################################################################################

try:
    if not os.path.exists('../data/middle_data/id_name_dict.json'):
        generate_id_name_dict()
    id_name_dict = py_op.myreadjson('../data/middle_data/id_name_dict.json')

    if not os.path.exists('../data/middle_data/name_id_dict.json'):
        generate_name_id_dict()
    name_id_dict = py_op.myreadjson('../data/middle_data/name_id_dict.json')
except:
    pass
