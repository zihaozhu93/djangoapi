# coding=utf8
#########################################################################
# File Name: stats.sh
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue Oct 24 09:58:42 2017
#########################################################################

import os
import kg_op, py_op

"""
此文件用于做知识图谱数据的统计信息,包含多种统计函数
"""

def get_connected_nodes(src_nt, different_type=False):
    """
    此函数用于找到所有连通子图
    """
    def get_root(node_root_dict,node):
        if node not in node_root_dict:
            node_root_dict[node] = node
            return node
        node_path_set = set()
        while node != node_root_dict[node]:
            node_path_set.add(node)
            node = node_root_dict[node]
        for n in node_path_set:
            node_root_dict[n] = node
        return node
    node_root_dict = dict()
    node_set = set()
    for line in open(src_nt):
        subject, predicate, object = kg_op.get_sub_pre_obj(line)
        sub_type, sub_id = kg_op.get_node_type_id(subject)
        node_set.add(sub_id)
        if 'property' not in predicate:
            obj_type, obj_id = kg_op.get_node_type_id(object)
            node_set.add(obj_id)
            if different_type:
                # 必须为不等类型节点
                if sub_type == obj_type:
                    continue
            obj_root = get_root(node_root_dict,obj_id)
            sub_root = get_root(node_root_dict,sub_id)
            node_root_dict[obj_root] = sub_root

    root_node_dict = dict()
    for node in node_set:
        root = get_root(node_root_dict,node)
        try:
            root_node_dict[root].add(node)
        except KeyError:
            root_node_dict[root] = set([node])
    print '一共有{:d}个节点'.format(len(node_set))
    print '一共有{:d}个连通子图'.format(len(root_node_dict))
    return root_node_dict.values()


def find_isolated_nodes(src_nt, key_type_list=None,different_type=False):
    """
    此函数用于找到知识图谱中所有孤立点。
    输入：nt文件路径
    输出：所有实体列表，孤立实体列表
          若报错返回None
    eg: 
        >>> _, isolated_node_set = find_isolated_nodes('data/merge.nt')
        >>> print isolated_node_set
        ['<http://xianjiaotong.edu/symptom/s20001>']
    """
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return

    if key_type_list is None:
        # 直接孤立点
        node_set = set()
        not_isolated_node_set = set()
        for i,line in enumerate(open(src_nt)):
            line = line.strip()
            subject, predicate, object = line.split()[:3]
            if 'property' in predicate:
                node_set.add(subject)
            else:
                node_set.add(subject)
                node_set.add(object)
                not_isolated_node_set.add(subject)
                not_isolated_node_set.add(object)
        isolated_node_set = node_set - not_isolated_node_set
        node_set = set([kg_op.get_node_type_id(node)[1] for node in node_set])
        isolated_node_set = set([kg_op.get_node_type_id(node)[1] for node in isolated_node_set])
        return node_set, isolated_node_set
    else:
        # 以关键点为中心找孤立点
        node_set = set()
        key_nodes, isolated_node_set = find_key_nodes(src_nt,key_type_list)
        connected_nodes_list = get_connected_nodes(src_nt,different_type)
        for connected_nodes in connected_nodes_list:
            node_set = node_set | connected_nodes
            if len(connected_nodes & key_nodes)==0:
                isolated_node_set = isolated_node_set | connected_nodes
        return node_set,isolated_node_set


def find_key_nodes(src_nt, key_type_list):
    """
    此函数用于找到知识图谱中的核心点
    核心点: 两个不同的核心类型节点有链接, 那这两个节点都为核心点
    """
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return

    # 找到关键点
    node_set = set()
    key_nodes = set()
    for i,line in enumerate(open(src_nt)):
        line = line.strip()
        subject, predicate, object = kg_op.get_sub_pre_obj(line)
        sub_type, sub_id = kg_op.get_node_type_id(subject)
        if sub_type in key_type_list:
            node_set.add(sub_id)
        if 'property' not in predicate:
            obj_type, obj_id = kg_op.get_node_type_id(object)
            if obj_type in key_type_list:
                node_set.add(obj_id)
            if sub_type != obj_type and sub_type in key_type_list and obj_type in key_type_list:
                key_nodes.add(sub_id)
                key_nodes.add(obj_id)
    print '一共{:d}个关键点'.format(len(key_nodes))
    isolated_node_set = node_set - key_nodes
    print '一共{:d}个非关键点'.format(len(isolated_node_set))
    return key_nodes, isolated_node_set


def relation_tree(src_nt):
    """
    此函数用于生成知识图谱中的关系结构
    输入：nt文件路径
    输出：结构树，所有的关系按照重要性排序
          若报错返回None
    """
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    relation_dict = {
            'node': { },
            'property': { },
            'relation': { }
            }
    relation_set = set()
    for i,line in enumerate(open(src_nt)):
        line = line.strip()
        subject, predicate, object = line.split()[:3]
        node_type, relation_type = predicate.strip('/>').split('/')[-2:]
        relation_set.add(relation_type)
        if node_type == 'property':
            try:
                relation_dict[node_type][relation_type] += 1
            except KeyError:
                relation_dict[node_type][relation_type]  = 1
            if relation_type == 'type':
                try:
                    relation_dict['node'][object.strip('"')] += 1
                except KeyError:
                    relation_dict['node'][object.strip('"')]  = 1
        else:
            if node_type not in relation_dict['relation']:
                relation_dict['relation'][node_type] = { }
            try:
                relation_dict['relation'][node_type][relation_type] += 1
            except KeyError:
                relation_dict['relation'][node_type][relation_type]  = 1
    # 排序
    # relation_dict['property'] = sorted( relation_dict['property'] )
    # for k,v in relation_dict['relation'].items():
    #     relation_dict['relation'][k] = sorted(v)
    # print len(relation_set)
    return relation_dict

def department_tree(src_nt):
    """
    此函数用于生成科室结构
    """
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    relation_tree = dict()
    id_name_dict = dict()
    for line in open(src_nt):
        sub,pre,obj = kg_op.get_sub_pre_obj(line)
        if 'property/name' in pre and 'department' in sub:
            node_type, node_id = kg_op.get_node_type_id(sub)
            id_name_dict[node_id] = obj.replace('"','')
    for id in sorted(id_name_dict.keys()):
        if id.endswith('00'):
            # 一级科室
            name = id_name_dict[id]
            relation_tree[name] = []
        else:
            # 二级科室
            parent_id = id[:-2] + '00'
            parent_name = id_name_dict[parent_id]
            name = id_name_dict[id]
            relation_tree[parent_name].append(name)
    return relation_tree

def bodypart_tree(src_nt, use_name=False):
    """
    此函数用于生成部位结构
    """
    def get_key(id,name):
        if use_name:
            return name + '_' + id
        else:
            return id
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    relation_tree = dict()
    id_name_dict = dict()
    for line in open(src_nt):
        sub,pre,obj = kg_op.get_sub_pre_obj(line)
        if 'property/name' in pre and 'bodypart' in sub:
            node_type, node_id = kg_op.get_node_type_id(sub)
            id_name_dict[node_id] = obj.replace('"','')
    for id in sorted(id_name_dict.keys()):
        if id.endswith('00'):
            # 一级部位
            name = id_name_dict[id]
            key = get_key(id,name)
            relation_tree[key] = []
        else:
            # 二级部位
            parent_id = id[:-2] + '00'
            parent_name = id_name_dict[parent_id]
            # key = parent_name + '_' + parent_id
            key = get_key(parent_id,parent_name)
            name = id_name_dict[id]
            # relation_tree[key].append(name+'_'+id)
            relation_tree[key].append(get_key(id,name))
    return relation_tree

def stats_sub_obj_tree(src_nt, key_type, val_type, reverse=False):
    """
    此函数统计两种类型的关系结构
    例如返回 疾病->科室 的dict
    """
    if not os.path.exists(src_nt):
        print '没有此文件', src_nt
        return
    map_dict = {
            'linked_nodes':{ }, # 有关系的节点
            'single_nodes':[]   # 无关系的节点
            }
    key_set = set()
    link_dict = { }
    for line in open(src_nt):
	if 'property' in line:
	    continue
        sub,pre,obj = kg_op.get_sub_pre_obj(line)
        fir_type, fir_id = kg_op.get_node_type_id(sub)
        trd_type, trd_id = kg_op.get_node_type_id(obj)
        if reverse:
            fir_type, trd_type = py_op.swap(fir_type,trd_type)
            fir_id, trd_id= py_op.swap(fir_id,trd_id)
        if fir_type == key_type and trd_type == val_type:
            key_set.add(fir_id)
            try:
                link_dict[fir_id].append(trd_id)
            except KeyError:
                link_dict[fir_id] = [trd_id]
    map_dict['linked_nodes'] = link_dict
    map_dict['single_nodes'] = sorted(key_set - set(link_dict))
    return map_dict

def stats_obj_sub_tree(src_nt, key_type, val_type):
    """
    此函数统计两种类型的关系结构
    例如返回 疾病->科室 的dict
    """
    return stats_sub_obj_tree(src_nt, key_type, val_type, True)

def stats_sub_obj_pre_tree(src_nt, sub_type_list, obj_type_list, pre_name_list=None,use_pre_as_key=True):
    '''
    生成节点关系映射文件
    '''
    sub_obj_pre_dict= dict()
    for line in open(src_nt):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        pre_type, pre_name = kg_op.get_node_type_id(pre)
        if pre_type != u'property':
            if pre_name_list:
                if pre_name not in pre_name_list:
                    continue
            sub_type, sub_id = kg_op.get_node_type_id(sub)
            obj_type, obj_id = kg_op.get_node_type_id(obj)
            if sub_type not in sub_type_list or obj_type not in obj_type_list:
                continue
            if sub_type not in sub_obj_pre_dict:
                sub_obj_pre_dict[sub_type] = dict()
            if obj_type not in sub_obj_pre_dict[sub_type]:
                sub_obj_pre_dict[sub_type][obj_type] = dict()
            if not use_pre_as_key:
                pre_name = '_'
            if pre_name not in sub_obj_pre_dict[sub_type][obj_type]:
                sub_obj_pre_dict[sub_type][obj_type][pre_name] = dict()
            if sub_id not in sub_obj_pre_dict[sub_type][obj_type][pre_name]:
                sub_obj_pre_dict[sub_type][obj_type][pre_name][sub_id] = set()
            sub_obj_pre_dict[sub_type][obj_type][pre_name][sub_id].add(obj_id)
    for s in sub_obj_pre_dict:
        for o in sub_obj_pre_dict[s]:
            for p in sub_obj_pre_dict[s][o]:
                for n in sub_obj_pre_dict[s][o][p]:
                    sub_obj_pre_dict[s][o][p][n] = sorted(set(sub_obj_pre_dict[s][o][p][n]))
    return sub_obj_pre_dict

def stats_id_name_dict(src_nt, sub_type_list=None):
    '''
    生成 id_name_dict 文件
    '''
    id_name_dict = dict()
    for line in open(src_nt):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        pre_type, pre_name = kg_op.get_node_type_id(pre)
        if pre_type == u'property' and pre_name == u'name':
            sub_type, sub_id = kg_op.get_node_type_id(sub)
            if sub_type_list:
                if sub_type not in sub_type_list:
                    continue
            name = obj.strip('"')
            id_name_dict[sub_id] = name
    return id_name_dict

def stats_id_degree_dict(src_nt):
    '''
    生成节点degree映射文件
    '''
    id_degree_dict = dict()
    for line in open(src_nt):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        if 'property' not in pre:
            sub_type, sub_id = kg_op.get_node_type_id(sub)
            obj_type, obj_id = kg_op.get_node_type_id(obj)
            try:
                id_degree_dict[sub_id] += 1
            except:
                id_degree_dict[sub_id]  = 1
            try:
                id_degree_dict[obj_id] += 1
            except:
                id_degree_dict[obj_id]  = 1
    return id_degree_dict

