# coding=utf8
#########################################################################
# File Name: merge_supernode.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue Oct 24 16:17:32 2017
#########################################################################
'''
这个文件主要关于
1. 生成supernode（疾病，症状）的csv文件
2. 处理csv文件，产生新的nt文件
'''

import re
import os
from libs import stats, kg_op, py_op
import json

def get_unique_name(name):
    name = unicode(name)
    name = ''.join(name.replace('，',',').split())
    if len(name.split('（')) > 2:
        print err
    if name[-1] == '）':
        name = name.split('（')[0]
    return name

def generate_disease_symptom_relation_dict_from_nt(src_nt,obj_dir):
    '''
    此函数用于生成疾病自诊相关的json文件
    '''
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)

    # 获取症状的可能疾病
    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['symptom'],['disease'],['SymPosDis'],False)
    symptom_disease_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 症状-疾病'.format(len(symptom_disease_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_disease_dict.json'),symptom_disease_dict)

    # 反向推出疾病的可能症状
    disease_symptom_dict = dict()
    for s,ds in symptom_disease_dict.items():
        for d in ds:
            try:
                disease_symptom_dict[d].append(s)
            except:
                disease_symptom_dict[d] = [s]
    print '一共有{:d}种 疾病-症状'.format(len(disease_symptom_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_symptom_dict.json'),disease_symptom_dict)

    # 症状的部位关系
    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['symptom'],['bodypart'],['SympFirBody','SymSecBody'],False)
    symptom_bodypart_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 症状-部位'.format(len(symptom_bodypart_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_bodypart_dict.json'),symptom_bodypart_dict)

    # 反向推出部位的症状关系
    bodypart_symptom_dict = dict()
    for s,bs in symptom_bodypart_dict.items():
        for b in bs:
            try:
                bodypart_symptom_dict[b].append(s)
            except:
                bodypart_symptom_dict[b] = [s]
    print '一共有{:d}种 部位-症状'.format(len(bodypart_symptom_dict))
    py_op.mywritejson(os.path.join(obj_dir,'bodypart_symptom_dict.json'),bodypart_symptom_dict)

    # 疾病的科室关系
    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['disease'],['department'],['DisFirDep','DisSecDep'],False)
    disease_department_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 疾病-科室'.format(len(disease_department_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_department_dict.json'),disease_department_dict)

    # 反向推出科室的疾病关系
    department_disease_dict = dict()
    for d,dps in disease_department_dict.items():
        for dp in dps:
            try:
                department_disease_dict[dp].append(d)
            except:
                department_disease_dict[dp] = [d]
    print '一共有{:d}种 科室-疾病'.format(len(department_disease_dict))
    py_op.mywritejson(os.path.join(obj_dir,'department_disease_dict.json'),department_disease_dict)

def get_kg_json_from_nt(src_nt,obj_dir):
    '''
    此函数用于从nt文件生成关系文件的json格式
    '''
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    id_name_dict = stats.stats_id_name_dict(src_nt)
    symptom_id_name_dict = {k:v for k,v in id_name_dict.items() if k.startswith('s')}
    bodypart_id_name_dict = {k:v for k,v in id_name_dict.items() if k.startswith('b')}
    department_id_name_dict = {k:v for k,v in id_name_dict.items() if k.startswith('dp')}
    disease_id_name_dict = {k:v for k,v in id_name_dict.items() if k.startswith('d') and not k.startswith('dp')}
    print '有{:d}个症状'.format(len(symptom_id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_id_name_dict.json'), symptom_id_name_dict)
    print '有{:d}个疾病'.format(len(disease_id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_id_name_dict.json'), disease_id_name_dict)
    print '有{:d}个科室'.format(len(department_id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'department_id_name_dict.json'), department_id_name_dict)
    print '有{:d}个部位'.format(len(bodypart_id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'bodypart_id_name_dict.json'), bodypart_id_name_dict)
    print '有{:d}个节点'.format(len(id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'id_name_dict.json'), id_name_dict)

    
    type_name_id_dict = dict()
    type_name_id_dict['repeat'] = set()
    for id,name in id_name_dict.items():
        id_type = id[0]
        if id_type not in type_name_id_dict:
            type_name_id_dict[id_type] = dict()
        name = get_unique_name(name)
        if name in type_name_id_dict[id_type]:
            type_name_id_dict[id_type][name].add(id)
            for rid in type_name_id_dict[id_type][name]:
                type_name_id_dict['repeat'].add(rid)
        else:
            type_name_id_dict[id_type][name] = set([id])
    type_name_id_dict['repeat'] = list( type_name_id_dict['repeat'])
    for t,d in type_name_id_dict.items():
        if t != 'repeat':
            for n,ids in d.items():
                type_name_id_dict[t][n] = list(ids)
    py_op.mywritejson(os.path.join(obj_dir,'type_name_id_dict.json'), type_name_id_dict)

    print '\n节点间关系'
    generate_disease_symptom_relation_dict_from_nt(src_nt,obj_dir)


def get_supernode_dict_json(src_dir, obj_json, node_type):
    '''
    此函数用于将标注的csv文件转为json格式
    '''
    def merge_same_name_node_with_different_department(node_dict):
        type_name_id_dict = py_op.myreadjson(os.path.join(obj_dir,'type_name_id_dict.json'))
        id_name_dict = py_op.myreadjson(os.path.join(obj_dir,'id_name_dict.json'))
        type_name_id_dict['repeat'] = set(type_name_id_dict['repeat'])
        id_key_dict = { key.split('_')[0]:key for key in node_dict }
        supernode_keys = node_dict.keys()
        for supernode in supernode_keys:
            if supernode in node_dict:
                super_id, super_name = supernode.split('_')
                # 合并supernode
                if super_id in type_name_id_dict['repeat']:
                    name = get_unique_name(super_name)
                    ids = type_name_id_dict[super_id[0]][name]
                    tmp_dict = dict()
                    for id in ids:
                        if id in id_key_dict:
                            tmp_dict.update(node_dict[id_key_dict[id]])
                            node_dict.pop(id_key_dict[id])
                    node_dict[supernode] = tmp_dict
                # 增加subnode
                subnode_keys = node_dict[supernode].keys()
                for sub_id in subnode_keys:
                    if sub_id in type_name_id_dict['repeat']:
                        name = get_unique_name(id_name_dict[sub_id])
                        ids = type_name_id_dict[super_id[0]][name]
                        for id in ids:
                            node_dict[supernode][id] = name
        return node_dict
    max_num = 70000
    node_dict = dict()
    print '#\n#    有问题的节点:\n#'
    obj_dir = '/'.join(obj_json.split('/')[:-1])
    id_name_dict = py_op.myreadjson(os.path.join(obj_dir,'id_name_dict.json'))
    name_file_dict = dict()
    for src_csv in os.listdir(src_dir):
        is_new_super_node = True
        for i,line in enumerate(open(os.path.join(src_dir, src_csv),'r')):
            line = line.strip()
            # line = line.replace('，',',')
            if '，' in line:
                print '# ',line.strip()
                # print('\n{:s}\t{:d}行：\n此文件超过三列'.format(os.path.join(src_dir,src_csv),i))
            if len(line.replace(',','').strip()) == 0:
                continue
            data = [x.strip() for x in line.split(',')]
            # 检测多余的数据
            if len(''.join(data[3:])):
                # raise NameError('\n{:s}\t{:d}行：\n此文件超过三列'.format(os.path.join(src_dir,src_csv),i))
                print '# ',line.strip()
            elif len(line.strip().strip(',').split(',')) > 2:
                # raise NameError('\n{:s}\t{:d}行：\n此文件超过三列'.format(os.path.join(src_dir,src_csv),i))
                print '# ',line.strip()

            if data[0].startswith(node_type[0]):
                # 为父亲节点
                if data[0] == node_type[0]:
                    key = node_type[0] + str(max_num)
                    max_num += 1
                else:
                    key = data[0]
                supernode_name = data[1]
                key =  key + '_' + data[1]
                is_new_super_node = True
            else:
                # 为儿子节点
                key_id, key_name = key.split('_')
                if is_new_super_node:
                    if key_name not in name_file_dict:
                        node_dict[key] = dict()
                        name_file_dict[key_name] = [src_csv, key]
                    else:
                        key = name_file_dict[key_name][1]
                        key_id, key_name = key.split('_')
                is_new_super_node = False
                if not data[1].startswith(node_type[0]):
                    raise NameError('\n{:s}\t{:d}行：\n子节点错误'.format(os.path.join(src_dir,src_csv),i+1))
                if len(data[0].strip()) == 0:
                    node_dict[key][data[1]] = data[2]
                else:
                    print '# ',line.strip()
                    raise NameError('\n{:s}\t{:d}行：\n子节点错误'.format(os.path.join(src_dir,src_csv),i+1))
                if key_id not in node_dict[key] and key_id in id_name_dict:
                    node_dict[key][key_id] = id_name_dict[key_id]

    print '一共有{:d}个合并节点'.format(len(node_dict))
    node_set = set([k.split('_')[0] for k in node_dict]) | set([v for k,vs in node_dict.items() for v in vs])
    node_set = set([node for node in node_set if node[1] != '7'])
    print '包含{:d}个子节点节点'.format(len(node_set))
    for id,name in id_name_dict.items():
        if id[0] == node_type[0]:
            if id not in node_set:
                key = id + '_' + name
                node_dict[key] = {id:name}
    node_dict = merge_same_name_node_with_different_department(node_dict)
    py_op.mywritejson(obj_json, node_dict)

def renew_kginterface(src_dir,obj_dir):
    '''
    此函数用于更新supernode之后的关系
    '''
    def generate_symptom_disease_dict(disease_department_dict):
        symptom_disease_dict = { }
        for d,ss in disease_symptom_dict.items():
            for s in ss:
                try:
                    symptom_disease_dict[s].add(d)
                except:
                    symptom_disease_dict[s] = set([d])
        return symptom_disease_dict
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    id_name_dict = py_op.myreadjson(os.path.join(src_dir,'id_name_dict.json'))
    symptom_supernode_dict = py_op.myreadjson(os.path.join(src_dir,'symptom_supernode_dict.json'))
    disease_supernode_dict = py_op.myreadjson(os.path.join(src_dir,'disease_supernode_dict.json'))
    disease_symptom_dict = py_op.myreadjson(os.path.join(src_dir,'disease_symptom_dict.json'))
    disease_department_dict = py_op.myreadjson(os.path.join(src_dir,'disease_department_dict.json'))
    symptom_bodypart_dict = py_op.myreadjson(os.path.join(src_dir,'symptom_bodypart_dict.json'))
    id_degree_dict = py_op.myreadjson(os.path.join(src_dir,'id_degree_dict.json'))

    id_degree_dict_new = dict()
    disease_department_dict = { k:set(v) for k,v in disease_department_dict.items() }
    symptom_bodypart_dict = { k:set(v) for k,v in symptom_bodypart_dict.items() }
    symptom_supernode_set = set([x.split('_')[0] for x in symptom_supernode_dict.keys()])
    symptom_supernode_set = symptom_supernode_set | set([id for id in id_name_dict if id.startswith('s')])
    disease_supernode_set = set([x.split('_')[0] for x in disease_supernode_dict.keys()])
    disease_supernode_set = disease_supernode_set | set([id for id in id_name_dict if id[0]=='d' and id[1]!='p'])
    disease_symptom_dict = { k:set(v) for k,v in disease_symptom_dict.items() }
    symptom_disease_dict = generate_symptom_disease_dict(disease_symptom_dict)

    # 按照symptom_supernode_dict更新disease_symptom_dict
    for k,vs in symptom_supernode_dict.items():
        id,name = k.split('_')
        if id not in id_name_dict:
            id_name_dict[id] = name
        # 更新symptom-disease关系
        for v in vs:
            # 子症状
            if v != id and v in symptom_supernode_set:
                symptom_supernode_set.remove(v)
            # 子症状对应的疾病
            for d in symptom_disease_dict.get(v,[]):
                if d in disease_symptom_dict:
                    disease_symptom_dict[d].add(id)
                else:
                    raise NameError('多出来的疾病')

    # 按照disease_supernode_dict更新disease_symptom_dict
    for k,vs in disease_supernode_dict.items():
        id,name = k.split('_')
        id_name_dict[id] = name
        disease_symptom_dict[k] = disease_symptom_dict.get(k,set())
        for v in vs:
            if v != id and v in disease_supernode_set:
                disease_supernode_set.remove(v)
            for s in disease_symptom_dict.get(v,[]):
                try:
                    disease_symptom_dict[id].add(s)
                except:
                    disease_symptom_dict[id] = set([s])

    # 删除多余的子症状,子疾病
    # 重新生成disease_symptom_dict
    for d in disease_symptom_dict.keys():
        if d in disease_supernode_set:
            ss = list(disease_symptom_dict[d])
            for s in ss:
                if s not in symptom_supernode_set:
                    disease_symptom_dict[d].remove(s)
        else:
            disease_symptom_dict.pop(d)
    # 重新生成symptom_disease_dict
    symptom_disease_dict = generate_symptom_disease_dict(disease_symptom_dict)

    # 按照disease_supernode_dict更新disease_department_dict
    # 更新id_degree_dict_new
    id_key_dict = { key.split('_')[0]:key for key in disease_supernode_dict}
    for dis in disease_symptom_dict:
        if dis not in disease_department_dict:
            disease_department_dict[dis] = set()
        if dis not in id_degree_dict_new:
            id_degree_dict_new[dis] = 0.0
        if dis in id_key_dict:
            key = id_key_dict[dis]
            sub_list = disease_supernode_dict.get(key,[])
            for sub_dis in sub_list:
                id_degree_dict_new[dis] += id_degree_dict[sub_dis]/float(len(sub_list))
                for dp in disease_department_dict.get(sub_dis,[]):
                    disease_department_dict[dis].add(dp)
        if id_degree_dict_new[dis] == 0:
            id_degree_dict_new[dis] = id_degree_dict.get(dis,0)
    for k,vs in disease_department_dict.items():
        vs_new = set()
        for v in vs:
            if v in id_name_dict:
                vs_new.add(v)
                v_f = v[:-2]+'00'
                vs_new.add(v_f)
        disease_department_dict[k] = list(vs_new)
    disease_department_dict = { k:v for k,v in disease_department_dict.items() if k in disease_symptom_dict}

    # 按照symptom_supernode_dict更新symptom_bodypart_dict
    # 更新id_degree_dict_new
    for sym in symptom_disease_dict:
        if sym not in symptom_bodypart_dict:
            symptom_bodypart_dict[sym] = set()
        if sym not in id_degree_dict_new:
            id_degree_dict_new[sym] = 0
        if sym in symptom_supernode_dict:
            for sub_sym in symptom_supernode_dict[sym]:
                id_degree_dict_new[sym] += id_degree_dict[sub_sym]
                for body in symptom_bodypart_dict[sub_sym]:
                    symptom_bodypart_dict[sym].add(sub_sym)
        if id_degree_dict_new[sym] == 0:
            id_degree_dict_new[sym] = id_degree_dict.get(dis,0)

    for k,vs in symptom_bodypart_dict.items():
        vs_new = set()
        for v in vs:
            if v in id_name_dict:
                vs_new.add(v)
                v_f = v[:-2]+'00'
                vs_new.add(v_f)
        symptom_bodypart_dict[k] = list(vs_new)
    symptom_bodypart_dict = { k:v for k,v in symptom_bodypart_dict.items() if k in symptom_disease_dict}

    disease_id_name_dict = id_name_dict
    symptom_id_name_dict = id_name_dict
    # 
    disease_symptom_dict = { k:list(v) for k,v in disease_symptom_dict.items() }
    symptom_disease_dict = { k:list(v) for k,v in symptom_disease_dict.items() }
    print '一共{:d}种 疾病-症状'.format(len(disease_symptom_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_symptom_dict.json'),disease_symptom_dict)
    print '一共{:d}种 症状-疾病'.format(len(symptom_disease_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_disease_dict.json'),symptom_disease_dict)
    print '一共{:d}种 疾病-科室'.format(len(disease_department_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_department_dict.json'),disease_department_dict)
    # py_op.mywritejson(os.path.join(obj_dir,'disease_id-name-dict.json'),disease_id_name_dict)
    # py_op.mywritejson(os.path.join(obj_dir,'symptom_id-name-dict.json'),symptom_id_name_dict)
    print '一共{:d}种 症状-部位'.format(len(symptom_bodypart_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_bodypart_dict.json'),symptom_bodypart_dict)
    print '一共{:d}种 症状和疾病'.format(len(id_degree_dict_new))
    py_op.mywritejson(os.path.join(obj_dir,'id_degree_dict.json'),id_degree_dict_new)
    # print '一共{:d}种 id-name'.format(len(id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'id_name_dict.json'),id_name_dict)
    print '  有部位的症状',len([k for k,v in symptom_bodypart_dict.items() if len(v)>0])
    print '没有部位的症状',len([k for k,v in symptom_bodypart_dict.items() if len(v)==0])
    print '  有科室的疾病',len([k for k,v in disease_department_dict.items() if len(v)>0])
    print '没有科室的疾病',len([k for k,v in disease_department_dict.items() if len(v)==0])


def main_generate_kg_json():
    src_nt = '../data/middle_data/merge.nt.sub'
    json_dir = '../result/disease_symptom_kg_json'
    kg_dir = '/var/www/djangoapi/file/kgInterface/'
    # get_kg_json_from_nt(src_nt, json_dir)
    # get_supernode_dict_json('../data/initial_data/merge_disease_csv/finished/', os.path.join(json_dir,'disease_supernode_dict.json'), 'disease')
    # get_supernode_dict_json('../data/initial_data/merge_symptom_csv/finished/', os.path.join(json_dir,'symptom_supernode_dict.json'), 'symptom')
    renew_kginterface(json_dir,kg_dir)

if __name__ == '__main__':
    main_generate_kg_json()
