# coding=utf8
#########################################################################
# File Name: renew_id_name_map.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Sun Dec  3 00:46:06 2017
#########################################################################
import os
from libs import stats, kg_op, py_op

def renew_kginterface(src_dir,obj_dir):
    def generate_symptom_disease_dict(disease_department_dict):
        symptom_disease_dict = { }
        for d,ss in disease_symptom_dict.items():
            for s in ss:
                try:
                    symptom_disease_dict[s].add(d)
                except:
                    symptom_disease_dict[s] = set([d])
        return symptom_disease_dict
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    id_name_dict = py_op.myreadjson(os.path.join(src_dir,'id_name_dict.json'))
    symptom_supernode_dict = py_op.myreadjson(os.path.join(src_dir,'symptom_supernode_dict.json'))
    disease_supernode_dict = py_op.myreadjson(os.path.join(src_dir,'disease_supernode_dict.json'))
    disease_symptom_dict = py_op.myreadjson(os.path.join(src_dir,'disease_symptom_dict.json'))
    disease_department_dict = py_op.myreadjson(os.path.join(src_dir,'disease_department_dict.json'))

    symptom_supernode_set = set([x.split('_')[0] for x in symptom_supernode_dict.keys()])
    symptom_supernode_set = symptom_supernode_set | set([id for id in id_name_dict if id.startswith('s')])
    disease_supernode_set = set([x.split('_')[0] for x in disease_supernode_dict.keys()])
    disease_supernode_set = disease_supernode_set | set([id for id in id_name_dict if id[0]=='d' and id[1]!='p'])
    disease_symptom_dict = { k:set(v) for k,v in disease_symptom_dict.items() }
    symptom_disease_dict = generate_symptom_disease_dict(disease_symptom_dict)
    for k,vs in symptom_supernode_dict.items():
        id,name = k.split('_')
        id_name_dict[id] = name
        # 更新symptom-disease关系
        for v in vs:
            if v != id and v in symptom_supernode_set:
                symptom_supernode_set.remove(v)
            for d in symptom_disease_dict.get(v,[]):
                if d in disease_symptom_dict:
                    disease_symptom_dict[d].add(k)
    for k,vs in disease_supernode_dict.items():
        id,name = k.split('_')
        id_name_dict[id] = name
        disease_symptom_dict[k] = disease_symptom_dict.get(k,set())
        for v in vs:
            if v != id and v in disease_supernode_set:
                disease_supernode_set.remove(v)
            for vv in v:
                disease_symptom_dict[k].add(vv)
    for d in disease_symptom_dict.keys():
        if d in disease_supernode_set:
            ss = list(disease_symptom_dict[d])
            for s in ss:
                if s not in symptom_supernode_set:
                    disease_symptom_dict[d].remove(s)
        else:
            disease_symptom_dict.pop(d)
    symptom_disease_dict = generate_symptom_disease_dict(disease_symptom_dict)
    # 瞎写的
    disease_department_dict = {d:disease_department_dict.values()[0] for d in disease_symptom_dict}
    disease_id_name_dict = id_name_dict
    symptom_id_name_dict = id_name_dict
    # 
    disease_symptom_dict = { k:list(v) for k,v in disease_symptom_dict.items() }
    symptom_disease_dict = { k:list(v) for k,v in symptom_disease_dict.items() }
    print '一共{:d}种 疾病-症状'.format(len(disease_symptom_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_symptom_dict.json'),disease_symptom_dict)
    print '一共{:d}种 症状-疾病'.format(len(symptom_disease_dict))
    py_op.mywritejson(os.path.join(obj_dir,'symptom_disease_dict.json'),symptom_disease_dict)
    print '一共{:d}种 疾病-科室'.format(len(disease_department_dict))
    py_op.mywritejson(os.path.join(obj_dir,'disease_department_dict.json'),disease_department_dict)
    # py_op.mywritejson(os.path.join(obj_dir,'disease_id-name-dict.json'),disease_id_name_dict)
    # py_op.mywritejson(os.path.join(obj_dir,'symptom_id-name-dict.json'),symptom_id_name_dict)
    print '一共{:d}种 id-name'.format(len(id_name_dict))
    py_op.mywritejson(os.path.join(obj_dir,'id_name_dict.json'),id_name_dict)

def main():
    renew_kginterface('../result/relation_json','/var/www/djangoapi/file/kgInterface/')

if __name__ == '__main__':
    main()
