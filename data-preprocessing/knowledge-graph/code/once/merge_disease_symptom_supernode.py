# coding=utf8
#########################################################################
# File Name: merge_supernode.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue Oct 24 16:17:32 2017
#########################################################################
'''
这个文件主要关于
1. 生成supernode（疾病，症状）的csv文件
2. 处理csv文件，产生新的nt文件
'''

import re
import os
from libs import stats, kg_op, py_op
import json
#########################################################################
# 公用函数
#########################################################################
#########################################################################
# 生成csv文件
def generate_merge_symptom_supernode_csv(src_nt, obj_csv_dir):
    '''
    此函数用于统计每种部位的症状数量,生成csv
    '''
    symptom_bodypart_dict = { }
    bodypart_symptom_dict = { }
    id_name_dict = { }
    name_id_dict = { }
    for line in open(src_nt,'r'):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        if 'name' in pre:
            name, id = kg_op.get_node_type_id(sub)
            id_name_dict[id] = obj.strip('"')
            if id.startswith('s'):
                name_id_dict[obj.strip('"')] = id

        if 'symptom' in sub and 'bodypart' in obj:
            _, sym_id = kg_op.get_node_type_id(sub)
            _, body_id = kg_op.get_node_type_id(obj)
            try:
                symptom_bodypart_dict[sym_id].add(body_id[:-2])
            except KeyError:
                symptom_bodypart_dict[sym_id] = set([body_id[:-2]])
            try:
                bodypart_symptom_dict[body_id].append(sym_id)
            except KeyError:
                bodypart_symptom_dict[body_id] = [sym_id]

    num_of_symptoms_with_many_bodypart= 0
    for sym, body_list in symptom_bodypart_dict.items():
        if len(body_list) > 1:
            num_of_symptoms_with_many_bodypart += 1
    print 'num_of_symptoms_with_many_bodypart', num_of_symptoms_with_many_bodypart

    if not os.path.exists(obj_csv_dir):
        os.mkdir(obj_csv_dir)
    print '部位，症状数量'
    print len(bodypart_symptom_dict)
    for body, sym_list in bodypart_symptom_dict.items():
        if body[-2:] == '00':
            print id_name_dict[body],'\t',len(sym_list)
            '''
            with open(os.path.join(obj_csv_dir, id_name_dict[body]+'.csv'),'w') as f:
                for sym in sym_list:
                    f.write(sym)
                    f.write(',')
                    f.write(id_name_dict[sym])
                    f.write('\n')
            '''
            with open(os.path.join(obj_csv_dir, id_name_dict[body]+'_无推荐.csv'),'w') as f:
                for k in sym_list:
                    f.write(k)
                    f.write(',')
                    f.write(id_name_dict[k])
                    f.write('\n')

            continue

            fuzzy_dict = py_op.fuzz_list([id_name_dict[id] for id in sym_list])
            with open(os.path.join(obj_csv_dir, id_name_dict[body]+'.csv'),'w') as f:
                for k,vs in fuzzy_dict.items():
                    f.write(name_id_dict[k])
                    f.write(',')
                    f.write(k)
                    f.write('\n')
                    for v in vs:
                        f.write(',')
                        f.write(',')
                        f.write(name_id_dict[v])
                        f.write(',')
                        f.write(v)
                        f.write('\n')

def generate_merge_disease_supernode_csv(src_nt, obj_csv_dir, symptom_csv_dir):
    '''
    此函数用于统计每种科室的疾病数量,生成csv
    '''
    def string_map(s):
        if type(s) is unicode:
            for t in [u'老年人',u'小儿',u'新生儿']:
                if t in s:
                    s = s.replace(t,u'')
            for t in [u'功能衰竭', u'力衰竭', u'衰竭']:
                if t in s:
                    s = s.replace(t,u'衰')
        elif type(s) is str:
            for t in ['老年人','小儿','新生儿']:
                if t in s:
                    s = s.replace(t,'')
            for t in ['功能衰竭', '力衰竭', '衰竭']:
                if t in s:
                    s = s.replace(t,'衰')
        if '（' in s:
            s = s.split('（')[0]
            # data = s.split('（')
            # if not data[1].endswith('）') or '科' not in data[1]:
            #     print s
        return s
    disease_left_set = generate_left_diseases(src_nt, symptom_csv_dir)
    map_dict = stats.stats_sub_obj_tree(src_nt, 'disease', 'department')
    disease_department_dict = map_dict['linked_nodes']
    map_dict = stats.stats_obj_sub_tree(src_nt, 'department', 'disease')
    department_disease_dict = map_dict['linked_nodes']
    # id_name_dict = kg_op.id_name_dict
    name_id_dict = { }
    id_name_dict = { }
    disease_set = set()
    for line in open(src_nt,'r'):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        if 'name' in pre:
            name, id = kg_op.get_node_type_id(sub)
            # id_name_dict[id] = obj.strip('"')
            if id in disease_left_set:
                name = obj.strip('"')
                # name = str(unicode(name))
                # name = unicode(name)
                name_id_dict[name] = id
                name_id_dict[unicode(name)] = id
                id_name_dict[id] = name
                if id[1] != 'p':
                    disease_set.add(name)

    # 保留一级科室
    for dis, dep_list in disease_department_dict.items():
        dep_list = list(set([dep[:-2]+'00' for dep in dep_list]))
        disease_department_dict[dis] = dep_list

    num_of_diseases_with_many_department = 0
    for dis, dep_list in disease_department_dict.items():
        if len(dep_list) > 1:
            num_of_diseases_with_many_department += 1
    print 'num_of_diseases_with_many_department', num_of_diseases_with_many_department

    if not os.path.exists(obj_csv_dir):
        os.mkdir(obj_csv_dir)

    node2_list = list(disease_set)
    length=1000
    for i in range(len(disease_set)/length + 1):
        node1_list = [id_name_dict[id] for id in sorted(id_name_dict.keys()) if id[0] == 'd' and id[1] != 'p'][i*length:i*length+length]
        fuzzy_dict, node2_list = py_op.fuzz_list(node1_list,node2_list,66,19,string_map)
        print len(node2_list),len(disease_set)
        with open(os.path.join(obj_csv_dir, str(i)+'.csv'),'w') as f:
            for k,vs in fuzzy_dict.items():
                f.write(name_id_dict[k])
                f.write(',')
                f.write(k)
                f.write('\n')
                for v in vs:
                    f.write(',')
                    f.write(',')
                    f.write(name_id_dict[v[0]])
                    f.write(',')
                    f.write(v[0])
                    f.write(',')
                    f.write(str((v[1])))
                    f.write('\n')

    return
    print '科室，疾病数量'
    for dep, dis_list in department_disease_dict.items():
        # if dep[-2:] == '00' or dep[:-2] in ['dp141','dp151']: 
        # if dep[-2:] == '00':
        if dep[:-2] in ['dp141','dp151']: 
            if len(dis_list) > 100:
                print id_name_dict[dep],'\t',len(dis_list)
            else:
                continue
                pass
            with open(os.path.join(obj_csv_dir, id_name_dict[dep]+'_无推荐.csv'),'w') as f:
                for k in dis_list:
                    f.write(k)
                    f.write(',')
                    f.write(id_name_dict[k])
                    f.write('\n')

            if len(dis_list) > 1000:
                continue
            # continue

            fuzzy_dict = py_op.fuzz_list([id_name_dict[id] for id in dis_list])
            with open(os.path.join(obj_csv_dir, id_name_dict[dep]+'.csv'),'w') as f:
                for k,vs in fuzzy_dict.items():
                    f.write(name_id_dict[k])
                    f.write(',')
                    f.write(k)
                    f.write('\n')
                    for v in vs:
                        f.write(',')
                        f.write(',')
                        f.write(name_id_dict[v])
                        f.write(',')
                        f.write(v)
                        f.write('\n')


#########################################################################
# 处理csv文件
#########################################################################
def generate_merged_symptom_tree(src_csv_dir):
    '''
    此函数用于将已经标注过的症状supernode,生成可以使用的dict
    '''
    max_num = 70000
    symptom_dict = dict()
    print '#\n#    有问题的症状:\n#'
    for src_csv in os.listdir(src_csv_dir):
        for line in open(os.path.join(src_csv_dir, src_csv),'r'):
            data = [x.strip() for x in line.split(',')]
            if len(data[1].strip()):
                if len(data[0]) == 0:
                    key = 's' + str(max_num) + '_' + data[1]
                    max_num += 1
                else:
                    key = data[0] + '_' + data[1]
                symptom_dict[key] = dict()
            if len(data[0].strip()):
                # symptom_dict[key].append(data[0])
                symptom_dict[key][data[0]] = data[1]
            if len(data) > 2 and len(data[2]):
                # symptom_dict[key].append(data[2])
                if data[2].startswith('s'):
                    symptom_dict[key][data[2]] = data[3]
                else:
                    for k in data:
                        print '#\t',k
                    print '#'
    return symptom_dict

def generate_left_diseases(src_nt, src_csv_dir):
    print '一共剩下{:d}个症状'.format(len(generate_merged_symptom_tree('../data/initial_data/merge_symptom_csv')))
    symptom_dict = generate_merged_symptom_tree(src_csv_dir)
    symptom_left_set = set([sid for sids in symptom_dict.values() for sid in sids])
    disease_left_set = set()
    for line in open(src_nt,'r'):
        if 'symptom' in line and 'disease' in line:
            sub, pre, obj = kg_op.get_sub_pre_obj(line)
            _ , sub_id = kg_op.get_node_type_id(sub)
            _ , obj_id = kg_op.get_node_type_id(obj)
            if sub_id.startswith('s'):
                did = obj_id
                sid = sub_id
            elif sub_id.startswith('d'):
                did = sub_id 
                sid = obj_id
            if sid in symptom_left_set:
                disease_left_set.add(did)
                # disease_left_set.add(did)
    print '一共剩下{:d}个疾病'.format(len(disease_left_set))
    return disease_left_set

def process_merge_disease_csv(src_csv_dir, obj_json):
    disease_dict = dict()
    disease_set = set()
    max_num = 70000
    for src_csv in os.listdir(src_csv_dir):
        src_csv = os.path.join(src_csv_dir,src_csv)
        new_supernode = True
        for line in open(src_csv):
            line = line.strip()
            if len(line):
                if line.startswith(','):
                    id,name = line.strip(',').split(',')[:2]
                    disease_set.add(id)
                    disease_dict[key][id] = name
                elif line.startswith('d'):
                    id,name = line.split(',')[:2]
                    if len(id) == 1:
                        id = 'd' + str(max_num)
                        max_num += 1
                    disease_set.add(id)
                    # key = name+'_'+id
                    key = id+'_'+name
                    disease_dict[key] = dict()
                    new_supernode = False
                    if '瘤' in name or '癌' in name:
                        print src_csv,name
                else:
                    raise NameError(src_csv)
                if new_supernode:
                    raise NameError(src_csv)
            else:
                new_supernode = True
    print '一共有{:d}个疾病supernode'.format(len(disease_dict))
    print '一共有{:d}个疾病子node'.format(len(disease_set))
    with open(obj_json,'w') as f:
        f.write(json.dumps(disease_dict,indent=4,ensure_ascii=False))

def generate_symptom_supernode_from_labeled_data(src_csv_dir,obj_json):
    '''
    此函数用于将已经标注过的症状supernode,生成可以使用的json文件(dict格式)
    '''
    symptom_dict = generate_merged_symptom_tree(src_csv_dir)
    print '融合后一共有{:d}个症状'.format(len(symptom_dict))
    py_op.mywritejson(obj_json,symptom_dict)
    symptom_set = set()
    for k,vs in symptom_dict.items():
        id,name = k.split('_')
        if id == '':
            print name
        for v in vs:
            symptom_set.add(v)
    print '融合前一共有{:d}个症状'.format(len(symptom_set))

def main_pre():
    src_nt = '../data/middle_data/merge.nt.sub' # 用处理过的子图来生成csv
    # generate_merge_symptom_supernode_csv(src_nt, '../result/merge_symptom_supernode_csv')
    symptom_dict = generate_merged_symptom_tree('../data/initial_data/merge_symptom_csv')
    print '一共剩下{:d}个症状'.format(len(symptom_dict))
    # generate_merge_disease_supernode_csv(src_nt, '../result/merge_disease_supernode_csv', '../result/merge_symptom_supernode_csv')
    # generate_left_diseases(src_nt,'../data/initial_data/merge_symptom_csv')
    # process_merge_disease_csv('../data/initial_data/disease_rename/finished/','../result/disease_supernode_dict.json')
    generate_symptom_supernode_from_labeled_data('../data/initial_data/merge_symptom_csv','../result/symptom_supernode_dict.json')

if __name__ == '__main__':
    main_pre()
