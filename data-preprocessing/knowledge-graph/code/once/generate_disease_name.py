# coding=utf8
import os
import sys
sys.path.append('../')
from libs import stats, kg_op, py_op

difficult_diseases= '''
	    肺炎
	    肾炎
	    肝炎
	    肠炎
	    腺癌
	    纤维瘤
	    腺瘤
	    菌感染
	    囊肿
	    皮肤病
	    营养不良
	    肉芽肿
	    动脉瘤
	    骨折
	    皮炎
	    肉瘤
            贫血
            白血病
	    休克
	    头痛
	    肾病
	    关节炎
	    软骨病
	    偏头痛
            头痛
            紫癜
	    宫颈
	    黏多糖贮积症
	    维生素
	    肝病
	    结肠炎
	    胰腺炎
	    不孕
            不育
	    鼻炎
            鼻窦炎
            血吸虫
            淋巴瘤
        '''

def gen_rename_disease_from_nt(src_nt,obj_dir):
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    for dis in difficult_diseases.split('\n'):
        dis = dis.strip()
        if len(dis):
            f = open(os.path.join(obj_dir,dis+'.csv'),'w')
            for line in open(src_nt):
                if dis in line:
                    sub, pre, obj = kg_op.get_sub_pre_obj(line)
                    dis_type, dis_id = kg_op.get_node_type_id(sub)
                    _, link_name = kg_op.get_node_type_id(pre)
                    if dis_type == 'disease' and link_name == 'name':
                        # f.write(line)
                        f.write(',')
                        f.write(dis_id)
                        f.write(',')
                        f.write(obj.strip('"'))
                        f.write('\n')
            f.close()

def gen_rename_disease_from_csv(src_obj,obj_dir):
    def get_node_root(node_dict,node):
        node_set = set()
        while node != node_dict[node]:
            node_set.add(node)
            node = node_dict[node]
        for n in node_set:
            node_dict[n] = node
        return node
    node_dict = dict()
    id_name_dict = dict()
    for fname in os.listdir(src_obj):
        for line in open(os.path.join(src_obj,fname)):
            line = line.strip()
            if line.startswith(','):
                line = line.strip(',')
                if line.startswith('d'):
                    id,name = line.split(',')[:2]
                    if id not in id_name_dict:
                        id_name_dict[id] = name
                    if id in node_dict:
                        id = get_node_root(node_dict,id)
                        node_dict[id] = key
            else:
                id,name = line.split(',')[:2]
                if id not in id_name_dict:
                    id_name_dict[id] = name
                key = id
                if key not in node_dict:
                    node_dict[key] = key
                else:
                    key = get_node_root(node_dict,key)
    # 合并node_dict
    root_node_dict = dict()
    for node in node_dict:
        if node_dict[node] == node:
            root_node_dict[node] = []
    for node in node_dict:
        root_node = get_node_root(node_dict,node)
        root_node_dict[root_node].append(node)
    for root_node,leaf_nodes  in root_node_dict.items():
        if len(leaf_nodes) == 1:
            continue
        f = open(os.path.join(obj_dir,root_node+'_'+id_name_dict[root_node]+'.csv'),'w')
        for node in leaf_nodes:
            f.write(',')
            f.write(node)
            f.write(',')
            f.write(id_name_dict[node])
            f.write('\n')
        f.close()




if __name__ == '__main__':
    src_nt = '../../data/middle_data/merge.nt.sub' 
    gen_rename_disease_from_nt(src_nt,'../../result/disease_rename/')
    gen_rename_disease_from_csv('../../data/initial_data/merge_disease_csv/','../../result/disease_rename/')
