# coding=utf8
#########################################################################
# File Name: generate_difficult_nodes.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: 2017年12月02日 星期六 15时13分25秒
#########################################################################
import json
import os
import sys
reload(sys)
sys.setdefaultencoding('utf8')

def generate_difficult_nodes(src_csv,obj_csv_dir,disease_name,must_word,fclass_list,sclass_list):
    '''
    几个大类疾病: 癌症,炎症,溃疡
    '''
    def belong_this_disease(line):
        '''
        判断该疾病是否属于癌症
        '''
        for words in must_word:
            vis = True
            for w in words:
                if w not in line:
                    vis = False
                    break
            if vis:
                return True
        return False
    if not os.path.exists(obj_csv_dir):
        os.mkdir(obj_csv_dir)
    disease_dict = dict()
    disease_set = set()
    disease_with_sub_class = set()
    for fcls in fclass_list:
        disease_dict[fcls[0]] = set()
    for line in open(src_csv):
        line = line.split('(')[0].split('（')[0].strip()+'\n'
        # line = line.split(',')[2]
        if belong_this_disease(line):
            disease_set.add(line)
            for fcls_list in fclass_list:
                for fcls in fcls_list:
                    if fcls in line:
                        disease_dict[fcls_list[0]].add(line)
                        disease_with_sub_class.add(line)
    print '{:s}一共有{:d}种'.format(disease_name, len(disease_set))
    print '有类别的一共有{:d}种'.format(len(disease_with_sub_class))
    print '无类别的一共有{:d}种'.format(len(disease_set)-len(disease_with_sub_class))
    name_set = set([name.split(',')[-1] for name in disease_with_sub_class])
    wf = open(os.path.join(obj_csv_dir,'{:s}_无分类.csv'.format(disease_name)),'w')
    for line in disease_set:
        # if line not in disease_with_sub_class:
        name = line.split(',')[-1]
        if name not in name_set:
            name_set.add(name)
            wf.write(line)
    wf.close()
    for fcls,lines in disease_dict.items():
        line_name_set = set([line.split(',')[-1] for line in lines])
        if len(line_name_set) <= 1:
            for line in lines:
                print line.strip()
            print
            continue
        wf = open(os.path.join(obj_csv_dir,'{:s}_{:s}.csv'.format(fcls,disease_name)),'w')
        for line in lines:
            wf.write(line)
        wf.close()


def get_max_num(src_nt):
    max_num_dict = dict()
    max_num_dict['symptom'] = 0
    max_num_dict['disease'] = 0
    for line in open(src_nt):
        data = line.split('/')
        for i in range(len(data)):
            if data[i] == 'disease':
                id = data[i+1].split('>')[0]
                if id.startswith('d'):
                    num = int(id[1:])
                    max_num_dict['disease'] = max(max_num_dict['disease'],num)
            elif data[i] == 'symptom':
                id = data[i+1].split('>')[0]
                if id.startswith('s'):
                    num = int(id[1:])
                    max_num_dict['symptom'] = max(max_num_dict['symptom'],num)
    print max_num_dict




def main():
    # 癌症
    generate_difficult_nodes(
            src_csv='../../data/middle_data/disease_id_name.csv',
            obj_csv_dir='../../result/hard_nodes',
            disease_name = '癌症',
            must_word=[['癌'],['瘤']],
            fclass_list=[
                # 头部
                ['脑','颅'],['鼻','窦'],['牙'],['垂体'],['口腔','口咽','口底'],['耳'],['腭'],
                ['视神经','眼'],['视网膜'],['听神经'],['泪'],
                # 颈部
                ['甲状腺'],['胸腺'],['颈部'],['椎'],['腹'],['脊'],['咽喉','喉','咽'],['气管'],
                # 胸腹部
                ['心','室管','室内','左房','室壁'],['肝'],['脾'],['肺'],['肾'],['胃'],

                ['肠'],['食管'],
                ['胆'],['胰'],['阑尾'],['横纹肌'],['平滑肌'],
                ['胸膜'],['胸壁'],
                # 全身
                ['骨'],['汗腺','汗管'],['皮肤'],['淋巴'],
                ['血管','脉管','动脉','静脉'],['毛'],
                ['腺瘤','腺癌'],
                # 腺癌
                ['涎腺'],['腮腺'],['皮脂腺'],
                # 生殖系统
                ['生殖'],['肛'],
                ['卵巢','输卵管'],['阴道','外阴','阴囊'],['子宫','宫颈'],['乳腺','乳房','乳头'],['盆腔'],
                ['前列腺'],['尿道','输尿管','泌尿'],['膀胱'],['睾'],
                # 其他
                ['松果体'],['星形细胞'],['棘皮'],['黑色素','黑素'],['黄色','黄瘤'],['角化'],['扁桃体'],['纵膈','纵隔','膈肌'],
                ['结核'],
                ['疼','痛'],

                # 不合并
                ['阴茎'],['舌'],['颌'],['面','脸','颊'],['前庭'],['浆细胞'],['贲门'],['唇'],['催乳素','泌乳素'],['间皮'],
                ['嗜酸性'],

                # 无法分类
                ['母细胞'],['滋养细胞'],['嗜铬细胞'],['颗粒细胞'],['基底'],
                ['软组织'],['胶质'],['错构'],['疣'],['脂肉'],
                ['神经纤维'],['纤维瘤','纤维肉瘤','纤维组织'],['中枢'],['神经鞘'],['脂肪'],

                ['神经系统'],
                ['胎'],
                ['鳞'],
                ['息肉'],
                ['叶'],
                ['表皮样'],
                ['上皮'],
                ['产'],
                ['巨细胞'],
                ['精原细胞'],
                ['梅克尔','神经内分泌'],
                ['内分泌'],
                ['腺泡细胞'],
                ['神经细胞'],
                ['成釉细胞'],
                ['网膜'],
                ['疹'],
                ['卡波西','Kaposis'],
                ['脉络膜'],
                ['眶假'],
                ['葡萄'],
                ['滑膜'],
                ['粘液'],
                ['声带'],
                ['隔膜'],
                ['尤因'],
                ['精囊','精索'],
                ['髓'],
                ['瘢痕'],
                ['蛰伏脂瘤','冬眠','棕色脂肪瘤'],
                ['多形性'],
                ['管状腺'],
                ['囊性','囊肿'],
                ['尤文氏'],
                ],
            sclass_list=['纤维'],
            )
    # 炎症
    generate_difficult_nodes(
            src_csv='../../data/middle_data/disease_id_name.csv',
            obj_csv_dir='../../result/hard_nodes',
            disease_name = '炎症',
            must_word=[['炎']],
            fclass_list=[
                # 头部
                ['脑','颅'],['鼻','窦'],['口腔','牙','龈','齿','口炎','口角','舌','唇'],['垂体'],['耳'],['腭'],
                ['视神经','视盘','眼','睑'],['视网膜'],['听神经'],['泪'],
                # 颈部
                ['甲状腺'],['胸腺'],['颈部'],['腹'],['脊'],['咽喉','喉','咽'],['气管'],
                # 胸腹部
                ['心'],['肝'],['脾'],['肺'],['肾'],['胃'],
                ['肠'],['食管'],
                ['胆'],['胰'],['阑尾'],['横纹肌'],['平滑肌'],
                ['胸膜'],['胸壁'],
                # 全身
                ['骨'],['汗腺','汗管'],['皮肤'],['淋巴'],
                ['血管','脉管','动脉','静脉'],['毛'],
                # 腺癌
                ['涎腺'],['腮腺'],['皮脂腺'],
                # 生殖系统
                ['肛'],
                ['附件','卵巢','输卵管'],
                ['阴道','外阴','阴囊'],['子宫','宫颈'],['乳'],['盆腔'],
                ['前列腺'],['尿道','输尿管'],['膀胱'],['睾'],['龟头'],
                # 其他
                ['松果体'],['扁桃体'],['纵隔'],['包涵体'],
                ['皮炎','皮肌炎'],['多肌','多发性肌','肌纤维组织炎','良性肌炎'],
                ['根尖周'],
                ['关节炎'],
                ['肌腱','腱鞘'],['跟腱'],
                ['蜂窝'],
                ['指头'],
                ['周围'],
                ['神经'],
                ['会厌'],
                ['脐'],
                ['妇科'],
                ['甲沟','甲周','指炎'],


                # 不合并
                ['阴茎'],['颌'],['面'],['前庭'],['血吸虫'],['黑变病'],['腺样体'],['迷路'],

                # 膜炎
                ['角膜'],['滑膜'],['网膜'],['结膜'],['巩膜'],['脂膜'],['葡萄膜'],['筋膜'],['虹膜'],['脉络膜'],['鼓膜'],
                ['滑囊'],['精囊'],

                ],
            sclass_list=['纤维'],
            )

if __name__ == '__main__':
    main()
    # get_max_num('../../data/initial_data/merge.nt')
