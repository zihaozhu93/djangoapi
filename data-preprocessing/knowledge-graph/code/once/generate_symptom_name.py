# coding=utf8
import os
import sys
sys.path.append('../')
from libs import stats, kg_op, py_op

def gen_rename_symptom_from_csv(src_obj,obj_dir):
    def get_node_root(node_dict,node):
        node_set = set()
        while node != node_dict[node]:
            node_set.add(node)
            node = node_dict[node]
        for n in node_set:
            node_dict[n] = node
        return node
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    id_max = 70000
    node_dict = dict()
    id_name_dict = dict()
    for fname in os.listdir(src_obj):
        for i,line in enumerate(open(os.path.join(src_obj,fname))):
            line = line.strip()
            data = line.split(',')
            if len(line.replace(',','').strip()) == 0:
                continue
            if len(data[1].strip()) + len(data[0].strip()) == 0:
                # 子症状
                if data[2].startswith('s'):
                    id,name = data[2:4]
                    if id not in id_name_dict:
                        id_name_dict[id] = name
                    if id in node_dict:
                        id = get_node_root(node_dict,id)
                    node_dict[id] = key
                else:
                    raise NameError('又错误!')
            else:
                # 合并症状
                id,name = line.split(',')[:2]
                if id.strip() == '':
                    id = 's'+str(id_max)
                    id_max += 1
                if id not in id_name_dict:
                    id_name_dict[id] = name
                key = id
                if key not in node_dict:
                    node_dict[key] = key
                else:
                    key = get_node_root(node_dict,key)
    # 合并node_dict
    root_node_dict = dict()
    for node in node_dict:
        if node_dict[node] == node:
            root_node_dict[node] = []
    for node in node_dict:
        root_node = get_node_root(node_dict,node)
        root_node_dict[root_node].append(node)
    print len(root_node_dict)
    for root_node,leaf_nodes  in root_node_dict.items():
        if len(leaf_nodes) == 1:
            continue
        f = open(os.path.join(obj_dir,root_node+'_'+id_name_dict[root_node]+'.csv'),'w')
        for node in leaf_nodes:
            f.write(',')
            f.write(node)
            f.write(',')
            f.write(id_name_dict[node])
            f.write('\n')
        f.close()


def gen_symptom_id_name(src_json,obj_json):
    id_name_dict = py_op.myreadjson(src_json)
    id_list = [id for id in id_name_dict if id[0] == 's']
    id_list = sorted(id_list)
    f = open(obj_json,'w')
    for id in id_list:
        f.write(',')
        f.write(id)
        f.write(',')
        f.write(id_name_dict[id])
        f.write('\n')

def gen_rename_symptom_from_json(src_json,obj_dir):
    if not os.path.exists(obj_dir):
        os.mkdir(obj_dir)
    symptom_supernode_dict = py_op.myreadjson(src_json)
    print len(symptom_supernode_dict)
    i = 0
    for super_node, sub_node_dict in symptom_supernode_dict.items():
        if len(sub_node_dict) > 1:
            f = open(os.path.join(obj_dir,super_node),'w')
            for id,name in sub_node_dict.items():
                f.write(',')
                f.write(id)
                f.write(',')
                f.write(name)
                f.write('\n')
            f.close()


if __name__ == '__main__':
    src_nt = '../../data/middle_data/merge.nt.sub' 
    gen_rename_symptom_from_csv('../../data/initial_data/merge_symptom_csv/','../../result/symptom_rename/')
    # gen_symptom_id_name('../../result/relation_json/id_name_dict.json','../../result/symptom_id_name.csv')
    # gen_rename_symptom_from_json('../../result/symptom_supernode_dict.json','../../result/symptom_rename/')
