# coding=utf-8
#########################################################################
# File Name: generate_stats_info.sh
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue 24 Oct 2017 01:16:56 PM CST
#########################################################################
"""
此文件主要用于收集nt文件的各种统计信息
"""

import os
from libs import stats,kg_op,py_op

def generate_relation_tree(src_nt, obj_json):
    relation_tree = stats.relation_tree(src_nt)
    py_op.mywritejson(obj_json,relation_tree)

def generate_bodypart_tree(src_nt, obj_json):
    bodypart_tree = stats.bodypart_tree(src_nt,True)
    py_op.mywritejson(obj_json,bodypart_tree)

def generate_department_tree(src_nt, obj_json):
    department_tree = stats.department_tree(src_nt)
    py_op.mywritejson(obj_json,department_tree)

def generate_disease_id_name(src_nt,obj_csv):
    f = open(obj_csv,'w')
    for line in open(src_nt):
        if 'disease' in line and 'name' in line:
            sub, pre, obj = kg_op.get_sub_pre_obj(line)
            name, id = kg_op.get_node_type_id(sub)
            f.write(',')
            f.write(id)
            f.write(',')
            f.write(obj.strip('"'))
            f.write('\n')
    f.close()

def generate_id_degree_dict(src_nt, obj_json):
    id_degree_dict = stats.stats_id_degree_dict(src_nt)
    py_op.mywritejson(obj_json, id_degree_dict)

def generate_id_name_dict(src_nt, obj_json):
    id_name_dict = stats.stats_id_name_dict(src_nt)
    py_op.mywritejson(obj_json, id_name_dict)

def generate_sub_obj_per_tree(src_nt,obj_json_dir):
    if not os.path.exists(obj_json_dir):
        os.mkdir(obj_json_dir)

    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['symptom'],['disease'],['SymPosDis'],False)
    symptom_disease_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 症状-疾病'.format(len(symptom_disease_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'symptom_disease_dict.json'),symptom_disease_dict)

    disease_symptom_dict = dict()
    for s,ds in symptom_disease_dict.items():
        for d in ds:
            try:
                disease_symptom_dict[d].append(s)
            except:
                disease_symptom_dict[d] = [s]
    print '一共有{:d}种 疾病-症状'.format(len(disease_symptom_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'disease_symptom_dict.json'),disease_symptom_dict)

    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['symptom'],['bodypart'],['SympFirBody','SymSecBody'],False)
    symptom_bodypart_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 症状-部位'.format(len(symptom_bodypart_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'symptom_bodypart_dict.json'),symptom_bodypart_dict)

    bodypart_symptom_dict = dict()
    for s,bs in symptom_bodypart_dict.items():
        for b in bs:
            try:
                bodypart_symptom_dict[b].append(s)
            except:
                bodypart_symptom_dict[b] = [s]
    print '一共有{:d}种 部位-症状'.format(len(bodypart_symptom_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'bodypart_symptom_dict.json'),bodypart_symptom_dict)

    sub_obj_pre_dict = stats.stats_sub_obj_pre_tree(src_nt, ['disease'],['department'],['DisFirDep','DisSecDep'],False)
    disease_department_dict = sub_obj_pre_dict.values()[0].values()[0].values()[0]
    print '一共有{:d}种 疾病-科室'.format(len(disease_department_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'disease_department_dict.json'),disease_department_dict)

    department_disease_dict = dict()
    for d,dps in disease_department_dict.items():
        for dp in dps:
            try:
                department_disease_dict[dp].append(d)
            except:
                department_disease_dict[dp] = [d]
    print '一共有{:d}种 科室-疾病'.format(len(department_disease_dict))
    py_op.mywritejson(os.path.join(obj_json_dir,'department_disease_dict.json'),department_disease_dict)

    generate_id_name_dict(src_nt,os.path.join(obj_json_dir,'id_name_dict.json'))

def main():
    src_nt = '../result/merge.nt.sub'
    src_nt = '../data/middle_data/merge.nt.sub'
    src_nt = '../data/middle_data/merge.nt.20171024'
    # generate_relation_tree('../data/middle_data/merge.nt.sub','../result/relation_tree.json')
    generate_bodypart_tree('../data/middle_data/merge.nt.sub','../result/bodypart_tree.json')
    # generate_department_tree('../data/middle_data/merge.nt.sub','../result/department_tree.json')
    # generate_disease_id_name('../data/middle_data/merge.nt.sub','../result/disease_id_name.csv')
    # generate_id_degree_dict('../data/middle_data/merge.nt.20171024','../result/id_degree_dict.json')
    # generate_sub_obj_per_tree(src_nt,'../result/relation_json')

if __name__ == '__main__':
    main()
    # generate_bodypart_tree('../data/middle_data/merge.nt.20171024.without_isolated_node','../result/bodypart_tree.json')

