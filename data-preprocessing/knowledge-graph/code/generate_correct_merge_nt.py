# coding=utf8
"""
    原有nt文件有少许问题，20171024重新做一次数据，以后尽可能不再更改merge.nt文件
    保存为 merge.nt.20171024
    后续的挑选数据都从 merge.nt.20171024 中进行
"""

import os
from libs import stats, kg_op

def replace_xxx(src_nt, obj_nt):
    """
    去掉以前未处理的XXX
    所有的XXX都是症状和疾病，未对应到部位和科室留下的
    处理起来相对简单
    """
    wf = open(obj_nt,'w')
    for line in open(src_nt,'r'):
        if 'xxx' in line:
            continue
        if 'XXX' in line:
            sub, pre, obj = kg_op.get_sub_pre_obj(line)
            sub_type, sub_id = kg_op.get_node_type_id(sub)
            obj_type, obj_id = kg_op.get_node_type_id(obj)
            if obj_id.endswith('00'):
                relation = sub_type[:3].title() + 'Fir' + obj_type[:3].title()
            else:
                relation = sub_type[:3].title() + 'Sec' + obj_type[:3].title()
            line = line.replace('XXX',relation)
        wf.write(line)
    wf.close()

def generate_correct_merge(src_nt, obj_nt):
    # 替换XXX为正确关系
    replace_xxx(src_nt,obj_nt)
    # 增加一级关系
    kg_op.add_first_relation(obj_nt, obj_nt)
    # 增加反向边
    pass
    # 删除自相连边
    kg_op.delete_self_relation(obj_nt, obj_nt)
    # 删除重复边
    kg_op.delete_repeate_line(obj_nt, obj_nt)

def generate_correct_merge_without_isolated_node(src_nt, obj_nt,key_type_list=None,differet_type=False):
    print '去除孤立点'
    node_set, isolated_node_set = stats.find_isolated_nodes(src_nt,key_type_list,differet_type)
    print '一共有{:d}个节点'.format(len(node_set))
    print '一共有{:d}个孤立点'.format(len(isolated_node_set))
    line_list = []
    for i,line in enumerate(open(src_nt,'r')):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        _,sub_id = kg_op.get_node_type_id(sub)
        if sub_id in isolated_node_set:
            continue
        if 'property' not in pre:
            _,obj_id = kg_op.get_node_type_id(obj)
            if obj_id in isolated_node_set:
                continue
        line_list.append(line)
    print '一共有{:d}行'.format(i)
    print '剩余有{:d}行'.format(len(line_list))
    wf = open(obj_nt,'w')
    for line in line_list:
        wf.write(line)
    wf.close()


if __name__ == '__main__':
    generate_correct_merge('../data/merge.nt','../result/merge.nt.20171024')
    generate_correct_merge_without_isolated_node('../result/merge.nt.20171024','../result/merge.nt.20171024.without_isolated_node')



    # 统计孤立点
    # all_node_set, isolated_node_set = stats.find_isolated_nodes('../data/merge.nt')
    # print len(all_node_set),len(isolated_node_set)
    # relation_tree = stats.relation_tree('../data/merge.nt')
    # print relation_tree

