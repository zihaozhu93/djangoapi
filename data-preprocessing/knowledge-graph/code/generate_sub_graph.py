# coding=utf8
#########################################################################
# File Name: generate_sub_graph.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: Tue Oct 24 17:08:44 2017
#########################################################################

import os
from libs import stats, kg_op, py_op
from generate_correct_merge_nt import generate_correct_merge_without_isolated_node

def generate_sub_graph(src_nt,node_type_list,obj_nt,property_list,pre_relation_list):
    # 生成子图
    kg_op.generate_sub_graph(src_nt, node_type_list, obj_nt, property_list,pre_relation_list)
    # 去除孤立点
    generate_correct_merge_without_isolated_node(obj_nt,obj_nt,['disease','symptom'],True)
    generate_correct_merge_without_isolated_node(obj_nt,obj_nt)

def remap_bodypart(src_nt, src_csv, obj_nt):
    """
    重新标注过 ../data/initial_data/bodypart_remap.csv 之后
    删去其他部位，这些部位没有症状，只有疾病
    把remap的部位修改
    """
    print 'remap 部位'
    map_dict = dict()
    for line in open(src_csv):
        pre_id, pre_name, pos_id, pos_name = line.strip().split(',')
        if pre_id != pos_id:
            map_dict[pre_id] = pos_id
    line_list = []
    for line in open(src_nt,'r'):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        pre_type, pre_relation = kg_op.get_node_type_id(pre)
        sub_type, sub_id = kg_op.get_node_type_id(sub)
        if pre_type == 'property':
            if sub_id in map_dict:
                print line
            else:
                line_list.append(line)
        else:
            obj_type, obj_id = kg_op.get_node_type_id(obj)
            if obj_id in map_dict:
                pos_id = map_dict[obj_id]
                line = line.replace(obj_id,pos_id)
                if pos_id[-2:] == '00':
                    line = line.replace('SecBody','FirBody')
                else:
                    line = line.replace('FirBody','SecBody')
            line_list.append(line)
    with open(obj_nt,'w') as f:
        for line in line_list:
            f.write(line)

def remap_department(src_nt, src_csv, obj_nt):
    """
    重新标注过 ../data/initial_data/department_remap.csv 之后
    把remap的科室修改
    """
    print 'remap 科室'
    map_dict = dict()
    for line in open(src_csv):
        pre_id, pre_name, pos_id, pos_name = line.strip().split(',')
        if pre_id != pos_id:
            map_dict[pre_id] = pos_id
    line_list = []
    for line in open(src_nt,'r'):
        sub, pre, obj = kg_op.get_sub_pre_obj(line)
        pre_type, pre_relation = kg_op.get_node_type_id(pre)
        sub_type, sub_id = kg_op.get_node_type_id(sub)
        if pre_type == 'property':
            if sub_id in map_dict:
                print line.strip()
            else:
                line_list.append(line)
        else:
            obj_type, obj_id = kg_op.get_node_type_id(obj)
            if obj_id in map_dict:
                pos_id = map_dict[obj_id]
                line = line.replace(obj_id,pos_id)
                if pos_id[-2:] == '00':
                    line = line.replace('SecDep','FirDep')
                else:
                    line = line.replace('FirDep','SecDep')
            line_list.append(line)
    with open(obj_nt,'w') as f:
        for line in line_list:
            f.write(line)

def replace_cross_bodypart(src_nt, cross_id_list, src_csv_list, obj_nt):
    """
    删去四肢部位，改为上肢下肢
    删去生殖部位，改为男性生殖，女性生殖
    """
    print '去掉交叉部位'
    line_list = []
    # sym_body_dict = { }
    for src_csv in src_csv_list:
        for i,line in enumerate(open(src_csv,'r')):
            if not line.startswith('s'):
                raise NameError('Run error in {:s} {:d} line:\n{:s}'.format(src_csv,i,line))
            line = line.strip().strip(',')
            data = line.split(',')
            # check id name
            for j in range(len(data)):
                if j % 2 == 0:
                    id = data[j].strip()
                    name = data[j+1].strip()
                    if not kg_op.check_id_name(id,name):
                        # raise NameError('Run error in {:s} {:d} line:\n{:s}'.format(src_csv,j,line))
                        print('Run error in {:s} {:d} line: {:s}'.format(src_csv,i,line)) 
                        # continue
                    if j == 0:
                        # sym_body_dict[data[0]] = []
                        pass
                    else:
                        # sym_body_dict[data[0]] = .append(data[j])
                        line_list.append(kg_op.generate_line(data[0],data[j]))

    for line in open(src_nt,'r'):
        is_cross = 0
        for cross_id in cross_id_list:
            if cross_id in line:
                is_cross = 1
                break
        if not is_cross:
            line_list.append(line)


    with open(obj_nt,'w') as f:
        for line in line_list:
            f.write(line)

def main():
    without_isolated_node = '../data/middle_data/merge.nt.20171024.without_isolated_node'
    merge_sub = '../result/merge.nt.sub'

    # 先生成子图
    generate_sub_graph(
            src_nt = without_isolated_node,
            node_type_list = ['disease','symptom','bodypart','department'],
            obj_nt = merge_sub,
            property_list = ['type','name'],
            pre_relation_list = ['DisFirDep','DisSecDep','SymFirBody','SymSecBody','SymPosDis']
            )

    # remap 部位
    remap_bodypart(merge_sub,'../data/initial_data/bodypart_remap.csv',merge_sub)

    # remap 科室
    remap_department(merge_sub,'../data/initial_data/department_remap.csv',merge_sub)

    # 替换 四肢、生殖 等交叉部位
    replace_cross_bodypart(merge_sub, 
            cross_id_list=[
                'bp20400',
                'bp14800',
                ], 
            src_csv_list=[
                '../data/initial_data/生殖部位.csv',
                '../data/initial_data/四肢部位.csv',
                ], 
            obj_nt=merge_sub)
    # generate_correct_merge_without_isolated_node(merge_sub,merge_sub,['disease','symptom'],True)

def test():
    merge_sub = '../result/merge.nt.sub.tmp'

    dis_dep_dict = stats.stats_sub_obj_tree(merge_sub,'disease','department')
    print len(dis_dep_dict)
    print len(dis_dep_dict['linked_nodes'])
    num = 0
    for k,vs in dis_dep_dict['linked_nodes'].items():
        # vs = list(set([v[:-2] for v in vs]))
        for v in ['dp21300']:
            break
            if v in vs:
                vs.remove(v)
        vs = list(set([v[:-2] for v in vs if v not in ['dp21300']]))
        if len(vs) > 1:
            num += 1
    print num
    print len(dis_dep_dict['single_nodes'])
    py_op.mywritejson('../result/dis_dep_dict.json',dis_dep_dict)


if __name__ == '__main__':
    # main()
    merge_sub = '../result/merge.nt.sub'
    generate_correct_merge_without_isolated_node(merge_sub,merge_sub,['disease','symptom'],True)
