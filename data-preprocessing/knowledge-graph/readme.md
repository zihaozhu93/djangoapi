<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
# Make a new documentation for knowledge-graph data preprocessing
<li><a href="#t1">1. data needed</a>
<li><a href="#t2">2. code </a>
<ul>
<li><a href="#t21">2.1. Basic env:</a></li>
<li><a href="#t22">2.2. Static files:</a></li>
<li><a href="#t23">2.3. API docs:</a>
<ul>
</li>
</ul>
</li>
</ul>
</div>
</div>


<a id="t1"></a>

# 1. data needed
-	原始文件：
	data/merge.nt (2017.4)
	data/bodypart_remap.csv
-	中间文件
	data/merge.nt.20171024 (2017.10.24 重新清洗一遍数据)
	data/merge.nt.20171024.without_isolated_nodes (2017.10.24 去除孤立点)

# 2. code

<a id="t21"></a>

## 2.1. code libs:



标注细节：
	1. 新建一个表，放确定融合节点
	2. 确定后原来的表中节点可以去掉
	3. 不确定的留在原来的表中
