# coding=utf8
#########################################################################
# File Name: generate_quyiyuan_json.py
# Author: ccyin
# mail: ccyin04@gmail.com
# Created Time: 2017年12月25日 星期一 16时48分35秒
#########################################################################

import json
import os
import sys
sys.path.append('../../knowledge-graph/code/libs/')
import py_op


def generate_id_name_dict(src_json,id_name_dict_json, symptom_disease_dict_json):
    '''
    注意: 男性,女性,儿童 还要作区分
    '''
    quyiyuan_dict = json.load(open(src_json))
    quyiyuan_dict = quyiyuan_dict[u'女性']
    num_symptom = 100
    id_name_dict = dict()
    symptom_disease_dict = dict()
    for bodypart, bodypart_dict in quyiyuan_dict.items():
        for symptom, symptom_dict in bodypart_dict.items():
            symptom_name = symptom
            if symptom_name not in id_name_dict.values():
                symptom_id = 's' + str(num_symptom)
                num_symptom += 1
                id_name_dict[symptom_id] = symptom_name
                if symptom_id not in symptom_disease_dict:
                    symptom_disease_dict[symptom_id] = dict()
            for sub_symptom, disease_list in symptom_dict.items():
                # symptom_name = '{:s}_{:s}'.format(symptom, sub_symptom)
                # symptom_id = 's' + str(num_symptom)
                # num_symptom += 1
                # id_name_dict[symptom_id] = symptom_name
                # if symptom_id not in symptom_disease_dict:
                #     symptom_disease_dict[symptom_id] = dict()
                for disease_dict in disease_list:
                    disease_id = 'd'+disease_dict['diseaseCode']
                    disease_name = disease_dict['diseaseName']
                    disease_score = float(disease_dict['correlation'].replace('%',''))
                    id_name_dict[disease_id] = disease_name
                    if symptom_disease_dict.get(symptom_id,{ }).get(disease_id):
                        symptom_disease_dict[symptom_id][disease_id] = max(symptom_disease_dict[symptom_id][disease_id], disease_score)
                    else:
                        symptom_disease_dict[symptom_id][disease_id] = disease_score
    py_op.mywritejson(id_name_dict_json, id_name_dict)
    py_op.mywritejson(symptom_disease_dict_json, symptom_disease_dict)

def generate_accompany_symptom(src_json, id_name_dict_json, accompany_symptom_json):
    '''
    注意: 男性,女性,儿童 还要作区分
    '''
    quyiyuan_dict = json.load(open(src_json))
    quyiyuan_dict = quyiyuan_dict[u'女性']
    id_name_dict = json.load(open(id_name_dict_json))
    name_id_dict = { v:k for k,v in id_name_dict.items() }
    accompany_symptom_dict = dict()

    for bodypart, bodypart_dict in quyiyuan_dict.items():
        for symptom, accompany_symptom_list in bodypart_dict.items():
            sid = name_id_dict[symptom]
            for accompany_symptom in  accompany_symptom_list:
                if accompany_symptom not in name_id_dict:
                    print accompany_symptom
                    continue
                asid = name_id_dict[accompany_symptom]
                if sid not in accompany_symptom_dict:
                    accompany_symptom_dict[sid] = []
                if asid not in accompany_symptom_dict[sid]:
                    accompany_symptom_dict[sid].append(asid)
    py_op.mywritejson(accompany_symptom_json, accompany_symptom_dict)




def main():
    generate_id_name_dict('../data/quyiyuan.json','../result/quyiyuan_id_name_dict.json','../result/quyiyuan_symptom_disease_dict.json')
    generate_accompany_symptom('../data/quyiyuan_symptom.json','../result/quyiyuan_id_name_dict.json','../result/quyiyuan_accompany_symptom_dict.json')

if __name__ == '__main__':
    main()
