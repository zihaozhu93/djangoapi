<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#t1">1. Make a new documentation for Django API</a>
<ul>
<li><a href="#t11">1.1. Basic env:</a></li>
<li><a href="#t12">1.2. Static files:</a></li>
<li><a href="#t13">1.3. API docs:</a>
<ul>
<li><a href="#t131">1.3.1. adr</a></li>
<li><a href="#t132">1.3.2. clinicalpath</a></li>
<li><a href="#t133">1.3.3. kgInterface</a></li>
<li><a href="#t134">1.3.4. queryShow</a></li>
<li><a href="#t135">1.3.5. medknowledge</a></li>
<li><a href="#t136">1.3.6. usdata</a></li>
</ul>
<li><a href="#t14">1.4. Kong API gateway:</a>
<li><a href="#t2">1. Make a new documentation for tools</a>
<li><a href="#t21">2.1. java:</a>
<li><a href="#t22">2.2. elastic search:</a>
<li><a href="#t23">2.3. rdf3x:</a>
<li><a href="#t24">2.4. mysql:</a>
<li><a href="#t25">2.5. mongodb:</a>
<li><a href="#t26">2.6. django:</a>
</li>
</ul>
</li>
</ul>
</div>
</div>


<a id="t1"></a>

# 1. Make a new documentation for Django API


<a id="t11"></a>

## 1.1. Basic env:

-   mysql 5.6
-   mongodb 3.4
-   rdf3x
-   virtualenv
-   django 1.8.6
-	java
-	elasticsearch


<a id="t12"></a>

## 1.2. Static files:

static files path is: 
-	*/var/www/djangoapi/file/* 
-	*data-preprocessing/knowledge-graph/data/*


<a id="t3"></a>

## 1.3 API docs:


<a id="t131"></a>

### adr

-   adr/getMapTree/
    - 功能: 查询指定id药品或药物相关的的所有不良反应,返回格式{nodename,nodetype,nodeparent}
    - 调用: GET
    - 输入: dxxxx or mxxxx(d药品或m药物id)
    - 输出: [{n1name,n1type,n1parent},...,]
    - 耗时: 0.2s
-   adr/getDrug
	- 功能: 查询指定id病人及其所患疾病,返回符合该病人的推荐药物
	- 调用: GET
    - 输入: 如: {"Disease":"感冒","Pid":1}
    - 输出: 病人基本信息,疾病史,健康状态;可治疗该疾病的所有推荐药物
    - 耗时: 0.5s

##### 未使用api
-   adr/getInfoList/
-   adr/getInfo/
-   adr/getMapList/
-   adr/getMap/
-   adr/getTaboo/
-   adr/getPids
-   adr/getXids

<a id="t132"></a>

### clinicalpath

-   clinicalpath/queryItemsTime
	- 功能: 根据选择输待查询病人id,返回该病人的所有临床路径事件的时间及关键时间
	- 调用: GET
	- 输入: {"PID":"ZY010001104143"}
	- 输出: 返回该病人的临床路径事件的时间及病人入出院及手术时间
	- 耗时: 0.3s
-   medcareFee/getUserItems
	- 功能: 根据选择输待查询病人id,返回该病人的所有临床路径中开具的药品和非药品清单
	- 调用: GET
	- 输入: {"PID":"ZY010001102284"}
	- 输出: 返回该病人的临床路径清单中各个条目id,名称,费用,得分,状态,原因和备注
	- 耗时: 0.2s
-   medcareFee/updateUserItems
	- 功能: 根据每个路径元素的具体状态,对于处于可修改状态的元素进行修改,返回修改状态
	- 调用: GET
	- 输入: {"PID":"ZY010001102284","IID":"F00000144169","Status":"rejected","Comment":""}
	- 输出: 返回该路径元素修改状态
	- 耗时: 0.2s

##### 未使用api
-   clinicalpath/getUserInfor
-   clinicalpath/queryCP
-   clinicalpath/queryScore
-   clinicalpath/queryScoreAmount
-   clinicalpath/queryItem
-   clinicalpath/queryPatients
-   clinicalpath/queryRawStat
-   medcareFee/queryPatients
-   medcareFee/queryItem

<a id="t133"></a>

### kgInterface

-   getNodesInfo
	- 功能: 输入待查询node名称,返回与该node相连的点和边
	- 调用: POST
	- 输入: {"filterbydomain":"","filterbyfirst":"","filterbyfclass":"","filterbysclass":"","filterbyfbody":"","filterbysbody":"","filterbynumber":"10"}
	- 输出: 返回所有和该node相连的node及边
	- 耗时: 0.8s
-   getDisOut
	- 功能: 输入一个或多个待查询node名称,返回与这些node相连的疾病和所属关系
	- 调用: POST
	- 输入: {"AND":["s11243","s10415"],"NOT":[""]}
	- 输出: 返回所有和这些node相连的疾病node及边
	- 耗时: 3.8-15s
-   getMedOut
	- 功能: 输入一个或多个待查询node名称,返回与这些node相连的药品和所属关系
	- 调用: POST
	- 输入: {"AND":["s11243","s10415"],"NOT":[""]}
	- 输出: 返回所有和这些node相连的药品node及边
	- 耗时: 3.8-15s
-   getDisRelation
	- 功能: 输入一个或多个待查询node id, 返回与这些node相连的边所属关系
	- 调用: POST
	- 输入: {"DIDS":["d37185","d41501",...]}
	- 输出: 返回所有和这些node相连的边
	- 耗时: 20s
	- *备注*: 考虑到查询速度,目前只查询返回10个node的边的关系
-   getMedRelation
	- 功能: 输入一个或多个待查询node名称,返回与这些node相连的药品和所属关系
	- 调用: POST
	- 输入: {"AND":["s11243","s10415"],"NOT":[""]}
	- 输出: 返回所有和这些node相连的药品node及边
	- 耗时: 20s
	- *备注*: 考虑到查询速度,目前只查询返回10个node的边的关系	
-   kgInterface/getBodypart
	- 功能: 输入可能部位名称,返回相关的部位和id
	- 调用: GET
	- 输入: {"Name":"头部"}
	- 输出: 返回相关的部位和id
-   kgInterface/getSymId
	- 功能: 输入可能症状名称,返回相关的症状和id
	- 调用: GET
	- 输入: {"Name":"感冒"}
	- 输出: 返回相关的症状和id
-   kgInterface/getSymDis
	- 功能: 输入症状id的list,返回可能疾病的名称和id
	- 调用: GET
	- 输入: {"Ids":["s15670"],"NotIds":[],"UnknownIds":[]}
	- 输出: 返回可能疾病的名称和id
-   kgInterface/getSymMed
	- 功能: 输入症状id的list,返回可能对应药品的名称和id
	- 调用: GET
	- 输入: {"Sids":["d53737","d43592","d43592"],"NotSids":[],"Tids":[],"NotTids":[],"Age":""}
	- 输出: 返回可能对应药品的名称和id


<a id="t134"></a>

### 1.3.4 queryShow

-   getQueryOut
	- 功能: 点击搜索框,返回10条供搜索的候选项
	- 调用: POST
	- 输入: {"filterbydomain":"","filterbyfirst":"","filterbyfclass":"","filterbysclass":"","filterbyfbody":"","filterbysbody":"","filterbynumber":"10"}
	- 输出: 返回10条候选项名字和id
	- 耗时: 0.08s


<a id="t135"></a>

### 1.3.5 medknowledge

-   medknowledge/op
	- 功能: 搜索键入的类型和名称,返回其具体的内容
	- 调用: GET
	- 输入: {"Table":"disease","Id":"d41310"}
	- 输出: 返回查询字段的具体内容
	- 耗时: 0.3s
-   medknowledge/zhongyiop
	- 功能: 返回中医应用下的四个子栏目名称,类别和具体细分类
	- 调用: GET
	- 输入: {"Table":"Zhongyi","Id":"z0000000"}
	- 输出: {中药检索:{…},药膳食疗:{…},辨证论治:{…},针灸推拿:{…}}
	- 耗时: 0.14s
-   medknowledge/zhongyilist
	- 功能: 返回首页默认展示中医条目
	- 调用: GET
	- 输入: {"Table":"Zhongyi","Id":"z0000000","Sort_item":"Name","Start":0,"End":15,"Filter":{"class1":"辨证论治","class2":"中医妇科","class3":"overview","class4":"","class5":""}}
	- 输出: 返回中医妇科下的全部条目
	- 耗时: 0.14s
-   medknowledge/imageop
	- 功能: 搜索指定图片id,返回图片
	- 调用: GET
	- 输入: {"Iid":"i16592"}
	- 输出: 返回该id对应图片
	- 耗时: 0.03s
-   medknowledge/videoop
	- 功能: 搜索指定的视频id,返回视频
	- 调用: GET
	- 输入: {"Table":"Video","Vid":"v1002118"}
	- 输出: 返回该id对应视频
	- 耗时: ---
	- *备注*: 部分视频无法播放
-   medknowledge/pdfop
	- 功能: 搜索指定的pdfid,返回pdf
	- 调用: GET
	- 输入: {"Table":"Pdf","Pid":"o1755143"}
	- 输出: 返回该id对应pdf文件
	- 耗时: ---
-   medknowledge/list
	- 功能: 查询指定类型,返回该类型下20条数据作为首页展示条目
	- 调用: GET
	- 输入: {"Table":"Symptom","Start":0,"Offset":20,"SortItem":"","Filter":{}}
	- 输出: 返回20个数据条目,每个条目包含一些简单基本信息
	- 耗时: 0.1s
-   medknowledge/search
	- 功能: 搜索键入的疾病名,返回相似的疾病条目
	- 调用: GET
	- 输入: {"QueryString":"雁头","Start":0,"Offset":10}
	- 输出: 返回10条相关的疾病名称,概述,临床表现
	- 耗时: 0.3s
-   medknowledge/auto
	- 功能: 根据键入的关键字或词,elasticsearch查询返回最相近的10个疾病名
	- 调用: GET
	- 输入: {"QueryString":"头"}
	- 输出: {Return: 0, Results: ["雁头", "头痛", "头沉", "头晕", "头昏", "鸭头", "百头", "鸿头", "鸡头", "小头"]}
	- 耗时: 0.1s

##### 未使用api
-   medknowledge/tab
-   medknowledge/query
-   medknowledge/fsfileop


<a id="t136"></a>

### 1.3.6 usdata

-   usdata/riskScore
	- 功能: 根据选择搜索的病人id, 返回该病人可能会患的疾病top10及预测概率
	- 调用: GET
	- 输入: {"PID":945209345}
	- 输出: 该病人的top10预测疾病及概率
	- 耗时: 0.4s
-   usdata/featureImportance
	- 功能: 根据选择搜索的病人id, 返回该病人可能会患的疾病top10中各项因素及其致病概率
	- 调用: GET
	- 输入: {"PID":945209345}
	- 输出: 该病人的top10预测疾病中各项因素名称及其致病概率
	- 耗时: 0.3s
-   usdata/similarPatientMedication
	- 功能: 根据选择搜索的病人id, 返回与该病人的相似的病人top10的所有药物信息
	- 调用: GET
	- 输入: {"PID":945209345}
	- 输出: 按cohort划分相似病人组,返回相似病人的基本信息及相似度,以及药物信息
	- 耗时: 3s
-   patientQuery
	- 功能: 根据选择搜索的病人id, 返回该病人的所有记录信息
	- 调用: GET
	- 输入: {"PID":945209345}
	- 输出: 该病人的诊断,检验,药物,手术记录信息
	- 耗时: 0.3s
-   PatientSimilarity
	- 功能: 根据选择搜索的病人id, 返回与该病人的相似的病人top10的所有记录信息
	- 调用: GET
	- 输入: {"PID":945209345}
	- 输出: 按cohort划分相似病人组,返回相似病人的基本信息及相似度
	- 耗时: 6s

##### 未使用api
-   usdata/record
-   usdata/tab
-   usdata/demograph
-   usdata/risk
-   usdata/getPatient
-   simPatientQuery


<a id="t14"></a>

## 1.4 Kong API gateway:

-   kong version: 0.9.7
-   kong Apis:
	- http://59.110.52.133:9999/ 下的所有api
	- 前端调用访问地址: http://59.110.52.133:8000/
-   Api plugins:
	- key-auth: 所有请求需要在请求头加上apikey, get 请求 http://59.110.52.133:8001/consumers/admin/key-auth/ 返回的是一个json, key就是后面要用到的apikey
	- cors: 保证跨域请求
-   my_auth:
	- djangoapi应用中增加一个用户token验证装饰器,需要验证token的api在函数定义上一行加上装饰器即可, 如:  
	`@csrf_exempt`  
	`@auth_required`  
	`def getInfo(request):`  
	`	 ...`
-   kong 启动
	- sudo kong start
-   kong 启动状态查看
	- sudo kong health	



<a id="t2"></a>

# 2. Make a new documentation for tools


<a id="t21"></a>

## 2.1 java:

    - 目录：/home/cc/work/java/


<a id="t22"></a>

## 2.2 elastic search:

-   目录：/home/cc/work/elasticsearch-2.3.5/

-   启动：

        cd /home/cc/work/elasticsearch-2.3.5/

        ./elasticsearch    //前台启动

        ./elasticsearch -d //后台启动

-   *备注*:	需要java环境支持

-   *备注*:	es更换环境时，把整个目录copy一份即可，不需要重新插入es

-	设置用户,外网访问


<a id="t23"></a>

## 2.3 rdf3x

知识图谱:

-   参考链接： http://www.mamicode.com/info-detail-1455707.html

-   目录：/var/rdf3x/merge.nt


<a id="t24"></a>

## 2.4 mysql:

-   进入数据库, 输入密码

		mysql -u root -p

-   创建 utf-8 格式的数据库 yiliao

        mysql> CREATE DATABASE yiliao DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

-   查看

        mysql> show databases;

-   设置MySQL路径在SSD上，访问更快

		查看文件存放路径:

			mysql> show global variables like "%datadir%";

		更改文件存放路径(如下是一个示例):

			- 先把mysql停掉

		    service mysql stop

			- 修改路径

			vim /etc/mysql/my.cnf

				- 更改如下一行:

				datadir		= /var/lib/mysql

				datadir		= /var/lib/mysql_new

			- 复制原目录的数据到新目录下

			*注意*:	使用cp操作，不要使用mv操作，以免操作以外终止时文件损坏

			cp -r /var/lib/mysql /var/lib/mysql_new

			- 重启mysql

		    service mysql start

		    - 检查数据库正常可删除原有数据

		    rm -r /val/lib/mysql

-   设置mysql用户,外网访问时使用

        grant all privileges on yiliao.* to 'yiliao'@'%' identified by 'drcubic_8888';
		grant all privileges on health.* to 'yiliao'@'%' identified by 'drcubic_8888';
		grant all privileges on brain.* to 'yiliao'@'%' identified by 'drcubic_8888';
		flush privileges;

<a id="t25"></a>

## 2.5 mongodb:

-   参考链接 ：

		https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

		https://segmentfault.com/q/1010000010171614?sort=created

-   启动mongodb

		sudo service mongod start
	
-   确认mongodb成功启动

		ps -ef | grep mongod
	
-   停止mongodb

		sudo service mongod stop
	
-   删除mongodb文件以及日志

		sudo rm -r /var/log/mongodb

		sudo rm -r /var/lib/mongodb

-   mongodb 与 mysql不同，无需建立database

-   mongodb 用于存储大文件，占用大量磁盘

-   设置mongodb路径在普通磁盘上，容量更大

		- 先把mongodb停掉

		- 修改路径

		vim /etc/mongod.cnf

			- 若配置文件不在这个地方，用如下命令寻找

				ps -ef | grep mongod

			- 更改如下一行:

			storage:

				dbPath: /var/lib/mongodb

				dbPath: /var/lib/mongodb_new

		- 复制原目录的数据到新目录下

		*注意*:	使用cp操作，不要使用mv操作，以免操作以外终止时文件损坏

		cp -r /var/lib/mongod /var/lib/mongodb_new

		- 重启mongod

		- 检查数据库正常可删除原有数据

		rm -r /val/lib/mongodb

-	mongodb外网IP访问
    1. 修改配置文件 /etc/mongod/conf
		- 注释掉这一行:            
                # bindIp: 127.0.0.1
		- 注释如下两行:
				# security:
				#     authorization: "enabled"
	2. 重启mongod
            sudo service mongod restart
    3. 创建mongo用户
            mongo
			use mongodatabase_name
            db.createUser({user:"username", pwd:"passward", roles:["read"]})
			# db.createUser({user:"username", pwd:"passward", roles:["readWrite"]})
	4. 添加如下两行:
	    	security:
                authorization: "enabled"
	5. 重启mongod
            sudo service mongod restart
    6. 参考博客
            https://blog.csdn.net/gurenyuan123/article/details/53334532
            http://www.jb51.net/article/48217.htm
            https://blog.csdn.net/u010523770/article/details/54599548
            https://www.linuxidc.com/Linux/2016-08/133774.htm
    


<a id="t26"></a>

## 2.6 django:

-   安装：
		
		source djangoapi-env

		pip install django==1.8.6


-   启动django

	Apache启动: 并发性更好

		sudo service apache2 start

	runserver启动: 仅用于调试
	
		python manage.py runserver ip:port
	
-   查看日志

		sudo vim /var/log/apache2/error.log
	
