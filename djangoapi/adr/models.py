# -*-coding: utf-8 -*-
from django.db import models

# Create your models here.


class AdvReaction(models.Model):
    Xid = models.CharField(max_length=8, null=False,
                           blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=128, null=True,
                            blank=True, unique=False, db_index=True)
    Ename = models.CharField(max_length=64, null=True,
                             blank=True, unique=False, db_index=True)
    Source = models.CharField(
        max_length=16, null=False, blank=False, unique=False, db_index=True)

    class Meta:
        unique_together = (('Name', 'Ename'),)


class Drug(models.Model):
    Xid = models.CharField(max_length=8, null=False,
                           blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=128, null=True,
                            blank=True, unique=False, db_index=True)
    Gname = models.CharField(max_length=128, null=True,
                             blank=True, unique=False, db_index=True)
    Manufacturer = models.CharField(
        max_length=128, null=True, blank=True, unique=False, db_index=True)
    Source = models.CharField(
        max_length=8, null=False, blank=False, unique=False, db_index=True)

    # added infomation
    Category = models.CharField(
        max_length=128, null=True, blank=True, unique=False, db_index=True)

    ##################################################
    ##################################################
    ##################################################
    # use these two columns or classes : Indication,taboo
    Taboo = models.CharField(max_length=4096, null=True,
                             blank=True, unique=False, db_index=False)
    Indication = models.CharField(
        max_length=2048, null=True, blank=True, unique=False, db_index=False)
    ##################################################
    ##################################################
    ##################################################

    # class Meta:
        # unique_together = (('Name', 'Gname', 'Manufacturer'),)


class Medication(models.Model):
    Xid = models.CharField(max_length=8, null=False,
                           blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=True,
                            blank=True, unique=False, db_index=True)
    Ename = models.CharField(max_length=256, null=True,
                             blank=True, unique=False, db_index=True)
    Source = models.CharField(
        max_length=16, null=False, blank=False, unique=False, db_index=True)

    class Meta:
        unique_together = (('Name', 'Ename'),)

# class Indication(models.Model):
#     Xid = models.CharField(max_length=8,null=False,blank=False,unique=True,db_index=True)
#     Name = models.CharField(max_length=256,null=False,blank=False,unique=True,db_index=True)
# class Taboo(models.Model):
#     Xid = models.CharField(max_length=8,null=False,blank=False,unique=True,db_index=True)
#     Name = models.CharField(max_length=256,null=False,blank=False,unique=True,db_index=True)


class MapAdrDrugMed(models.Model):
    Aid = models.CharField(max_length=8, null=True,
                           blank=True, unique=False, db_index=True)
    Did = models.CharField(max_length=8, null=True,
                           blank=True, unique=False, db_index=True)
    Mid = models.CharField(max_length=8, null=True,
                           blank=True, unique=False, db_index=True)
    # Iid = models.CharField(max_length=8,null=True,blank=True,unique=False,db_index=True)
    # Tid = models.CharField(max_length=8,null=True,blank=True,unique=False,db_index=True)

    class Meta:
        unique_together = (('Aid', 'Did'), ('Aid', 'Mid'), ('Did', 'Mid'),)
        # unique_together = (('Aid','Did'),('Aid','Mid'),('Did','Mid'),('Did','Iid'),('Did','Tid'),)


class TabooRef(models.Model):
    Tid = models.CharField(max_length=8, null=False,
                           blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=4096, null=False,
                            blank=False, unique=False, db_index=False)

    # class Meta:
    #     unique_together = ('Tid', 'Name')


class DrugTaboo(models.Model):
    Did = models.CharField(max_length=8, null=True,
                           blank=True, unique=False, db_index=True)
    Tid = models.CharField(max_length=8, null=True,
                           blank=True, unique=False, db_index=True)

    class Meta:
        unique_together = (('Did', 'Tid'),)


##########################################################################
##########################################################################
##########################################################################
# use MapAdrDrugMed or these three class
# class MapAdrDrug(models.Model):
#     Aid	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     Did	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     class Meta:
#         unique_together = ((Aid,Did),)
#
# class MapAdrMed(models.Model):
#     Aid	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     Mid	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     class Meta:
#         unique_together = ((Aid,Mid),)
#
# class MapDrugMed(models.Model):
#     Did	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     Mid	= models.CharField(max_length=8,null=False,blank=False,unique=False,db_index=True)
#     class Meta:
#         unique_together = ((Mid,Did),)
##########################################################################
##########################################################################
##########################################################################
