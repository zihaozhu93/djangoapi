# coding=utf-8
from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import traceback
from datetime import datetime
from adr.models import AdvReaction, Drug, Medication, MapAdrDrugMed, DrugTaboo, TabooRef
from pyweed import WeedFS
from StringIO import StringIO
from collections import OrderedDict
import string
import sys
from my_auth.views import auth_required
reload(sys)
sys.setdefaultencoding('utf8')

useless_list = ["", u"", None, False, " "]

dobject = {
    'AdvReaction': AdvReaction,
    'Drug': Drug,
    'Medication': Medication,
    'MapAdrDrugMed': MapAdrDrugMed,
    'DrugTaboo': DrugTaboo,
    'TabooRef': TabooRef,

}


@csrf_exempt
@auth_required
def getInfo(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            table = input_dict['Table']
            xid = input_dict['Id']
            json_content = {}
            record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            exec(record_info)
            if record_info:
                record_info = record_info.values()[0]
                for key in record_info.keys():
                    if record_info[key] not in useless_list:
                        if key == 'id':
                            continue
                        elif key == 'Xid':
                            json_content['Id'] = record_info[key]
                        else:
                            json_content[key] = record_info[key]
                json_out["Results"] = json_content
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            input_dict.pop('Table')
            xid = input_dict['Xid']
            record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            exec(record_info)
            if record_info:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} already exists.'.format(
                    table, xid)
            else:
                n = dobject[table]()
                for key in input_dict:
                    if input_dict[key] not in useless_list:
                        # insert in database
                        value = input_dict[key]
                        setattr(n, key, value)
                n.save()
                json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "PUT":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            input_dict.pop('Table')
            xid = input_dict['Xid']
            record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            exec(record_info)
            if record_info:
                # n = dobject[table]()
                n = record_info[0]
                for key in input_dict:
                    if input_dict[key] not in useless_list:
                        # insert in database
                        value = input_dict[key]
                        setattr(n, key, value)
                n.save()
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "DELETE":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            xid = input_dict['Xid']
            record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            exec(record_info)
            if record_info:
                record_info[0].delete()
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getInfoList(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            table = input_dict['Table']

            record_count = 'reccord_count = {:s}.objects.count()'.format(table)
            exec(record_count)
            record_list = 'record_list = {:s}.objects.order_by("Xid")[0:min(record_count,100)].values()'.format(
                table)
            exec(record_list)

            json_out['Results'] = []
            for record_info in record_list:
                json_content = {}
                for key in record_info.keys():
                    if record_info[key] not in useless_list:
                        if key == 'id':
                            continue
                        elif key == 'Xid':
                            json_content['Id'] = record_info[key]
                        else:
                            json_content[key] = record_info[key]
                json_out['Results'].append(json_content)
            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getMap(request):
    if request.method == "GET":
        json_out = {}
        try:
            # input_dict = json.loads(request.GET["q"])
            # # table= input_dict['Table']
            # table = 'MapAdrDrugMed'
            # record_infos = 'record_infos = {:s}.objects'.format(table)
            # xid = ''
            # for k, v in input_dict.items():
            #     if 'id' in k.lower():
            #         key = v[0] + 'id'
            #         record_infos = '{:s}.filter({:s}="{:s}")'.format(
            #             record_infos, key.title(), v)
            #         xid = '{:s}_{:s}'.format(xid, v).strip('_')
            # exec(record_infos)
            # if record_infos:
            #     # record_info = record_infos.values()[0]
            #     json_list = []
            #     for record_info in record_infos.values():
            #         json_content = {}
            #         for key in record_info.keys():
            #             if record_info[key] not in useless_list:
            #                 if key == 'id':
            #                     continue
            #                 else:
            #                     json_content[key] = record_info[key]
            #         json_list.append(json_content)
            input_dict = json.loads(request.GET["q"])
            table = 'MapAdrDrugMed'
            record_infos = 'record_infos = {:s}.objects'.format(table)
            xid = ''
            for k, v in input_dict.items():
                if 'id' in k.lower():
                    key = v[0] + 'id'
                    record_infos = '{:s}.filter({:s}="{:s}")'.format(
                        record_infos, key.title(), v)
                    xid = '{:s}_{:s}'.format(xid, v).strip('_')
                elif 'Table' in k:  # to specify the map table for contents
                    table_flag = v[0]  # 'A','D' or 'M'
            exec(record_infos)
            if record_infos:
                # record_info = record_infos.values()[0]
                json_list = []
                for record_info in record_infos.values():
                    json_content = {}
                    for key in record_info.keys():
                        if record_info[key] not in useless_list:
                            if key == 'id':
                                continue
                            else:
                                json_content[key] = record_info[key]
                    if table_flag + 'id' in json_content.keys():
                        json_list.append(json_content)
                json_out["Results"] = json_list[:min(100,len(json_list))]
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            # table= input_dict['Table']
            table = 'MapAdrDrugMed'
            json_content = {}
            record_info = 'record_info = {:s}.objects'.format(table)
            xid = ''
            for k, v in input_dict.items():
                if 'id' in k.lower():
                    key = v[0] + 'id'
                    record_info = '{:s}.filter({:s}=\"{:s}\")'.format(
                        record_info, key.title(), v)
                    xid = '{:s}_{:s}'.format(xid, v).strip('_')
            print record_info
            exec(record_info)

            if record_info:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} already exists.'.format(
                    table, xid)
            else:
                n = dobject[table]()
                for key in input_dict:
                    if input_dict[key] not in useless_list:
                        # insert in database
                        value = input_dict[key]
                        setattr(n, key, value)
                n.save()
                json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "PUT":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            input_dict.pop('Table')
            xid = input_dict['Xid']
            record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            exec(record_info)
            if record_info:
                n = dobject[table]()
                for key in input_dict:
                    if input_dict[key] not in useless_list:
                        # insert in database
                        value = input_dict[key]
                        setattr(n, key, value)
                n.save()
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "DELETE":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            # table = input_dict['Table']
            # xid = input_dict['Xid']
            # record_info = 'record_info=' + table + '.objects.filter(Xid=xid)'
            table = 'MapAdrDrugMed'
            xid = input_dict['Did']  # delete the drug records
            record_info = 'record_info=' + table + '.objects.filter(Did=xid)'
            exec(record_info)
            if record_info:
                record_info[0].delete()
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getMapList(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            # table=input_dict['Table']
            table = 'MapAdrDrugMed'

            record_count = 'reccord_count = {:s}.objects.count()'.format(table)
            exec(record_count)
            record_list = 'record_list = {:s}.objects.order_by("Aid","Did","Mid")[0:min(record_count,100)].values()'.format(
                table)
            exec(record_list)

            json_out['Results'] = []
            for record_info in record_list:
                json_content = {}
                print record_info
                for key in record_info.keys():
                    if record_info[key] not in useless_list:
                        if key == 'id':
                            continue
                        else:
                            json_content[key] = record_info[key]
                json_out['Results'].append(json_content)
            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getDisDrug(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])

            # drug_all
            disease = input_dict['Disease']
            drugList = Drug.objects.filter(
                Indication__icontains=disease).order_by("Xid").values()
            # drugList = Drug.objects.filter(Indication__icontains=disease).order_by("Xid").values()[:3]
            # drugList = Drug.objects.filter(Indication__icontains=disease).order_by("Xid").values()[:5]
            drugAllSet = set([d['Xid'] for d in drugList])
            drugAllDict = {}

            # drug_without_adr
            adrList = input_dict['AdrList']
            adrDrug = [x for x in adrList if x[0] == 'd']
            adrMed = [x for x in adrList if x[0] == 'm']
            adrMedMap = MapAdrDrugMed.objects.filter(
                Mid__in=adrMed).filter(Did__icontains='d').values()
            adrMedDrugSet = set([d['Did'] for d in adrMedMap])
            drugWithoutAdrSet = drugAllSet - set(adrDrug) - adrMedDrugSet
            drugWithoutAdrDict = {}

            # drug_with_hurt
            drugWithHurtSet = set([d['Xid'] for d in drugList if d[
                                  'Taboo'] not in useless_list])
            drugWithHurtDict = {}

            for drugSet, drugDict in [[drugAllSet, drugAllDict], [drugWithoutAdrSet, drugWithoutAdrDict], [drugWithHurtSet, drugWithHurtDict]]:
                for d in drugList:
                    xid = d['Xid']
                    if xid in drugSet:
                        drugDict[xid] = d

            json_out['Results'] = {
                'drug_all': drugAllDict,
                'drug_without_adr': drugWithoutAdrDict,
                'drug_with_hurt': drugWithHurtDict
            }
            json_out['Num'] = [len(drugAllSet), len(
                drugWithoutAdrSet), len(drugWithHurtSet)]
            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

        
@csrf_exempt
@auth_required
def getPids(request):
    if request.method == "GET":
        json_out = {}
        try:
            pids = []
            patient_file = json.load(
                open('/var/www/djangoapi/file/patient_taboo.json', 'r'))
            for index, [xid, pname, diagnosis, disHist, drugAdr, medAdr, groups, healthStatus] in enumerate(patient_file):
                pids.append(int(xid))
            json_out['Results'] = {}
            json_out['Results']['Pids'] = pids
            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out['Return'] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

        
@csrf_exempt
@auth_required
def getXids(request):
    if request.method == "GET":
        json_out = {}
        try:
            xids = {}
            xids['Drug'] = map(lambda x: x['Xid'],
                               Drug.objects.values("Xid").distinct())
            xids['Med'] = map(lambda x: x['Xid'],
                              Medication.objects.values("Xid").distinct())
            xids['Adr'] = map(lambda x: x['Xid'],
                              AdvReaction.objects.values("Xid").distinct())

            json_out['Results'] = xids
            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out['Return'] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getDrug(request):
    if request.method == "GET":
        json_out = {}
        json_out['Results'] = []
        try:
            disease = '感冒'
            # pid = 1
            input_dict = json.loads(request.GET["q"])
            # disease = input_dict['Disease']
            pid = int(input_dict['Pid'])
            drugAll = Drug.objects.filter(
                Indication__icontains=disease).order_by("Xid").values()
            f = open('/var/www/djangoapi/file/patient_taboo.json', 'r')
            patient_taboo = json.loads(f.read())
            f.close()

            # for pname,adrList,checkList in patient_taboo:
            for index, [xid, pname, diagnosis, disHist, drugAdr, medAdr, groups, healthStatus] in enumerate(patient_taboo):
                # if index != 2:
                if int(xid) != int(pid):
                    continue
                drugUseList = []
                drugTabooList = []
                drugCheckList = []
                json_content = OrderedDict()
                json_content['Pid'] = pid
                json_content['Name'] = pname
                json_content['Diagnosis'] = diagnosis
                json_content['DiseaseHistory'] = disHist
                json_content['DrugAdr'] = drugAdr
                json_content['MedicationAdr'] = medAdr
                json_content['Groups'] = groups
                json_content['HealthStatus'] = healthStatus
                adrList = medAdr + drugAdr
                checkList = [u'肝肾功能']
                json_out['Results'] = {}
                json_out['Results']['PatientInfo'] = json_content
                dnewlist = []
                for d in drugAll:
                    taboo = d['Taboo']
                    xid = d['Xid']
                    if taboo is None:
                        continue
                    if u'任何' in taboo or u'任一' in taboo or u'任一' in taboo:
                        continue
                    if u'过敏者禁用' == taboo or u'成分过敏者禁用' == taboo:
                        continue
                    dnew = OrderedDict()
                    dnewlist.append(dnew)
                    for k in ['Xid', 'Name', 'Gname', 'Manufacturer', 'Indication', 'Taboo']:
                        kdict = {
                            'Xid': 'Xid',
                            'Name': '名字',
                            'Gname': '通用名',
                            'Manufacturer': '生产厂商',
                            'Indication': '适应症',
                            'Taboo': '禁忌症',
                        }
                        # dnew[kdict[k]] = d[k]
                        dnew[k] = d[k]
                    reason = u'药品可能对如下功能有损害,应先做检查：'
                    dnew['Selectable'] = 1
                    dnew['Reason'] = ''
                    visTaboo = 0
                    for chk in checkList:
                        if chk in taboo:
                            if visTaboo:
                                reason = reason + u'、' + chk
                            else:
                                reason = reason + chk
                            visTaboo += 1
                    if visTaboo:
                        dnew['Selectable'] = 0
                        dnew['Reason'] = dnew['Reason'] + reason + '\n'

                    visTaboo = 0
                    reason = u'患者有如下疾病史：'
                    for drug in disHist:
                        # med = adr.replace(u'过敏者','').replace(u'患者','')
                        if drug in taboo:
                            if visTaboo:
                                reason = reason + u'、' + drug
                            else:
                                reason = reason + drug
                            visTaboo += 1
                    if visTaboo:
                        dnew['Selectable'] = -1
                        dnew['Reason'] = dnew['Reason'] + reason + '\n'

                    visTaboo = 0
                    reason = u'患者对如下药品过敏：'
                    for drug in drugAdr:
                        # med = adr.replace(u'过敏者','').replace(u'患者','')
                        if drug in taboo:
                            if visTaboo:
                                reason = reason + u'、' + drug
                            else:
                                reason = reason + drug
                            visTaboo += 1
                    if visTaboo:
                        dnew['Selectable'] = -1
                        dnew['Reason'] = dnew['Reason'] + reason + '\n'

                    visTaboo = 0
                    reason = u'患者对如下药物过敏：'
                    for drug in medAdr:
                        # med = adr.replace(u'过敏者','').replace(u'患者','')
                        if drug in taboo:
                            if visTaboo:
                                reason = reason + u'、' + drug
                            else:
                                reason = reason + drug
                            visTaboo += 1
                    if visTaboo:
                        dnew['Selectable'] = -1
                        dnew['Reason'] = dnew['Reason'] + reason + '\n'

                    visTaboo = 0
                    reason = u'患者属于：'
                    for drug in groups:
                        # med = adr.replace(u'过敏者','').replace(u'患者','')
                        if drug in taboo:
                            if visTaboo:
                                reason = reason + u'、' + drug
                            else:
                                reason = reason + drug
                            visTaboo += 1
                    if visTaboo:
                        dnew['Selectable'] = -1
                        dnew['Reason'] = reason
                # json_out['Results'].append(json_content)
                json_out['Results']['Drugs'] = dnewlist
                break

            json_out['Return'] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def getTaboo(request):
    if request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            input_dict.pop('Table')
            if 'Name' in input_dict.keys():  # post operation for TabooRef table
                tid = input_dict['Tid']
                record_info = 'record_info=' + \
                    table + '.objects.filter(Tid=tid)'
                exec(record_info)
                if record_info:
                    json_out["Return"] = 1
                    json_out["Results"] = '{:s}_{:s} already exists.'.format(
                        table, tid)
                else:
                    n = dobject[table]()
                    for key in input_dict:
                        if input_dict[key] not in useless_list:
                            # insert in database
                            value = input_dict[key]
                            setattr(n, key, value)
                    n.save()
                    json_out["Return"] = 0
            else:  # post operation for DrugTaboo table
                n = dobject[table]()
                for key in input_dict:
                    if input_dict[key] not in useless_list:
                        # insert in database
                        value = input_dict[key]
                        setattr(n, key, value)
                n.save()
                json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

        
@csrf_exempt
@auth_required
def getMapTree(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            qid = input_dict['Id']
            table = 'MapAdrDrugMed'
            record_infos = 'record_infos = {:s}.objects'.format(table)
            xid = ''
            for k, v in input_dict.items():
                if 'id' in k.lower():
                    key = v[0] + 'id'
                    record_infos = '{:s}.filter({:s}="{:s}")'.format(
                        record_infos, key.title(), v)
                    xid = '{:s}_{:s}'.format(xid, v).strip('_')
            exec(record_infos)
            if record_infos:
                # record_info = record_infos.values()[0]
                json_list = []
                if qid[0] == 'd':  # the return format of drug querys
                    d_name = Drug.objects.get(Xid=qid).Name
                    json_content = {}
                    json_content['name'] = d_name
                    json_content['category'] = 'D'
                    json_content['parent'] = ''
                    json_list.append(json_content)
                    for record_info in record_infos.values():
                        json_content = {}
                        for key in record_info.keys():
                            if record_info[key] not in useless_list:
                                if key == 'id':
                                    continue
                                elif key == 'Aid':
                                    a_name = AdvReaction.objects.get(
                                        Xid=record_info[key]).Name
                                    json_content['name'] = a_name
                                    json_content['category'] = 'A'
                                    json_content['parent'] = d_name
                                    json_list.append(json_content)
                                elif key == 'Mid':
                                    m_name = Medication.objects.get(
                                        Xid=record_info[key]).Name
                                    json_content['name'] = m_name
                                    json_content['category'] = 'M'
                                    json_content['parent'] = d_name
                                    json_list.append(json_content)
                                    json_content = {}
                                    m_aids = MapAdrDrugMed.objects.filter(
                                        Mid=record_info[key], Aid__isnull=False)
                                    if m_aids:
                                        for m_aid in m_aids.values():
                                            json_content = {}
                                            m_aid_name = AdvReaction.objects.get(
                                                Xid=m_aid['Aid']).Name
                                            json_content['name'] = m_aid_name
                                            json_content['category'] = 'A'
                                            json_content['parent'] = m_name
                                    else:
                                        json_content['name'] = u'无不良反应'
                                        json_content['category'] = ''
                                        json_content['parent'] = m_name
                                        json_list.append(json_content)

                elif qid[0] == 'm':  # format of med querys
                    m_name = Medication.objects.get(Xid=qid).Name
                    json_content = {}
                    json_content['name'] = m_name
                    json_content['category'] = 'M'
                    json_content['parent'] = ''
                    json_list.append(json_content)
                    for record_info in record_infos.values():
                        json_content = {}
                        for key in record_info.keys():
                            if record_info[key] not in useless_list:
                                if key == 'id':
                                    continue
                                elif key == 'Aid':
                                    a_name = AdvReaction.objects.get(
                                        Xid=record_info[key]).Name
                                    json_content['name'] = a_name
                                    json_content['category'] = 'A'
                                    json_content['parent'] = m_name
                                    json_list.append(json_content)
                                elif key == 'Did':
                                    d_name = Drug.objects.get(
                                        Xid=record_info[key]).Name
                                    json_content['name'] = d_name
                                    json_content['category'] = 'D'
                                    json_content['parent'] = m_name
                                    json_list.append(json_content)

                json_out["Results"] = json_list
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
