# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.views import auth_required
import json
import sys
import traceback
from collections import OrderedDict
reload(sys)

from django.conf import settings
es = settings.ES

table_dic = { }
table_dic["dis"] = "Disease"
table_dic["med"] = "Medication"
table_dic["sym"] = "Symptom"
table_dic["lab"] = "Lab"
 
def is_chinese(us):
    try:
        us = us.decode('utf-8')
    except:
        pass
    for uchar in us:
        if uchar >= u'\u4e00' and uchar<=u'\u9fa5':
            return True
    return False
def pinyinfenge(s):
    s=s.lower()
    s = ''.join(s.split()) + ' '
    yunmu=['a','o','i','e','u']
    shengmu=['b','c','d','f','h','j','k','l','m','p','q','r','s','t','w','x','y','z']
    pinyin=''
    vis=0
    for i in range(len(s)):
        if vis:
            vis=0
            continue
        if s[i] in shengmu:
            pinyin=pinyin+' '+s[i:i+2]
            vis=1
        elif s[i]=='g':
            if s[i-1]!='n':
                pinyin=pinyin+' '+s[i:i+2]
                vis=1
            else:
                pinyin=pinyin+s[i]
        elif s[i]=='n':
            if s[i-1] not in yunmu or s[i+1] in yunmu:
                pinyin=pinyin+' '+s[i:i+2]
                vis=1
            else:
                pinyin=pinyin+s[i]
        else:
            pinyin=pinyin+s[i]
    return pinyin
   
@csrf_exempt
# @auth_required
def get_query_output(request):  # get disease list back
    if request.method == "POST":
    # if request.method == "GET":
      json_out = {}

      try:
        json_elements = []
        
        input_dict = json.loads(request.body)
        # input_dict = json.loads(request.GET["q"])
        # json_out['input_dict'] = input_dict
        filter_by_domain = input_dict["filterbydomain"].encode('utf-8')
        filter_by_first = input_dict["filterbyfirst"]
        # filter_by_first = input_dict["filterbyfirst"].encode('utf-8')

        filter_by_fclass = input_dict["filterbyfclass"].encode('utf-8')
        filter_by_sclass = input_dict["filterbysclass"].encode('utf-8')
        filter_by_fbody = input_dict["filterbyfbody"].encode('utf-8')
        filter_by_sbody = input_dict["filterbysbody"].encode('utf-8')
        filter_by_number = int(input_dict["filterbynumber"])
        '''
        filter_by_fclass = ''
        filter_by_sclass = ''
        filter_by_fbody = ''
        filter_by_sbody = ''
        filter_by_number = 20
        '''

        esbody = OrderedDict() 
        filters = { }
        filters["and"] = []
        if len(filter_by_domain):
            xid = filter_by_domain[0].upper()+"id"
            esbody["fields"] = ["Name","Ename",xid,"Weight","Pinyin","Table"]
            domainquery = { "match":{ "Table": table_dic[filter_by_domain] } }
            filters["and"].append(domainquery)
        else:
            esbody["fields"] = ["Name","Ename","Sid","Lid","Did","Mid","Weight","Pinyin","Table"]
        #esbody["filter"] = { }
        esbody["size"] = filter_by_number * 5
        if filter_by_domain == "sym":
            if filter_by_fclass != "":
                fclassquery = { "match_phrase":{ "Yjks": { "query": filter_by_fclass , "slop": 0 }} }
                filters["and"].append(fclassquery)
                if filter_by_sclass != "":
                    sclassquery = { "match_phrase":{ "Ejks": { "query": filter_by_fclass , "slop": 0 }} }
                    filters["and"].append(sclassquery)
            if filter_by_fbody != "":
                fbodyquery = { "match_phrase":{ "Yjbw": { "query": filter_by_fbody, "slop": 0 }} }
                filters["and"].append(fbodyquery)
                if filter_by_sbody != "":
                    sbodyquery = { "match_phrase":{ "Ejbw": { "query": filter_by_sbody, "slop": 0 }} }
                    filters["and"].append(sbodyquery)
        elif filter_by_domain == "dis":
            if filter_by_fclass != "":
                fclassquery = { "match_phrase":{ "Dclass": { "query": filter_by_fclass , "slop": 0 }} }
                filters["and"].append(fclassquery)
        elif filter_by_domain == "med" or filter_by_domain == "lab":
            if filter_by_fclass != "":
                fclassquery = { "match_phrase":{ "Fclass": { "query": filter_by_fclass , "slop": 0 }} }
                filters["and"].append(fclassquery)
                if filter_by_sclass != "":
                    sclassquery = { "match_phrase":{ "Sclass": { "query": filter_by_fclass , "slop": 0 }} }
                    filters["and"].append(sclassquery)
        if len(filter_by_domain):
            esbody["filter"] = filters
        

        ischinese = is_chinese(filter_by_first)

        if ischinese:
            mainquery = { "query_string" :{ "fields": ["Name","Oname"], "query": filter_by_first } }
            esbody["query"] = mainquery
            res1 = es.search( index='medknowledge', doc_type='search', body=esbody )
            res1 = res1['hits']['hits']
            if len(filter_by_first):
                for i in range(len(res1)):
                    try:
                        if res1[i]['fields']['Name'][0].startswith(filter_by_first):
                            res1[i]['_score'] += len(filter_by_first) 
                    except:
                        pass
            res = sorted(res1,cmp=(lambda x,y:1 if x['_score']<y['_score'] else -1))
            #print len(res)
        else:
            res = []
            if len(filter_by_first) == 0:
                mainquery = { "match_all" :{ } }
                esbody["query"] = mainquery
                esbody["size"] = filter_by_number * 5
                res1 = es.search( index='medknowledge', doc_type='search', body=esbody )
                res1 = res1['hits']['hits']
                if len(res1):
                    res = sorted(res1,cmp=(lambda x,y:1 if x['fields']['Weight']<y['fields']['Weight'] else -1))
            if len(filter_by_first)>1:
                # 拼音首字母
                mainquery = { "prefix": {"Firstletter": filter_by_first} }
                esbody["query"] = mainquery
                #print json.dumps(esbody,indent=4)
                res1 = es.search( index='medknowledge', doc_type='search', body=esbody )
                res1 = res1['hits']['hits']
                if len(res1):
                    res = sorted(res1,cmp=(lambda x,y:1 if x['fields']['Weight']<y['fields']['Weight'] else -1))
            if len(res) < filter_by_number:
                # 英文名
                mainquery = { "query_string" :{ "fields": ["Ename","Oname"], "query": filter_by_first } }
                esbody["query"] = mainquery
                res2 = es.search( index='medknowledge', doc_type='search', body=esbody )
                res2 = res2['hits']['hits']
                for i in range(len(res2)):
                    if res2[i]['fields']['Ename'][0].startswith(' '.join(filter_by_first.split())):
                        res2[i]['_score'] += len(filter_by_first.split())
                #res2 = sorted(res2,cmp=(lambda x,y:1 if x['_score']<y['_score'] else -1))
                # 拼音
                mainquery = { "query_string" :{ "fields": ["Pinyin"], "query": pinyinfenge(filter_by_first)} }
                esbody["query"] = mainquery
                res3 = es.search( index='medknowledge', doc_type='search', body=esbody )
                res3 = res3['hits']['hits']
                for i in range(len(res3)):
                    if res3[i]['fields']['Pinyin'][0].replace(' ','').startswith(''.join(filter_by_first.split())):
                        res3[i]['_score'] += len(filter_by_first.split())
                res = res + sorted(res2+res3,cmp=(lambda x,y:1 if x['_score']<y['_score'] else -1))


        #print res
        #print json.dumps(esbody,indent=4)
        answers = []
        for ans in res:
            #print ans
            '''
            try:
                #print ans["_score"]
            except:
                #print ans["fields"]["Weight"]
            '''
            ans = ans["fields"]
            xid = ans['Table'][0][0] + 'id'
            #print ans["Name"][0]
            nans = { }
            nans["value"] = ans[xid][0]
            nans['name'] = ans['Name'][0].encode('utf-8')
            try:
                nans['ename'] = ans['Ename'][0]
            except:
                nans['ename'] = ''
            # answers.append(nans)
            answers.append(nans)

        json_out["return"] = 0
        json_out["elements"] = answers[:filter_by_number]
        # if len(filter_by_first.strip()) == 0:
        #     json_out["elements"] = []

      except:
        json_out["return"] = 1
        json_out["elements"] = []
        traceback.print_exc()

      return HttpResponse(json.dumps(json_out), content_type="application/json")

