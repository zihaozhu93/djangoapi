import os
import sys

sys.path.append("/var/www/djangoapi")
sys.path.append("/var/www/djangoapi/djangoapi")
os.environ['DJANGO_SETTINGS_MODULE'] = 'djangoapi.settings'

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
