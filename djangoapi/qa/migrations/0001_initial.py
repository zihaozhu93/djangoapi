# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entity_relation',
            fields=[
                ('rid', models.AutoField(serialize=False, primary_key=True)),
                ('entity_id1', models.CharField(max_length=20, db_index=True)),
                ('entity_name1', models.CharField(max_length=255)),
                ('entity_type1', models.CharField(max_length=50)),
                ('relation', models.CharField(max_length=50, blank=True)),
                ('entity_id2', models.CharField(max_length=20, db_index=True)),
                ('entity_name2', models.CharField(max_length=255)),
                ('entity_type2', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('pid', models.AutoField(serialize=False, primary_key=True)),
                ('entity_id', models.CharField(max_length=20, db_index=True)),
                ('entity_type', models.CharField(max_length=50)),
                ('property_name', models.CharField(max_length=50)),
                ('property_value', models.CharField(max_length=2000, blank=True)),
            ],
        ),
    ]
