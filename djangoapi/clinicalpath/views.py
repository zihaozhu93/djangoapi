# -*- coding: utf-8 -*-
import json
import time
import traceback
import re
import numpy as np
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.views import auth_required

cp_dict = json.load(
    open('/var/www/djangoapi/file/cp.json', 'r'))

patient_set = set()
patient_info = json.load(
    open('/var/www/djangoapi/file/all_patient_info.json', 'r'))

for p in patient_info:
    patient_set.add(p['INPATIENT_NO'])


drug_json = {}
for var in json.load(open('/var/www/djangoapi/file/all_exec_drug.json', 'r')):
    pid = var['INPATIENT_NO']
    if pid not in drug_json.keys():
        drug_json[pid] = []
    if var['VALID_FLAG'] == 1:
        drug_json[pid].append(var)
undrug_json = {}
for var in json.load(open('/var/www/djangoapi/file/all_exec_undrug.json', 'r')):
    pid = var['INPATIENT_NO']
    if pid not in undrug_json.keys():
        undrug_json[pid] = []
    if var['VALID_FLAG'] == 1:
        undrug_json[pid].append(var)

normal_set = json.load(open(
    '/var/www/djangoapi/file/normal_set.json', 'r'))
normal_drug = [item['item_name'] for item in normal_set['normal_drug']]
normal_undrug = [item['item_name'] for item in normal_set['normal_undrug']]
exclude_nor_drug = json.load(open(
    '/var/www/djangoapi/file/exclude_nor_drug.json', 'r'))
exclude_nor_undrug = json.load(open(
    '/var/www/djangoapi/file/exclude_nor_undrug.json', 'r'))
amount = open(
    '/var/www/djangoapi/file/amount_list.txt', 'r').read()
amount = re.split('\[|,|\]', amount)
amount_list = [val.strip() for val in amount]
amount_list = amount_list[1:-1]
amount_list = [float(val) for val in amount_list]
narray = np.array(amount_list)
amount_mean = np.mean(narray)
amount_std = np.std(narray)

operation_dict = {}
for var in json.load(open('/var/www/djangoapi/file/all_ops.json', 'r')):
    patient_id = var["INPATIENT_NO"]
    operation_dict[patient_id] = var["OPERATION_DATE"]

non_pa_file = "/var/www/djangoapi/file/non_cp_patient_info.json"
pa_file = "/var/www/djangoapi/file/clinicpath_patient_info.json"

patient_dict = {}

file_obj = open(non_pa_file, 'r')
content = json.load(file_obj)
file_obj.close()

for var in content:
    patient_id = var["INPATIENT_NO"]
    # print patient_id
    patient_dict[patient_id] = var

file_obj = open(pa_file, 'r')
content = json.load(file_obj)
file_obj.close()

for var in content:
    patient_id = var["INPATIENT_NO"]
    # print patient_id
    patient_dict[patient_id] = var

@csrf_exempt
@auth_required
def get_user_infor(request):
    '''
    This function querys the basic patient information for any given PID.
    Input: patient ID
    Outputs: a json format of patient information.
    '''
    if request.method == "GET":
        json_out = {}
        try:
            #input_dict = json.loads(request.body)
            input_dict = json.loads(request.GET["q"])
            pid = unicode(str(input_dict["PID"]),"utf-8")
            pid = str(pid)
            pobj = patient_dict[pid]

            json_out["Patient"] = {}
            json_out["Patient"]["ID"] = pobj["INPATIENT_NO"]
            json_out["Patient"]["AGE"] = pobj["AGE"]
            json_out["Patient"]["OutTme"] = pobj["OUT_TIME"]
            json_out['Patient']["Contact"] = pobj["CONTACT_NAME"]
            json_out["Patient"]["OperCode"] = pobj["OPER_CODE"]
            json_out["Patient"]["Name"] = pobj["NAME"]
                  
            json_out["Return"] = 0
            
        except:
            traceback.print_exc()
            json_out["Return"] = 1
    
    return HttpResponse(json.dumps(json_out),content_type="application/json")

@csrf_exempt
@auth_required
def get_user_items(request):
    '''
    This function querys the items information for any given PID.
    Input: patient ID
    Outputs: a json format of items detail information.
    '''
    if request.method == "GET":
        json_out = {}
        try:
            #input_dict = json.loads(request.body)
            input_dict = json.loads(request.GET["q"])
            pid = unicode(str(input_dict["PID"]),"utf-8")
            pid = str(pid)
            pfile = "/var/www/djangoapi/file/" + pid + ".json"            
            pcontent = json.load(open(pfile, "r"))

            json_out["Patient"] = {}
            json_out["Patient"]["ID"] = pid
            json_out["Patient"]["Items"] = pcontent

            json_out["Return"] = 0

        except:
            traceback.print_exc()
            json_out["Return"] = 1

    return HttpResponse(json.dumps(json_out),content_type="application/json")

@csrf_exempt
@auth_required
def update_user_items(request):
    '''
    This function updates the items information for any given PID, IID and status.
    Input: patient ID, item ID and item status
    Outputs: 0 or 1.
    '''
    if request.method == "POST":
        json_out = {}
        try:
            #input_dict = json.loads(request.body)
            input_dict = json.loads(request.GET["q"])
            pid = unicode(str(input_dict["PID"]),"utf-8")
            pid = str(pid)
            pfile = "/var/www/djangoapi/file/" + pid + ".json"
            pcontent = json.load(open(pfile, "r"))

            iid = unicode(str(input_dict["IID"]),"utf-8")
            status = unicode(str(input_dict["Status"]),"utf-8")
            status = str(status)
            
            if status != "":
              json_out["Return"] = 0
            else:
              json_out["Return"] = 1

        except:
            traceback.print_exc()
            json_out["Return"] = 1

    return HttpResponse(json.dumps(json_out),content_type="application/json")


@csrf_exempt
@auth_required
def query_clinicpath(request):
    '''
    This function querys the standard clinical path for any given ICD.
    Input: ICD code
    Outputs: a json format of clinical path.
    '''
    if request.method == "GET":
        json_out = {}
    try:
        input_dict = json.loads(request.GET["q"])
        icd = unicode(str(input_dict['ICD']), "utf-8")
        json_out = cp_dict
    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")


def find_pid(json_array, pid):
    '''
    This function can be viewed as find({}) function in mongodb.
    Input: A json array data, pid
    Outputs: All data about pid, and meets the following need.
    '''
    pid_item_list = []
    for p in json_array:
        if p['INPATIENT_NO'] == pid and p['VALID_FLAG'] == 1:
            pid_item_list.append(p)
    return pid_item_list

    
def score_drug(pid, count_max=10, x_score=1, th1=0.5, th2=1, norm=5):
    '''
    This function compute the pid's drugs score.
    Input: pid
    Outputs: a list of drugs score.
    '''
    drug_cursor = drug_json[pid]
    score_list = []
    drug_freq = {}
    temp_dict = {}
    for drug in drug_cursor:
        item_score = 0
        drug_cost = drug['ITEM_PRICE'] * drug['QTY_TOT'] / drug['PACK_QTY']
        if drug["DRUG_CODE"] not in drug_freq.keys():
            drug_freq[drug["DRUG_CODE"]] = 1
        else:
            drug_freq[drug["DRUG_CODE"]] += 1
        if drug["DRUG_NAME"] in normal_drug:
            continue
        else:
            item_score = (
                1 - exclude_nor_drug[drug["DRUG_NAME"]]["adoption"]) * x_score / norm
            if drug_cost == 0:
                continue
            elif drug_cost <= exclude_nor_drug[drug["DRUG_NAME"]]["fee"]:
                item_score *= th1
            else:
                item_score *= th2
        if drug_freq[drug["DRUG_CODE"]] <= count_max:
            score_list.append({drug["DRUG_CODE"]: {
                              "score": item_score, "item_name": drug["DRUG_NAME"], "item_fee": drug_cost}})

    for item in score_list:
        if item.keys()[0] not in temp_dict:
            temp_dict[item.keys()[0]] = item.values()[0]
        else:
            temp_dict[item.keys()[0]]["score"] += item.values()[0]["score"]

    score_list = []
    for item in temp_dict:
        score_list.append({item: temp_dict[item]})
    return score_list


def score_undrug(pid, count_max=10, x_score=1, th1=0.5, th2=1, norm=5):
    undrug_cursor = undrug_json[pid]
    score_list = []
    undrug_freq = {}
    temp_dict = {}
    for undrug in undrug_cursor:
        item_score = 0
        undrug_cost = undrug['UNIT_PRICE'] * undrug['QTY_TOT']
        if undrug["UNDRUG_CODE"] not in undrug_freq.keys():
            undrug_freq[undrug["UNDRUG_CODE"]] = 1
        else:
            undrug_freq[undrug["UNDRUG_CODE"]] += 1
        if undrug["UNDRUG_NAME"] in normal_undrug:
            continue
        else:
            item_score = (
                1 - exclude_nor_undrug[undrug["UNDRUG_NAME"]]["adoption"]) * x_score / norm
            if undrug_cost == 0:
                continue
            elif undrug_cost <= exclude_nor_undrug[undrug["UNDRUG_NAME"]]["fee"]:
                item_score *= th1
            else:
                item_score *= th2
        if undrug_freq[undrug["UNDRUG_CODE"]] <= count_max:
            score_list.append({undrug["UNDRUG_CODE"]: {
                              "score": item_score, "item_name": undrug["UNDRUG_NAME"], "item_fee": undrug_cost}})
    for item in score_list:
        if item.keys()[0] not in temp_dict:
            temp_dict[item.keys()[0]] = item.values()[0]
        else:
            temp_dict[item.keys()[0]]["score"] += item.values()[0]["score"]
    score_list = []
    for item in temp_dict:
        score_list.append({item: temp_dict[item]})
    return score_list


def score_ops(pid, x_score=50, norm=5):
    amount = 0
    for p in patient_info:
        if p['INPATIENT_NO'] == pid:
            amount = p['BALANCE_COST']
    if amount <= amount_mean:
        return 0
    else:
        score = (amount - amount_mean) / amount_std * x_score
        return score / norm


@csrf_exempt
@auth_required
def query_score(request):
    '''
    The function query all items score for given pid.
    Input: pid
    Output: a dict format of {item:score} for items and sum the total score
    Modified: change the item dict key from item_name to item_code
    '''
    if request.method == "GET":
        json_out = {}
    try:
        input_dict = json.loads(request.GET["q"])
        pid = unicode(str(input_dict['PID']), "utf-8")
        json_out = {}
        score_dict = {}
        score_dict['drug_items_score'] = {}
        score_dict['undrug_items_score'] = {}
        for var in score_drug(pid):
            score_dict['drug_items_score'][var.keys()[0]] = var.values()[0]
        for var in score_undrug(pid):
            score_dict['undrug_items_score'][
                var.keys()[0]] = var.values()[0]

        drug_score = 0
        undrug_score = 0
        max_score = 0
        min_score = 10000
        for var in score_dict['drug_items_score']:
            drug_score += score_dict['drug_items_score'][var]["score"]
            if max_score <= score_dict['drug_items_score'][var]["score"]:
                max_score = score_dict['drug_items_score'][var]["score"]
            if min_score >= score_dict['drug_items_score'][var]["score"]:
                min_score = score_dict['drug_items_score'][var]["score"]
        for var in score_dict['undrug_items_score']:
            undrug_score += score_dict['undrug_items_score'][var]["score"]
            if max_score <= score_dict['undrug_items_score'][var]["score"]:
                max_score = score_dict['undrug_items_score'][var]["score"]
            if min_score >= score_dict['undrug_items_score'][var]["score"]:
                min_score = score_dict['undrug_items_score'][var]["score"]
        score_dict['clinical_score'] = {}
        score_dict['clinical_score']['drug_score'] = drug_score
        score_dict['clinical_score']['undrug_score'] = undrug_score
        total_score = drug_score + undrug_score + score_ops(pid)
        score_dict['clinical_score'][
            'total_score'] = total_score if total_score <= 100 else 100
        score_dict['clinical_score']['max_score'] = max_score
        score_dict['clinical_score']['min_score'] = min_score
        score_dict['clinical_score']['mean_score'] = (drug_score + undrug_score) / (
            len(score_dict['drug_items_score'].keys()) + len(score_dict['undrug_items_score'].keys()))
        json_out['clinical_score'] = score_dict['clinical_score']
        json_out['Return'] = 0
    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")
        

@csrf_exempt
@auth_required
def query_items_time(request):
    '''
    The function returns the items happend time.
    Input: pid
    Output: json format dict storage items happend time.
    '''
    if request.method == "GET":
        json_out = {}
    try:
        timelist = []
        timelist.append(time.time())
        input_dict = json.loads(request.GET["q"])
        pid = unicode(str(input_dict['PID']), "utf-8")
        json_out = {}
        drug_cursor = []
        undrug_cursor = []
        if pid in drug_json.keys():
            drug_cursor = drug_json[pid]
        if pid in undrug_json.keys():
            undrug_cursor = undrug_json[pid]
        item_time = {}
        timelist.append(time.time())
        if drug_cursor:
            for val in drug_cursor:
                val_time = val["USE_TIME"].split(' ')[0]
                if val["DRUG_CODE"] == 999:
                    continue
                if val_time not in item_time.keys():
                    item_time[val_time] = []
                item_time[val_time].append(val["DRUG_CODE"])
        timelist.append(time.time())
        if undrug_cursor:
            json_out['num_undrug_cursor'] = len(undrug_cursor)
            for val in undrug_cursor:
                val_time = val["USE_TIME"].split(' ')[0]
                if val["UNDRUG_CODE"] == 999:
                    continue
                if val_time not in item_time.keys():
                    item_time[val_time] = []
                item_time[val_time].append(val["UNDRUG_CODE"])

        timelist.append(time.time())
        for key in item_time:
            unique_items = set(item_time[key])
            item_time[key] = list(unique_items)
        json_out['normal_date'] = item_time
        json_out['special_date'] = {}
        pid = str(pid)
        json_out['special_date']['in_date'] = patient_dict[pid]['IN_TIME'].split(' ')[
            0]
        json_out['special_date']['out_date'] = patient_dict[pid]['OUT_TIME'].split(' ')[
            0]
        if pid in operation_dict.keys():
            json_out['special_date']['operation_date'] = operation_dict[pid]
        else:
            json_out['special_date']['operation_date'] = 'No operation'
        timelist.append(time.time())
        json_out["timelist"] = '__'.join([str(timelist[i+1]-timelist[i]) for i in range(len(timelist)-1) ])
        json_out['Return'] = 0

    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")

@csrf_exempt
@auth_required
def query_item_reason(request):
    '''
    The function query the pid's item code for item score details.
    Input: pid, item code
    Outputs: a dict of score detail and explanation for a item_code.
    Modified v1: return all the items of patient and their explanations.
    Modified v2: return all the items of patient and their chinese explanations.
    '''
    if request.method == "GET":
        json_out = {}

    try:
        input_dict = json.loads(request.GET["q"])
        pid = unicode(str(input_dict['PID']), "utf-8")
        json_out = {}
        score_dict = {}
        items_dict = {}
        if pid in drug_json.keys():
            for val in score_drug(pid):
                score_dict[val.keys()[0]] = val.values()[0]

            for item in drug_json[pid]:
                if item['DRUG_CODE'] == 999:
                    continue
                if item['DRUG_NAME'] not in exclude_nor_drug.keys():
                    if item['DRUG_CODE'] not in items_dict.keys():
                        items_dict[item['DRUG_CODE']] = {}
                        items_dict[item['DRUG_CODE']][
                            'item_name'] = item['DRUG_NAME']
                        items_dict[item['DRUG_CODE']]['score'] = ''
                        items_dict[item['DRUG_CODE']]['item_fee'] = ''
                        items_dict[item['DRUG_CODE']][
                            'reason'] = "本项属于标准临床路径。"
                elif item['DRUG_CODE'] not in items_dict.keys():
                    items_dict[item['DRUG_CODE']] = {}
                    items_dict[item['DRUG_CODE']][
                        'item_name'] = item['DRUG_NAME']
                    items_dict[item['DRUG_CODE']][
                        'score'] = score_dict[item['DRUG_CODE']]["score"]
                    items_dict[item['DRUG_CODE']][
                        'item_fee'] = score_dict[item['DRUG_CODE']]["item_fee"]
                    items_dict[item['DRUG_CODE']]['reason'] = "本项不属于标准临床路径。" + "项目采用率是: " + str(exclude_nor_drug[item['DRUG_NAME']][
                        'adoption']) + "，平均花费是: " + str(exclude_nor_drug[item['DRUG_NAME']]['fee']) + "，平均花费标准差是: " + str(exclude_nor_drug[item['DRUG_NAME']]['std'])

        if pid in undrug_json.keys():
            for val in score_undrug(pid):
                score_dict[val.keys()[0]] = val.values()[0]
            for item in undrug_json[pid]:
                if item['UNDRUG_CODE'] == 999:
                    continue
                if item['UNDRUG_NAME'] not in exclude_nor_undrug.keys():
                    if item['UNDRUG_CODE'] not in items_dict.keys():
                        items_dict[item['UNDRUG_CODE']] = {}
                        items_dict[item['UNDRUG_CODE']][
                            'item_name'] = item['UNDRUG_NAME']
                        items_dict[item['UNDRUG_CODE']]['score'] = ''
                        items_dict[item['UNDRUG_CODE']]['item_fee'] = ''
                        items_dict[item['UNDRUG_CODE']][
                            'reason'] = "本项属于标准临床路径。"
                elif item['UNDRUG_CODE'] not in items_dict.keys():
                    items_dict[item['UNDRUG_CODE']] = {}
                    items_dict[item['UNDRUG_CODE']][
                        'item_name'] = item['UNDRUG_NAME']
                    items_dict[item['UNDRUG_CODE']][
                        'score'] = score_dict[item['UNDRUG_CODE']]["score"]
                    items_dict[item['UNDRUG_CODE']][
                        'item_fee'] = score_dict[item['UNDRUG_CODE']]["item_fee"]
                    items_dict[item['UNDRUG_CODE']]['reason'] = "本项不属于标准临床路径。" + "项目采用率是: " + str(exclude_nor_undrug[item['UNDRUG_NAME']][
                        'adoption']) + "，平均花费是: " + str(exclude_nor_undrug[item['UNDRUG_NAME']]['fee']) + "，平均花费标准差是: " + str(exclude_nor_undrug[item['UNDRUG_NAME']]['std'])

        json_out['Items'] = items_dict
        json_out['Return'] = 0
    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")

    
def get_score_label(score, mean, std):
    if score <= mean - 0.5 * std:
        return '吻合'
    elif score > mean - 0.5 * std and score <= mean + std:
        return '正常'
    elif score > mean + std and score <= mean + 2 * std:
        return '偏离'
    elif score > mean + 2 * std and score <= mean + 3 * std:
        return '中度偏离'
    else:
        return '极度偏离'


@csrf_exempt
@auth_required
def query_patients_info(request):
    '''
    This function querys the basic patient information of all patients, also
    return their clinical socre
    Input: none
    Output: a dict format of all patients basic information and clinical score.
    '''
    if request.method == "GET":
        json_out = {}
    try:
        # input_dict = json.loads(request.GET["q"])
        # icd = unicode(str(input_dict['ICD']), "utf-8")
        patients_score = json.load(
            open('/var/www/djangoapi/file/patients_score.json', 'r'))
        score_list = [float(var) for var in patients_score.values()]
        score_list = np.array(score_list)
        score_mean = np.mean(score_list)
        score_std = np.std(score_list)
        p_dict = {}
        for pid in patient_dict:
            pobj = patient_dict[pid]
            p_dict[pid] = {}
            p_dict[pid]["ID"] = pobj["INPATIENT_NO"]
            p_dict[pid]["AGE"] = pobj["AGE"]
            p_dict[pid]["OutTime"] = pobj["OUT_TIME"]
            p_dict[pid]["Contact"] = pobj["CONTACT_NAME"]
            p_dict[pid]["OperCode"] = pobj["OPER_CODE"]
            p_dict[pid]["Name"] = pobj["NAME"]
            p_dict[pid]["Gender"] = '男' if pobj["SEX_CODE"] == 1 else '女'

            # score_dict = {}
            # total_score = 0
            # if pid in drug_json.keys():  # Someone has no drug records.
            #     for var in score_drug(pid):
            #         score_dict[var.keys()[0]] = var.values()[0]
            # if pid in undrug_json.keys():
            #     for var in score_undrug(pid):
            #         score_dict[var.keys()[0]] = var.values()[0]
            # if score_dict:
            #     for var in score_dict:
            #         total_score += score_dict[var]["score"]
            # p_dict[pid]["Score"] = total_score + score_ops(pid)
            p_dict[pid]["Score"] = patients_score[pid]
            p_dict[pid]["Is_standard"] = get_score_label(
                p_dict[pid]["Score"], score_mean, score_std)
        json_out["content"] = p_dict
        json_out["Return"] = 0
    except:
        traceback.print_exc()
        json_out["Return"] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")

    
def query_stat_info(request):
    """
    This function just return 6 items of patients info: pathway on or off,
    gender, age, length of stay, score, and cost.
    """
    if request.method == "GET":
        json_out = {}
    try:
        stat = {}
        cp_list = json.load(
            open('/var/www/djangoapi/file/clinicpath_patient_info.json', 'r'))
        non_cp_list = json.load(
            open('/var/www/djangoapi/file/non_cp_patient_info.json', 'r'))
        patients_score = json.load(
            open('/var/www/djangoapi/file/patients_score.json', 'r'))
        for pid in patient_dict:
            stat[pid] = {}
            stat[pid]['ID'] = pid
            stat[pid]['Contact'] = patient_dict[pid]['CONTACT_NAME']
            stat[pid]['OutTime'] = patient_dict[pid]['OUT_TIME']
            stat[pid]['OperCode'] = patient_dict[pid]['OPER_CODE']
            stat[pid]['Name'] = patient_dict[pid]['NAME']
            stat[pid]['Gender'] = '男' if patient_dict[
                pid]['SEX_CODE'] == 1 else '女'
            stat[pid]['Age'] = patient_dict[pid]['AGE']
            stat[pid]['Stay_days'] = patient_dict[pid]['IN_DAYS']
        for p in cp_list:
            pid = p['INPATIENT_NO']
            stat[pid]['Pathway_on'] = 1
        for p in non_cp_list:
            pid = p['INPATIENT_NO']
            stat[pid]['Pathway_on'] = 0
        for p in patient_info:
            pid = p['INPATIENT_NO']
            stat[pid]['Cost'] = p['BALANCE_COST']
        for pid in patients_score:
            stat[pid]['Score'] = patients_score[pid]

        json_out['content'] = stat
        json_out['Return'] = 0
    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")
    

# add one: query patient's items detail for medcareFee app.
def get_reason(isInClinicalpath, adoption, itemCost, meanCost, stdCost):
    '''
    format the reason of items based on our system.
    '''
    try:
        isInClinicalpath = int(isInClinicalpath)
        adoption = float(adoption)
        itemCost = float(itemCost)
        meanCost = float(meanCost)
        stdCost = float(stdCost)
    except:
        if isInClinicalpath:
            reason = u'在临床路径里，\n自动接受'.format(itemCost, meanCost, stdCost)
            state = 'Automatically approved'
            return state, reason
        else:
            print err
        pass

    # paras : is-in-clinicalpath , adoption , item-cost ,
    # compared-with-meancost
    paras = ['n', 'l', 'l', 'l']
    if isInClinicalpath:
        paras[0] = 'y'
    # 采用率
    if adoption > 0.3:
        paras[1] = 'h'
    # 价格
    if itemCost < 1:
        paras[2] = 'l'
    elif itemCost < 10:
        paras[2] = 'm'
    else:
        paras[2] = 'h'
    # 比平均价格高
    if itemCost <= meanCost + 0.00001:
        paras[3] = 'l'
    elif itemCost - meanCost < stdCost:
        paras[3] = 'm'
    else:
        paras[3] = 'h'
    # 在标准临床路径
    if paras[0] == 'n':
        # 费用极低
        if paras[2] == 'l':
            # reason = u'不在临床路径里，\n费用极低，为{:.2f}，\n自动接受'.format(
            #     adoption, itemCost)
            reason = u'不在临床路径里，\n费用极低，为{:.2f}，\n自动接受'.format(itemCost)
        # 费用低
        elif paras[2] == 'm':
            # 采用率高
            if paras[1] == 'h':
                reason = u'不在临床路径里，\n采用率高，为{:.2f}，\n费用低,为{:.2f}，\n自动接受'.format(
                    adoption, itemCost)
            # 采用率低
            else:
                # 比市场高
                if paras[3] == 'h':
                    reason = u'不在临床路径里，\n采用率低，为{:.2f}，\n费用低,为{:.2f}，\n平均费用为{:.2f}，\n标准差为{:.2f}，\n比平均费用高，\n待定'.format(
                        adoption, itemCost, meanCost, stdCost)
                # 比市场略高
                elif paras[3] == 'm':
                    reason = u'不在临床路径里，\n采用率低，为{:.2f}，\n费用低,为{:.2f}，\n平均费用为{:.2f}，\n标准差为{:.2f}，\n与平均费用相差不大，\n自动接受'.format(
                        adoption, itemCost, meanCost, stdCost)
                # 比市场低
                else:
                    reason = u'不在临床路径里，\n采用率低，为{:.2f}，\n费用低,为{:.2f}，\n平均费用为{:.2f}，比平均费用低，\n自动接受'.format(
                        adoption, itemCost, meanCost)
        # 费用高
        else:
            # 采用率低
            if paras[1] == 'l':
                reason = u'不在临床路径里，\n采用率低，为{:.2f}，\n费用高,为{:.2f}，\n待定'.format(
                    adoption, itemCost)
            # 采用率高
            else:
                # 比市场高
                if paras[3] == 'h':
                    reason = u'不在临床路径里，\n采用率高，为{:.2f}，\n费用高,为{:.2f}，\n平均费用为{:.2f}，\n标准差为{:.2f}，\n比平均费用高，\n待定'.format(
                        adoption, itemCost, meanCost, stdCost)
                elif paras[3] == 'm':
                    reason = u'不在临床路径里，\n采用率高，为{:.2f}，\n费用高,为{:.2f}，\n平均费用为{:.2f}，\n标准差为{:.2f}，\n与平均费用相差不大，\n自动接受'.format(
                        adoption, itemCost, meanCost, stdCost)
                if paras[3] == 'l':
                    reason = u'不在临床路径里，\n采用率高，为{:.2f}，\n费用高,为{:.2f}，\n平均费用为{:.2f}，比平均费用低，\n自动接受'.format(
                        adoption, itemCost, meanCost)
    # 在标准路径里
    else:
        # 费用太高
        if paras[2] == 'h' and paras[3] == 'h':
            reason = u'在临床路径里，\n费用高,为{:.2f}，\n平均费用为{:.2f}，\n标准差为{:.2f}，\n比平均费用高，\n待定'.format(
                itemCost, meanCost, stdCost)
        # 费用低
        else:
            reason = u'在临床路径里，\n自动接受'.format(itemCost, meanCost, stdCost)

    if u'待定' in reason:
        state = 'Pending'
    elif u'自动接受' in reason:
        state = 'Automatically approved'
    else:
        print err
    return state, reason


@csrf_exempt
@auth_required
def medfee_query_item_reason(request):
    '''
    The function query the pid's item code for item score details.
    Input: pid, item code
    Outputs: a dict of score detail and explanation for a item_code.
    Modified: return all the items of patient and their explanations.
    '''
    if request.method == "GET":
        json_out = {}

    try:
        input_dict = json.loads(request.GET["q"])
        pid = unicode(str(input_dict['PID']), "utf-8")
        json_out = {}
        score_dict = {}
        items_dict = {}
        pre_item_dict = {}
        p1_dict = json.load(
            open('/var/www/djangoapi/file/ZY010001101411.json', 'r'))
        p2_dict = json.load(
            open('/var/www/djangoapi/file/ZY010001102284.json', 'r'))
        pre_item_dict['ZY010001101411'] = p1_dict.keys()
        pre_item_dict['ZY010001102284'] = p2_dict.keys()
        for val in score_drug(pid, 1):
            score_dict[val.keys()[0]] = val.values()[0]

        for val in score_undrug(pid, 1):
            score_dict[val.keys()[0]] = val.values()[0]
        # for item in find_pid(drug_json, pid):
        for item in drug_json[pid]:
            if item['DRUG_CODE'] == 999:
                continue
            if item['DRUG_CODE'] not in pre_item_dict[pid]:
                continue
            if item['DRUG_NAME'] not in exclude_nor_drug.keys():
                if item['DRUG_CODE'] not in items_dict.keys():
                    items_dict[item['DRUG_CODE']] = {}
                    items_dict[item['DRUG_CODE']][
                        'item_name'] = item['DRUG_NAME']
                    items_dict[item['DRUG_CODE']]['is_in_standard_path'] = 1
                    items_dict[item['DRUG_CODE']]['score'] = ''
                    items_dict[item['DRUG_CODE']]['item_fee'] = ''
                    items_dict[item['DRUG_CODE']]['adoption'] = ''
                    items_dict[item['DRUG_CODE']]['cost_mean'] = ''
                    items_dict[item['DRUG_CODE']]['cost_std'] = ''
                    # items_dict[item['DRUG_CODE']][
                    #     'reason'] = get_reason()
            elif item['DRUG_CODE'] not in items_dict.keys():
                items_dict[item['DRUG_CODE']] = {}
                items_dict[item['DRUG_CODE']][
                    'item_name'] = item['DRUG_NAME']
                items_dict[item['DRUG_CODE']][
                    'is_in_standard_path'] = 0
                items_dict[item['DRUG_CODE']][
                    'score'] = score_dict[item['DRUG_CODE']]["score"]
                items_dict[item['DRUG_CODE']][
                    'item_fee'] = score_dict[item['DRUG_CODE']]["item_fee"]
                items_dict[item['DRUG_CODE']][
                    'adoption'] = exclude_nor_drug[item['DRUG_NAME']]['adoption']
                items_dict[item['DRUG_CODE']][
                    'cost_mean'] = exclude_nor_drug[item['DRUG_NAME']]['fee']
                items_dict[item['DRUG_CODE']][
                    'cost_std'] = exclude_nor_drug[item['DRUG_NAME']]['std']
                # items_dict[item['DRUG_CODE']]['reason'] = "The total adoption is: " + str(exclude_nor_drug[item['DRUG_NAME']][
                #     'adoption']) + ". The average cost is: " + str(exclude_nor_drug[item['DRUG_NAME']]['fee']) + ". And cost std is: " + str(exclude_nor_drug[item['DRUG_NAME']]['std'])
            state, reason = get_reason(items_dict[item['DRUG_CODE']]['is_in_standard_path'], items_dict[item['DRUG_CODE']]['adoption'], items_dict[
                                       item['DRUG_CODE']]['item_fee'], items_dict[item['DRUG_CODE']]['cost_mean'], items_dict[item['DRUG_CODE']]['cost_std'])
            items_dict[item['DRUG_CODE']]['state'] = state
            items_dict[item['DRUG_CODE']]['reason'] = reason

        # for item in find_pid(undrug_json, pid):
        for item in undrug_json[pid]:
            if item['UNDRUG_CODE'] == 999:
                continue
            if item['UNDRUG_CODE'] not in pre_item_dict[pid]:
                continue
            if item['UNDRUG_NAME'] not in exclude_nor_undrug.keys():
                if item['UNDRUG_CODE'] not in items_dict.keys():
                    items_dict[item['UNDRUG_CODE']] = {}
                    items_dict[item['UNDRUG_CODE']][
                        'item_name'] = item['UNDRUG_NAME']
                    items_dict[item['UNDRUG_CODE']][
                        'is_in_standard_path'] = 1
                    items_dict[item['UNDRUG_CODE']]['score'] = ''
                    items_dict[item['UNDRUG_CODE']]['item_fee'] = ''
                    items_dict[item['UNDRUG_CODE']]['adoption'] = ''
                    items_dict[item['UNDRUG_CODE']]['cost_mean'] = ''
                    items_dict[item['UNDRUG_CODE']]['cost_std'] = ''
                    # items_dict[item['UNDRUG_CODE']][
                    #     'reason'] = "This item is the normal item."
            elif item['UNDRUG_CODE'] not in items_dict.keys():
                items_dict[item['UNDRUG_CODE']] = {}
                items_dict[item['UNDRUG_CODE']][
                    'item_name'] = item['UNDRUG_NAME']
                items_dict[item['UNDRUG_CODE']][
                    'is_in_standard_path'] = 0
                items_dict[item['UNDRUG_CODE']][
                    'score'] = score_dict[item['UNDRUG_CODE']]["score"]
                items_dict[item['UNDRUG_CODE']][
                    'item_fee'] = score_dict[item['UNDRUG_CODE']]["item_fee"]
                items_dict[item['UNDRUG_CODE']][
                    'adoption'] = exclude_nor_undrug[item['UNDRUG_NAME']]['adoption']
                items_dict[item['UNDRUG_CODE']][
                    'cost_mean'] = exclude_nor_undrug[item['UNDRUG_NAME']]['fee']
                items_dict[item['UNDRUG_CODE']][
                    'cost_std'] = exclude_nor_undrug[item['UNDRUG_NAME']]['std']
                # items_dict[item['UNDRUG_CODE']]['reason'] = "The total adoption is: " + str(exclude_nor_undrug[item['UNDRUG_NAME']][
                #     'adoption']) + ". The average cost is: " + str(exclude_nor_undrug[item['UNDRUG_NAME']]['fee']) + ". And cost std is: " + str(exclude_nor_undrug[item['UNDRUG_NAME']]['std'])
            state, reason = get_reason(items_dict[item['UNDRUG_CODE']]['is_in_standard_path'], items_dict[item['UNDRUG_CODE']]['adoption'], items_dict[
                                       item['UNDRUG_CODE']]['item_fee'], items_dict[item['UNDRUG_CODE']]['cost_mean'], items_dict[item['UNDRUG_CODE']]['cost_std'])
            items_dict[item['UNDRUG_CODE']]['state'] = state
            items_dict[item['UNDRUG_CODE']]['reason'] = reason

        json_out['Items'] = items_dict
        json_out['Return'] = 0
    except:
        traceback.print_exc()
        json_out['Return'] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")


# add two: query patient's info for medcareFee app.
@csrf_exempt
@auth_required
def medfee_query_patients_info(request):
    '''
    This function querys the basic patient information of all patients, also
    return their clinical socre
    Input: none
    Output: a dict format of all patients basic information and clinical score.
    '''
    if request.method == "GET":
        json_out = {}
    try:
        # input_dict = json.loads(request.GET["q"])
        # icd = unicode(str(input_dict['ICD']), "utf-8")
        pre_patient = ['ZY010001101411', 'ZY010001102284']
        patients_score = json.load(
            open('/var/www/djangoapi/file/patients_score.json', 'r'))
        p_dict = {}
        for pid in pre_patient:
            pobj = patient_dict[pid]
            p_dict[pid] = {}
            p_dict[pid]["ID"] = pobj["INPATIENT_NO"]
            p_dict[pid]["AGE"] = pobj["AGE"]
            p_dict[pid]["OutTme"] = pobj["OUT_TIME"]
            p_dict[pid]["Contact"] = pobj["CONTACT_NAME"]
            p_dict[pid]["OperCode"] = pobj["OPER_CODE"]
            p_dict[pid]["Name"] = pobj["NAME"]

            # score_dict = {}
            # total_score = 0
            # for var in score_drug(pid):
            #     score_dict[var.keys()[0]] = var.values()[0]
            # for var in score_undrug(pid):
            #     score_dict[var.keys()[0]] = var.values()[0]
            # for var in score_dict:
            #     total_score += score_dict[var]["score"]
            # p_dict[pid]["Score"] = total_score + score_ops(pid)
            p_dict[pid]["Score"] = patients_score[pid]
        json_out["content"] = p_dict
        json_out["Return"] = 0
    except:
        traceback.print_exc()
        json_out["Return"] = 1
    return HttpResponse(json.dumps(json_out), content_type="application/json")
