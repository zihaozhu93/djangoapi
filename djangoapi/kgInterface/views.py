# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.views import auth_required
#import settings
import json
import os
import traceback
from collections import OrderedDict
import numpy as np
from fuzzywuzzy import fuzz
import sys
reload(sys)
sys.setdefaultencoding('utf8')



################################################################################
# 疾病自诊所需文件
id_name_dict = json.load(open('/var/www/djangoapi/file/kgInterface/id_name_dict.json'))
id_gender_dict = json.load(open('/var/www/djangoapi/file/kgInterface/id_gender_dict.json'))
id_degree_dict= json.load(open('/var/www/djangoapi/file/kgInterface/id_degree_dict.json'))
symptom_disease_dict = json.load(open('/var/www/djangoapi/file/kgInterface/symptom_disease_dict.json'))
symptom_bodypart_dict = json.load(open('/var/www/djangoapi/file/kgInterface/symptom_bodypart_dict.json'))
disease_symptom_dict = json.load(open('/var/www/djangoapi/file/kgInterface/disease_symptom_dict.json'))
disease_department_dict = json.load(open('/var/www/djangoapi/file/kgInterface/disease_department_dict.json'))
# id_degree_dict = { did:len(disease_symptom_dict[did]) for did in disease_symptom_dict }
################################################################################

PREFIX_BASE = "<http://xianjiaotong.edu/"
PREFIX_PRO = PREFIX_BASE + "property/>"
PREFIX_DIS = PREFIX_BASE + "disease/>"
PREFIX_LAB = PREFIX_BASE + "lab/>"
PREFIX_SYM = PREFIX_BASE + "symptom/>"
PREFIX_MED = PREFIX_BASE + "medicine/>"
PREFIX_DC = PREFIX_BASE + "dclass/>"
PREFIX_MC = PREFIX_BASE + "mclass/>"
PREFIX_SC = PREFIX_BASE + "sclass/>"
PREFIX_SB = PREFIX_BASE + "sbody/>"
PREFIX_LC = PREFIX_BASE + "lclass/>"

ABBRAVIATION_PRO = "pro:"
ABBRAVIATION_DIS = "dis:"
ABBRAVIATION_LAB = "lab:"
ABBRAVIATION_SYM = "sym:"
ABBRAVIATION_MED = "med:"
ABBRAVIATION_DC = "dc:"
ABBRAVIATION_MC = "mc:"
ABBRAVIATION_SC = "sc:"
ABBRAVIATION_SB = "sb:"
ABBRAVIATION_LC = "lc:"


RDF3X_API_DIR = "/var/rdf3x/bin/"
KG_DAT_DIR = "/var/rdf3x/"
KG_DATABASE = "kgdatanew"
REQ_FILE_DIR = "/var/www/djangoapi/file/"

rel_chinese_dic = {}
rel_chinese_dic["DisOtherLink"] = "其他关系"
rel_chinese_dic["DisLab"] = "实验室检查"
rel_chinese_dic["DisOtherLab"] = "其他辅助检查"
rel_chinese_dic["DisDiag"] = "诊断"
rel_chinese_dic["DisDiagDiff"] = "鉴别诊断"
rel_chinese_dic["DisComp"] = "并发症"
rel_chinese_dic["DisMedi"] = "治疗"
rel_chinese_dic["DisProg"] = "预后"
rel_chinese_dic["DisPathogen"] = "发病机制"
rel_chinese_dic["DisManifest"] = "临床表现"
rel_chinese_dic["DisCause"] = "病因"
rel_chinese_dic["DisOver"] = "概述"
rel_chinese_dic["DisEpid"] = "流行病学"
rel_chinese_dic["DisPrecau"] = "预防"
rel_chinese_dic["Disclass"] = "科室"
rel_chinese_dic["RelateDrag"] = "相关药物"
rel_chinese_dic["RelateDis"] = "相关疾病"
rel_chinese_dic["RelateLab"] = "相关检查"

rel_chinese_dic["MedOtherLink"] = "其他关系"
rel_chinese_dic["MedIndDiag"] = "适应症"
rel_chinese_dic["MedContDiag"] = "禁忌症"
rel_chinese_dic["Med"] = "药物相互作用"
rel_chinese_dic["MedPharmaAct"] = "药理作用"
rel_chinese_dic["MedDosage"] = "药物剂型"
rel_chinese_dic["MedInstruct"] = "用法用量"
rel_chinese_dic["MedPharmacok"] = "药动学"
rel_chinese_dic["MedAtten"] = "注意事项"
rel_chinese_dic["MedRect"] = "不良反应"
rel_chinese_dic["MedOpin"] = "专家点评"
rel_chinese_dic["MedRelateDrag"] = "相关药物"
rel_chinese_dic["MedRelateDis"] = "相关疾病"
rel_chinese_dic["MedRelateLab"] = "相关检查"

rel_chinese_dic["MedFirClass"] = "一级关系"
rel_chinese_dic["MedSecClass"] = "二级关系"
rel_chinese_dic["MedThrClass"] = "三级关系"
rel_chinese_dic["McParent"] = "药物父母关系"

rel_chinese_dic["SymComCheck"] = "常用检查"
rel_chinese_dic["SymPosDis"] = "可能疾病"
rel_chinese_dic["SymFirClass"] = "一级关系"
rel_chinese_dic["SymSecClass"] = "二级关系"
rel_chinese_dic["SymFirBody"] = "一级部位"
rel_chinese_dic["SymSecBody"] = "二级部位"
rel_chinese_dic["SymOtherLink"] = "其他关系"
rel_chinese_dic["ScParent"] = "科室父母关系"
rel_chinese_dic["SbParent"] = "部位父母关系"

rel_chinese_dic["LabOtherLink"] = "其他关系"
rel_chinese_dic["LabNote"] = "附注"
rel_chinese_dic["LabOper"] = "操作方法"
rel_chinese_dic["LabClinSig"] = "临床意义"
rel_chinese_dic["LabOverview"] = "概述"
rel_chinese_dic["LabPrin"] = "原理"
rel_chinese_dic["LabReag"] = "试剂"
rel_chinese_dic["LabRelateDrag"] = "相关药物"
rel_chinese_dic["LabRelateDis"] = "相关疾病"
rel_chinese_dic["LabRelateLab"] = "相关检查"
rel_chinese_dic["LabFirClass"] = "一级关系"
rel_chinese_dic["LabSecClass"] = "二级关系"
rel_chinese_dic["LcParent"] = "检查父母关系"


class RDF_node:

    def __init__(self, id, type):
        self.id = id
        self.type = type
        if self.type == "dis":
            self.abbraviation = ABBRAVIATION_DIS
        elif self.type == "lab":
            self.abbraviation = ABBRAVIATION_LAB
        elif self.type == "sym":
            self.abbraviation = ABBRAVIATION_SYM
        elif self.type == "med":
            self.abbraviation = ABBRAVIATION_MED
        elif self.type == "dc":
            self.abbraviation = ABBRAVIATION_DC
        elif self.type == "mc":
            self.abbraviation = ABBRAVIATION_MC
        elif self.type == "sc":
            self.abbraviation = ABBRAVIATION_SC
        elif self.type == "sb":
            self.abbraviation = ABBRAVIATION_SB
        elif self.type == "lc":
            self.abbraviation = ABBRAVIATION_LC
        elif self.type == None:
            # this serverd as a dummy node type
            self.abbraviation = None
        else:
            raise ValueError("Invalid node type for RDF.")
        prefix_str = "PREFIX " + ABBRAVIATION_PRO + PREFIX_PRO
        prefix_str += " PREFIX " + ABBRAVIATION_DIS + PREFIX_DIS
        prefix_str += " PREFIX " + ABBRAVIATION_LAB + PREFIX_LAB
        prefix_str += " PREFIX " + ABBRAVIATION_SYM + PREFIX_SYM
        prefix_str += " PREFIX " + ABBRAVIATION_MED + PREFIX_MED
        prefix_str += " PREFIX " + ABBRAVIATION_DC + PREFIX_DC
        prefix_str += " PREFIX " + ABBRAVIATION_MC + PREFIX_MC
        prefix_str += " PREFIX " + ABBRAVIATION_SC + PREFIX_SC
        prefix_str += " PREFIX " + ABBRAVIATION_SB + PREFIX_SB
        prefix_str += " PREFIX " + ABBRAVIATION_LC + PREFIX_LC
        self.prefix = prefix_str

    def query_all(self):
        '''
        Obtain all the relations and nodes that are connects (both direction)
        to the specific node
        '''
        query = self.prefix
        query += ' SELECT DISTINCT ?r ?n WHERE {{?x ?r ?n FILTER (regex (?x, "' + str(
            self.id) + '"))} UNION {?n ?r ?x FILTER (regex (?x, "' + str(
                self.id) + '"))}}'
        return query

    def get_property(self):
        '''
        This function generate the query to obtain all the property
        '''
        query = self.prefix
        query += ' SELECT DISTINCT ?r ?n WHERE { ' + self.abbraviation + \
            self.id + ' ?r ?n FILTER (regex (?r, "property"))}'
        return query

    def get_path_one_node(self):
        '''
        This function generate the query to obtain all the 
        node (both in or out link) that connects to the query node
        The retrun result from RDF will be in the format of:
        "relationship" "id" "chinese_name"
        '''
        query = self.prefix
        query += ' SELECT DISTINCT ?r ?n ?p WHERE { ' + self.abbraviation + \
            self.id + '?r ?n FILTER (!regex (?r, "property")). ?n pro:name ?p}'
        return query

    def get_path_one_node_cross_rel(self):
        '''
        It generates cross raltionships between all one degree node
        The returned result is in the form of 
        id1 chinese_name_1 relationship id2 chinese_name_2
        where id1 and id2 are all the belongs to (subset of) the list
        of ids that "get_path_one_node" returns
        '''
        query = self.prefix
        query += ' SELECT DISTINCT ?n1 ?p1 ?r ?n2 ?p2 WHERE { ' + self.abbraviation + self.id + \
            ' ?r1 ?n1 FILTER (!regex (?r1, "property")).' + self.abbraviation + self.id + \
            ' ?r2 ?n2 FILTER (!regex (?r2, "property")).' + \
            '?n1 ?r ?n2. ?n1 pro:name ?p1. ?n2 pro:name ?p2.}'
        return query

# In[ ]:


def get_first_order_rel(id_list):
    '''
    This function is used to generate a query that returns all 
    first order relationships among a set of different nodes
    RDF returned format: 
        id1, chinese_name_1 ,relationship, id2, chinese_name_2,
    where id1 and id2 have to be in the id_list.
    '''
    # generate a dummy RDF node
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    query += ' SELECT DISTINCT ?n1 ?c1 ?p ?n2 ?c2 WHERE { ?n1 ?p ?n2 ' + \
        'FILTER (('

    # regular expression for the first node (n1 node)
    for idx, node_id in enumerate(id_list):
        node_id = str(node_id).strip()
        if idx == len(id_list) - 1:
            query += 'regex (?n1, "' + node_id + '") ) && '
        else:
            query += 'regex (?n1, "' + node_id + '") || '
    # regular expression for the second node (n2 node)
    query += '('
    for idx, node_id in enumerate(id_list):
        node_id = str(node_id).strip()
        if idx == len(id_list) - 1:
            query += 'regex (?n2, "' + node_id + '") )'
        else:
            query += 'regex (?n2, "' + node_id + '") || '
    query += ').'
    query += ' ?n1 pro:name ?c1. ?n2 pro:name ?c2.}'

    return query

# In[ ]:


def get_second_order_rel(id_list):
    '''
    This function is used to generate a query that returns all 
    second order relationships among a set of different nodes. 
    REF return format: 
        id1, chinese_name1, relationship1, connected_node_id, 
        connected_node_chinese_name, relationship2, id2, 
        chinese_name2
    where, id1 and id2 have to be in the set of id_list. 
    '''
    # generate a dummy RDF node
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    query += ' SELECT DISTINCT ?n1 ?c1 ?p1 ?d ?c ?p2 ?n2 ?c2 ' + \
        'WHERE { ?n1 ?p1 ?d. FILTER ('
    # regular expression for the first node (n1 node)
    for idx, node_id in enumerate(id_list):
        node_id = str(node_id).strip()
        if idx == len(id_list) - 1:
            query += 'regex (?n1, "' + node_id + '") ) '
        else:
            query += 'regex (?n1, "' + node_id + '") || '

    # second query expression
    query += '?d ?p2 ?n2 ' + 'FILTER ('
    # regular expression for the second node (n2 node)
    for idx, node_id in enumerate(id_list):
        node_id = str(node_id).strip()
        if idx == len(id_list) - 1:
            query += 'regex (?n2, "' + node_id + '") ).'
        else:
            query += 'regex (?n2, "' + node_id + '") || '
    query += ' ?n1 pro:name ?c1. ?n2 pro:name ?c2. ?d pro:name ?c.}'

    return query

# In[ ]:


def find_from_input(id_list, return_type):
    '''
    This function is used to retrun all disease or meds that
    are all connected with the inputs 
    Ex: input = ["s15394", "m12767", "l1087", "m12754"], 
    we want to return disease 
    '''
    if return_type == "dis":
        property_type = "disease"
    elif return_type == "med":
        property_type = "medicine"
    else:
        raise ValueError("Unsupported return type")
    abbraviation = return_type + ":"
    # generate a dummy RDF node
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    query += ' SELECT ?n1 ?r ?n2 WHERE {' + \
        ' ?n1 ?r ?n2. ?n2 pro:type "' + property_type + '". FILTER ('
    for idx, node_id in enumerate(id_list):
        node_id = str(node_id).strip()
        if idx == len(id_list) - 1:
            query += 'regex (?n1, "' + node_id + '") )}'
        else:
            query += 'regex (?n1, "' + node_id + '") || '

    return query


def call_api_rdf3x(request):
    req_content = request

    tmp_file_dir = REQ_FILE_DIR
    tmp_file = tmp_file_dir + "input"
    file_obj = open(tmp_file, "w")
    file_obj.write(req_content)
    file_obj.close()

    api_dir = RDF3X_API_DIR
    dat_dir = KG_DAT_DIR
    dbname = dat_dir + KG_DATABASE
    command_line = api_dir + "rdf3xquery " + dbname + " " + tmp_file
    # print(command_line)

    api_req = os.popen(command_line)
    out_list = api_req.readlines()

    return out_list


def get_node_name(meta_data):
    metas = meta_data
    # print metas
    pre_string = "<http://xianjiaotong.edu/property/"
    if(metas[0] == "<empty result>\n"):
        return ""
    else:
        for i in range(0, len(metas)):
            meta_arr = metas[i].split(" ")
            meta_arr[0] = meta_arr[0].replace(pre_string, "")
            meta_arr[0] = meta_arr[0].replace(">", "")

            if(meta_arr[0] == "name"):
                tname = meta_arr[1].replace("\"", "")
                tname = tname.strip()
                # print tname
                return tname


def get_prefix(fid):
    first = fid[0]
    if first == 'd':
        return "dis"
    elif first == 'l':
        return "lab"
    elif first == 'm':
        return "med"
    elif first == 's':
        return "sym"
    elif first == 'b':
        return "sbody"
    elif first == 'z':
        return "sclass"
    elif first == 'p':
        return "mclass"
    elif first == 'b':
        return "sbody"
    elif first == 'j':
        return "lclass"
    elif first == 'c':
        return "dclass"


def get_cat(cat):
    if cat == "dis":
        return "disease"
    elif cat == "lab":
        return "lab"
    elif cat == "med":
        return "medicine"
    elif cat == "sym":
        return "symptom"
    elif cat == "lclass":
        return "lclass"
    elif cat == "dclass":
        return "dclass"
    elif cat == "mclass":
        return "mclass"
    elif cat == "sclass":
        return "sclass"
    elif cat == "sbody":
        return "sbody"


def get_id(in_link):
    link = in_link.replace("\n", "")
    pre_string = "<http://xianjiaotong.edu/"
    if "disease" in link:
        rem_string = pre_string + "disease/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "lab" in link:
        rem_string = pre_string + "lab/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "medicine" in link:
        rem_string = pre_string + "medicine/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "symptom" in link:
        rem_string = pre_string + "symptom/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "property" in link:
        rem_string = pre_string + "property/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "dclass" in link:
        rem_string = pre_string + "dclass/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "mclass" in link:
        rem_string = pre_string + "mclass/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "sclass" in link:
        rem_string = pre_string + "sclass/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "sbody" in link:
        rem_string = pre_string + "sbody/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")
    elif "lclass" in link:
        rem_string = pre_string + "lclass/"
        link = link.replace(rem_string, "")
        link = link.replace(">", "")

    return link


def get_type(fid):
    if "disease" in fid:
        return "disease"
    elif "lab" in fid:
        return "lab"
    elif "medicine" in fid:
        return "medicine"
    elif "symptom" in fid:
        return "symptom"
    elif "sbody" in fid:
        return "sbody"
    elif "sclass" in fid:
        return "sclass"
    elif "dclass" in fid:
        return "dclass"
    elif "mclass" in fid:
        return "mclass"
    elif "lclass" in fid:
        return "lclass"


def get_nodes_json(center, nodes_list):
    list_nodes = nodes_list
    ret_json = []
    ret_list = []
    check_list = []

    if(list_nodes[0] == "<empty result>\n"):
        return {}
    else:
        # print list_nodes
        for i in range(0, len(list_nodes)):
            data_list = {}
            data_list["data"] = {}

            data_json = {}
            data_json["data"] = {}
            node_arr = list_nodes[i].split(" ")
            node_arr[2] = node_arr[2].replace("\n", "")

            did = get_id(node_arr[1])
            data_list["data"]["source"] = center
            data_list["data"]["target"] = did
            data_list["data"]["selected"] = "false"
            rel = rel_chinese_dic[get_id(node_arr[0])]
            data_list["data"]["relation"] = rel
            ret_list.append(data_list)

            if did not in check_list:
                check_list.append(did)

                data_json["data"]["id"] = did
                nname = node_arr[2].replace("\"", "")
                data_json["data"]["name"] = nname
                data_json["data"]["type"] = get_type(node_arr[1])
                data_json["data"]["center"] = "no"
                ret_json.append(data_json)

    return ret_json, ret_list


def get_edges_json(edges_list):
    list_edges = edges_list
    ret_json = []
    check_list = []

    if(list_edges[0] == "<empty result>\n"):
        return ""
    else:
        # print len(list_edges)
        for i in range(0, len(list_edges)):
            data_json = {}
            data_json["data"] = {}
            edge_arr = list_edges[i].split(" ")
            sid = get_id(edge_arr[0])
            tid = get_id(edge_arr[3])
            rel = get_id(edge_arr[2])
            com = sid + rel + tid
            rcom = tid + rel + sid
            # print com
            if com not in check_list:
                check_list.append(com)
                check_list.append(rcom)
                data_json["data"]["source"] = sid
                data_json["data"]["target"] = tid
                data_json["data"]["selected"] = "false"
                data_json["data"]["level"] = "2"
                data_json["data"]["relation"] = rel_chinese_dic[rel]
                ret_json.append(data_json)

    return ret_json


def get_nodes_edges_json(edges_list):
    list_edges = edges_list
    # print list_edges
    ret_nodes_json = []
    ret_edges_json = []
    edge_check_list = []
    node_check_list = []

    if(list_edges[0] == "<empty result>\n"):
        return ret_nodes_json, ret_edges_json
    else:
        # print len(list_edges)
        for i in range(0, len(list_edges)):
            data_json = {}
            data_json["data"] = {}
            snode_json = {}
            snode_json["data"] = {}
            tnode_json = {}
            tnode_json["data"] = {}

            edge_arr = list_edges[i].split(" ")
            sid = get_id(edge_arr[0])
            sname = edge_arr[1]
            sname = sname.replace("\"", "")
            tid = get_id(edge_arr[3])
            rel = get_id(edge_arr[2])
            tname = edge_arr[4]
            tname = tname.replace("\"", "")
            com = sid + rel + tid
            rcom = tid + rel + sid

            if com not in edge_check_list:
                edge_check_list.append(com)
                edge_check_list.append(rcom)
                data_json["data"]["source"] = sid
                data_json["data"]["target"] = tid
                data_json["data"]["selected"] = "false"
                data_json["data"]["level"] = "1"
                data_json["data"]["relation"] = rel_chinese_dic[rel]
                ret_edges_json.append(data_json)

            if sid not in node_check_list:
                # print sid
                node_check_list.append(sid)
                snode_json["data"]["id"] = sid
                scat = get_prefix(sid)
                scategory = get_cat(scat)
                snode_json["data"]["type"] = scategory
                snode_json["data"]["center"] = "no"
                snode_json["data"]["name"] = sname
                # print sname
                ret_nodes_json.append(snode_json)

            if tid not in node_check_list:
                # print tid
                node_check_list.append(tid)
                tnode_json["data"]["id"] = tid
                tcat = get_prefix(tid)
                tcategory = get_cat(tcat)
                tnode_json["data"]["type"] = tcategory
                tnode_json["data"]["center"] = "no"
                tnode_json["data"]["name"] = tname
                # print tname
                ret_nodes_json.append(tnode_json)

    return ret_nodes_json, ret_edges_json


def get_nodes_list(content_list):
    list_content = content_list
    ret_list = []

    if(list_content[0] == "<empty result>\n"):
        return ret_list
    else:
        # print len(list_content)
        for i in range(0, len(list_content)):
            node_arr = list_content[i].split(" ")
            node_id = get_id(node_arr[2])

            if node_id not in ret_list:
                ret_list.append(node_id)

    return ret_list


@csrf_exempt
@auth_required
def get_node_info(request):  # get the 1-st order link information of a given node
    if request.method == "POST":
        json_out = {}
        tmp_json = {}
        tmp_json["data"] = {}

        try:
            json_elements = {}
            json_nodes = []
            json_edges = []

            input_dict = json.loads(request.body)
            nid = input_dict["NID"]
            cat = get_prefix(nid)
            category = get_cat(cat)
            a = RDF_node(nid, cat)
            ret_nodes_query = a.get_path_one_node()
            # print ret_nodes_query
            out_nodes = call_api_rdf3x(ret_nodes_query)
            # print out_nodes
            (json_nodes, ret_edges) = get_nodes_json(nid, out_nodes)
            # print json_nodes
            # print ret_edges
            ret_name_query = a.get_property()
            out_meta = call_api_rdf3x(ret_name_query)

            tmp_json["data"]["id"] = nid
            tmp_json["data"]["type"] = category
            tmp_json["data"]["center"] = "yes"
            tmp_json["data"]["name"] = get_node_name(out_meta)
            json_nodes.append(tmp_json)

            ret_edges_query = a.get_path_one_node_cross_rel()
            out_edges = call_api_rdf3x(ret_edges_query)
            json_edges = get_edges_json(out_edges)

            # print len(json_edges)
            if json_edges == "":
                json_edges = ret_edges
            else:
                json_edges = json_edges + ret_edges

            json_elements["nodes"] = json_nodes
            json_elements["edges"] = json_edges
            json_out["elements"] = json_elements

            json_out["return"] = 0

        except:
            traceback.print_exc()
            json_out["return"] = 1
            json_out["elements"] = {}

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_diseases_relations(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_element = {}
            json_nodes = []
            json_edges = []
            input_dict = json.loads(request.body)
            id_list = input_dict["DIDS"]
            id_size = len(id_list)

            if (id_size <= 5):
                ret_first_query = get_first_order_rel(id_list)
                ret_second_query = get_second_order_rel(id_list)
            else:
                # ret_first_query = get_first_order_rel(id_list)
                if (id_size <= 10):
                    ret_first_query = get_first_order_rel(id_list)
                else:
                    ret_first_query = get_first_order_rel(id_list[0:10]) # boost the rdf query
                sub_list = id_list[0:5]
                ret_second_query = get_second_order_rel(sub_list)

            out_first_edges = call_api_rdf3x(ret_first_query)
            out_second_edges = call_api_rdf3x(ret_second_query)

            if (out_first_edges[0] == "<empty result>\n"):
                out_edges = out_second_edges
            else:
                out_edges = out_first_edges + out_second_edges

            (json_nodes, json_edges) = get_nodes_edges_json(out_edges)

            json_out["nodes"] = json_nodes
            json_out["edges"] = json_edges
            json_out["return"] = 0

        except:
            json_out["nodes"] = []
            json_out["return"] = 1
            json_out["edges"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_medicines_relations(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_elements = {}
            json_nodes = []
            json_edges = []
            input_dict = json.loads(request.body)
            id_list = input_dict["MIDS"]
            id_size = len(id_list)

            if (id_size <= 5):
                ret_first_query = get_first_order_rel(id_list)
                ret_second_query = get_second_order_rel(id_list)
            else:
                # ret_first_query = get_first_order_rel(id_list)
                if (id_size <= 10):
                    ret_first_query = get_first_order_rel(id_list)
                else:
                    ret_first_query = get_first_order_rel(id_list[0:10]) # boost the rdf query
                sub_list = id_list[0:5]
                ret_second_query = get_second_order_rel(sub_list)

            out_first_edges = call_api_rdf3x(ret_first_query)
            out_second_edges = call_api_rdf3x(ret_second_query)

            if (out_first_edges[0] == "<empty result>\n"):
                out_edges = out_second_edges
            else:
                out_edges = out_first_edges + out_second_edges

            (json_nodes, json_edges) = get_nodes_edges_json(out_edges)

            json_out["nodes"] = json_nodes
            json_out["edges"] = json_edges
            json_out["return"] = 0

        except:
            json_out["nodes"] = []
            json_out["return"] = 1
            json_out["edges"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_diseases_out(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_elements = []
            id_list = []
            return_type = "dis"
            input_dict = json.loads(request.body)

            for key in input_dict.keys():
                tmp_list = input_dict[key]
                for ele in tmp_list:
                    id_list.append(ele)

            ret_query = find_from_input(id_list, return_type)
            out_nodes = call_api_rdf3x(ret_query)
            json_elements = get_nodes_list(out_nodes)

            json_out["elements"] = json_elements
            json_out["return"] = 0

        except:
            json_out["return"] = 1
            json_out["elements"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_diseases_out_v2(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_elements = []
            and_list = []
            not_list = []
            remove_list = []
            return_type = "dis"

            input_dict = json.loads(request.body)
            and_list = input_dict["AND"]
            not_list = input_dict["NOT"]

            and_query = find_from_input(and_list, return_type)
            and_nodes = call_api_rdf3x(and_query)
            and_nodes = get_nodes_list(and_nodes)
            not_query = find_from_input(not_list, return_type)
            not_nodes = call_api_rdf3x(and_query)
            not_nodes = get_nodes_list(not_nodes)

            print not_list
            print and_list
            out_nodes = []
            for nid in and_nodes:
                if nid not in not_nodes:
                    out_nodes.append(nid)

            # print len(out_nodes)
            if len(out_nodes) == 0:
                out_nodes = and_nodes
                remove_list = not_list

            json_out["elements"] = out_nodes
            json_out["return"] = 0
            json_out["remove"] = remove_list

        except:
            json_out["return"] = 1
            json_out["elements"] = []
            json_out["remove"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_medicines_out(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_elements = []
            and_list = []
            not_list = []
            remove_list = []
            return_type = "dis"

            input_dict = json.loads(request.body)
            and_list = input_dict["AND"]
            not_list = input_dict["NOT"]

            and_query = find_from_input(and_list, return_type)
            and_nodes = call_api_rdf3x(and_query)
            and_nodes = get_nodes_list(and_nodes)
            not_query = find_from_input(not_list, return_type)
            not_nodes = call_api_rdf3x(and_query)
            not_nodes = get_nodes_list(not_nodes)

            out_nodes = []
            for nid in and_nodes:
                if nid not in not_nodes:
                    out_nodes.append(nid)

            if len(out_nodes) == 0:
                out_nodes = and_nodes
                remove_list = not_list

            json_out["elements"] = out_nodes
            json_out["return"] = 0
            json_out["remove"] = remove_list

        except:
            json_out["return"] = 1
            json_out["elements"] = []
            json_out["remove"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_medicines_out_v2(request):  # get disease list back
    if request.method == "POST":
        json_out = {}
        try:
            json_elements = []
            and_list = []
            not_list = []
            remove_list = []
            return_type = "med"

            input_dict = json.loads(request.body)
            and_list = input_dict["AND"]
            not_list = input_dict["NOT"]

            and_query = find_from_input(and_list, return_type)
            and_nodes = call_api_rdf3x(and_query)
            and_nodes = get_nodes_list(and_nodes)
            not_query = find_from_input(not_list, return_type)
            not_nodes = call_api_rdf3x(not_query)
            # print not_nodes
            not_nodes = get_nodes_list(not_nodes)

            out_nodes = []
            for nid in and_nodes:
                if nid not in not_nodes:
                    out_nodes.append(nid)

            if len(out_nodes) == 0:
                out_nodes = and_nodes
                remove_list = not_list

            json_out["elements"] = out_nodes
            json_out["return"] = 0
            json_out["remove"] = remove_list

        except:
            json_out["return"] = 1
            json_out["elements"] = []
            json_out["remove"] = []

        return HttpResponse(json.dumps(json_out), content_type="application/json")

################################################################################
##### 症状问疾病
def get_id_name_list(ret):
    idname_list = []
    if 'empty' not in ret[0]:
        for line in ret:
            line = line.strip()
            url,name = line.split()
            name = name.replace('"','')
            xid = url.split('/')[-1].replace('>','')
            idname_list.append({ 'Id':xid,'Name':name })
    return idname_list

def get_type_list(typ):
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    query += '''SELECT DISTINCT ?n1 ?p1    WHERE {
        ?n1 pro:type ?t1 FILTER (regex(?t1,"'''+typ+'''")).
        ?n1 pro:name ?p1 
        }
    '''.replace('\n',' ')
    ret = call_api_rdf3x(query)
    typ_list = get_id_name_list(ret)
    return typ_list

def get_fid_to_nodetype2list(fid,nodetype1,nodetype2):
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    xquery = ''
    # { ?n2 ?r1 dis:'''+dis+'''. ?n2 pro:type ?t2 FILTER (regex(?r1,"Dis")) } 
    query += '''SELECT DISTINCT ?n2 ?p2  WHERE {{
        { '''+nodetype1[:3]+''':'''+fid+''' ?r1 ?n2. ?n2 pro:type ?t2 FILTER (regex(?t2,"'''+nodetype2+'''")) } UNION 
        { ?n2 ?r2 '''+nodetype1[:3]+''':'''+fid+'''. ?n2 pro:type ?t2 FILTER (regex(?t2,"'''+nodetype2+'''")) } 
        }.
        { ?n2 pro:name ?p2}
        }
    '''
    # print query.replace('PREFIX','\nPREFIX').replace('SELECT','\nSELECT')
    query = query.replace('\n','').replace('    ','')
    ret = call_api_rdf3x(query)
    idname_list = get_id_name_list(ret)
    return idname_list

def get_fids_to_nodetype2list(fids,nodetype1,nodetype2):
    dummy_node = RDF_node(None, None)
    query = dummy_node.prefix
    query += '''SELECT DISTINCT ?n0 ?p0  WHERE {\n'''
    for index,fid in enumerate(fids):
        index = index + 1
        query1 = nodetype1[:3]+ ':'+fid+' ?r1 ?n0. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")).\n'
        # query1 = 'sym:'+sid+' ?r ?n0 FILTER (regex(?r,"Dis")).'
        query2 = '?n0 ?r2 '+nodetype1[:3]+':'+fid+'. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")).\n'
        query += '{{'+query1+' } UNION { '+query2+'}}.'
    query += '{ ?n0 pro:name ?p0}}'
    query = query.replace('PREFIX','\nPREFIX')
    query = query.replace('\n',' ')
    ret = call_api_rdf3x(query)
    idname_list = get_id_name_list(ret)
    return idname_list

def get_dis_sym_dict(ret):
    dis_sym_dict = { }
    sidname_dict = { }
    if len(ret) and 'empty' not in ret[0]:
        for line in ret:
            line = line.strip()
            disurl,symurl,symname = line.split()
            symname = symname.replace('"','')
            did = disurl.split('/')[-1].replace('>','')
            sid = symurl.split('/')[-1].replace('>','')
            # idname_list.append({ 'Id':xid,'Name':name })
            if did not in dis_sym_dict:
                dis_sym_dict[did] = []
            dis_sym_dict[did].append(sid)
            sidname_dict[sid] = symname
    return dis_sym_dict, sidname_dict


def get_fids_to_nodetype2all(fids,nodetype1,nodetype2):
    dummy_node = RDF_node(None, None)
    queryprefix = dummy_node.prefix
    queryprefix += '''SELECT DISTINCT ?n ?n0 ?p0  WHERE {\n'''
    fid = fids[0]
    
    query1 = '?n ?r1 ?n0. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")) FILTER (regex(?n,"'+fid+'"))\n'
    query2 = '?n0 ?r2 ?n. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")) FILTER (regex(?n,"'+fid+'"))\n'
    query = '{{'+query1+' } UNION { '+query2+'}'

    for index,fid in enumerate(fids[:-1]):
        # index = index + 1

        query1 = '?n ?r1 ?n0. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")) FILTER (regex(?n,"'+fid+'"))\n'
        query2 = '?n0 ?r2 ?n. ?n0 pro:type ?t0 FILTER (regex(?t0,"'+nodetype2+'")) FILTER (regex(?n,"'+fid+'"))\n'
        query += 'UNION {'+query1+' } UNION { '+query2+'}'

    query += '} .'

    query += '{ ?n0 pro:name ?p0}}'
    query = queryprefix + query
    print query
    query = query.replace('PREFIX','\nPREFIX')
    query = query.replace('\n',' ')
    ret = call_api_rdf3x(query)
    print ret
    dis_sym_dict, sidname_dict = get_dis_sym_dict(ret)
    # dis_sym_dict = get_id_name_list(ret)
    return dis_sym_dict, sidname_dict



def get_bodypart(request):  # get body list back
    if request.method == "GET":
        json_out = {}
        try:
            # input_dict = json.loads(request.GET["q"])
            # json_out['Results'] = get_type_list(input_dict['Type'])
            json_out['Results'] = get_type_list('sbody')
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

def get_symptom_id(request):  # get symptom id
    '''
    # 根据标注数据返回结果的API
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            sname = input_dict['Name']
            body = input_dict.get('Body',None)
            gender = input_dict.get('Gender',None)
            id_score_dict = {id:fuzz.partial_ratio(name,sname) for id,name in id_name_dict.items() if id.startswith('s')}
            id_list = sorted(id_score_dict.keys(), key=lambda name:id_score_dict[name], reverse=True)
            results = []
            for id in id_list:
                if id in symptom_disease_dict:
                    if body and body not in symptom_bodypart_dict[id]:
                        continue
                    if gender and gender not in id_gender_dict[id]:
                        continue
                    tmp_d = { 'Name':id_name_dict[id], 'Id':id }
                    results.append(tmp_d)
            json_out["Return"] = 0
            json_out["num"] = len(results)
            json_out["Results"] = results[:20]
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    '''
    # 根据quyiyuan的API
    if request.method == "GET":
        json_out = {}
        try:
            id_name_dict = json.load(open('/var/www/djangoapi/file/kgInterface/quyiyuan_id_name_dict.json'))
            input_dict = json.loads(request.GET["q"])
            sname = input_dict['Name']
            body = input_dict.get('Body',None)
            gender = input_dict.get('Gender',None)
            id_score_dict = {id:fuzz.partial_ratio(name,sname) for id,name in id_name_dict.items() if id.startswith('s')}
            # print len(id_score_dict)
            id_list = sorted(id_score_dict.keys(), key=lambda name:id_score_dict[name], reverse=True)
            results = []
            for id in id_list:
                tmp_d = { 'Name':id_name_dict[id], 'Id':id }
                results.append(tmp_d)
            json_out["Return"] = 0
            json_out["num"] = len(results)
            json_out["Results"] = results[:20]
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

def get_disease_symptom(posDis,yes_sids,not_sids,unknown_sids):  # get symptom of disease
        json_out = {}
        nodetype1 = 'disease'
        nodetype2 = 'symptom'
        # dis_sym_dict, sidname_dict = get_fids_to_nodetype2all([d['Id'] for d in posDis],nodetype1,nodetype2)
        ### read file
        dids = [d['Id'] for d in posDis]
        with open('/var/www/djangoapi/file/symptom_disease/disease-symptomlist-dict.json','r') as f:
            did_sid_all = json.loads(f.read())
        dis_sym_dict = { did:did_sid_all[did] for did in dids if did in did_sid_all }
        with open('/var/www/djangoapi/file/symptom_disease/symptom-id-name-dict.json','r') as f:
            sid_name_all = json.loads(f.read())
        sidall = set([sid for sids in dis_sym_dict.values() for sid in sids if sid in sid_name_all])
        sidname_dict = { sid:sid_name_all[sid].strip('"') for sid in sidall }
        ### 
        sidall = set(sidname_dict)
        sidall = sidall - yes_sids - not_sids - unknown_sids
        sidall = sorted(sidall)
        numdis = len(posDis)
        numsym = len(sidall)
        matrix = np.zeros([numdis,numsym],dtype=int)
        rowvis = np.ones(numdis,dtype=bool)
        for i,dis in enumerate(posDis):
            did = dis['Id']
            if did in dis_sym_dict:
                sidset = set(dis_sym_dict[did])
            else:
                sidset = set()
            for j,sid in enumerate(sidall):
                if sid in sidset:
                    matrix[i,j] = 1
                    if sid in not_sids:
                        rowvis[i] = False
        # print rowvis.shape
        # print matrix.shape
        matrix = matrix[rowvis]
        # print matrix.shape
        colsum = np.sum(matrix,axis=0) 
        colnum = np.ones(np.size(colsum),dtype=int)*matrix.shape[0]
        coldiff = abs(2*colsum - colnum)
        sidindex = { sid:index for index,sid in enumerate(sidall) }
        sidall = sorted(sidall,key = lambda sid: coldiff[sidindex[sid]])
        sidname_list = [{ 'Id':sid,'Name':sidname_dict[sid]} for sid in sidall if sid not in unknown_sids and colsum[sidindex[sid]] != matrix.shape[0]][:10]
        didname_dict= { }
        for i,dis in enumerate(posDis):
            if rowvis[i]:
                didname_dict[dis['Id']] = dis['Name']
        '''
        sidsel = [sid for sid in sidall if colsum[sidindex[sid]] > 0 and colsum[sidindex[sid]] < matrix.shape[0]]
        ddd = OrderedDict()
        for sid in sidall:
            ddd[sid] = colsum[sidindex[sid]]
        return sidall,sidsel,ddd
        '''
        return sidname_list,didname_dict
        return dis_sym_dict,sidname_dict


def get_dis_list(didlist,prefix=''):
    def myreadjson(fname):
        with open(fname,'r') as f:
            return json.loads(f.read())
    did_name_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/disease-id-name-dict.json')
    dep_name_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/department-id-name-dict.json')
    did_deplist_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/disease-departmentlist-dict.json')
    did_list_new = []
    for did in didlist:
        d = { }
        name = did_name_dict[did]
        deps = did_deplist_dict[did]
        d['Id'] = did
        d['Name'] = prefix + name
        deplist = []
        for dep in did_deplist_dict[did]:
            deplist.append({ 'Id':dep,'Name':dep_name_dict[dep] })
        # d['department'] = deplist
        d['department'] = [dtemp['Id'] for dtemp in deplist]
        did_list_new.append(d)
    return did_list_new
        

def get_dep_dis_list(didlist):
    def myreadjson(fname):
        with open(fname,'r') as f:
            return json.loads(f.read())
    did_name_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/disease-id-name-dict.json')
    dep_name_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/department-id-name-dict.json')
    did_deplist_dict  = myreadjson('/var/www/djangoapi/file/symptom_disease/disease-departmentlist-dict.json')
    didlist = [d['Id'] for d in didlist]
    didlist = didlist[:50]
    dep_did_dict = { }
    for did in didlist:
        for dep in did_deplist_dict[did]:
            try:
                dep_did_dict[dep].append(did)
            except:
                dep_did_dict[dep] = [did]
    dep_list = sorted(dep_did_dict.keys(),key = lambda s:len(dep_did_dict[s]),reverse=True)
    dep_dict_list = []
    for dep in dep_list:
        d = { 'Id':dep,'Name':dep_name_dict[dep] }
        # d['disease'] = [{ 'Id':did,'Name':did_name_dict[did] } for did in dep_did_dict[dep]]
        d['disease'] = dep_did_dict[dep]
        dep_dict_list.append(d)
    return dep_dict_list

            


def get_symptom_disease(request):  # get possible disease of symptom 
    '''
    # 根据标注数据返回结果的API
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            sids = input_dict['Ids']
            not_sids = set(input_dict.get('NotIds',[]))
            gender = input_dict.get('Gender',None)
            did_weight_dict = dict()
            not_did_set = set()
            for sid in not_sids:
                for did in symptom_disease_dict.get(sid,[]):
                    not_did_set.add(did)
            for sid in sids:
                for did in symptom_disease_dict[sid]:
                    if did in not_did_set:
                        continue
                    try:
                        did_weight_dict[did] += 1
                    except:
                        did_weight_dict[did]  = 1
            did_list = sorted(did_weight_dict.keys(), key=lambda did:max(id_degree_dict.values())*did_weight_dict[did]+id_degree_dict[did], reverse=True)
            if gender:
                did_list = [did for did in did_list if gender in id_gender_dict[did]]
            posDis = [{ 'department':disease_department_dict[did],'Id':did,'Name':id_name_dict[did] } for did in did_list if did_weight_dict[did]==len(sids)]
            # 只取交集
            # posDis += [{ 'department':disease_department_dict[did],'Id':did,'Name':'- '+id_name_dict[did] } for did in did_list if did_weight_dict[did]<len(sids)]
            posDep = dict()
            for dis in posDis:
                for dep in dis['department']:
                    if dep not in posDep:
                        posDep[dep] = dict()
                        posDep[dep]['ID'] = dep
                        posDep[dep]['Name'] = id_name_dict[dep]
                        posDep[dep]['disease'] = []
                    posDep[dep]['disease'].append(dis['Id'])
            sid_weight_dict = dict()
            for dis in posDis:
                did = dis['Id']
                for sid in disease_symptom_dict[did]:
                    if sid not in sids:
                        if sid in not_sids:
                            continue
                        try:
                            sid_weight_dict[sid] += 1
                        except:
                            sid_weight_dict[sid]  = 1
            sid_list = sorted(sid_weight_dict.keys(), key=lambda sid:sid_weight_dict[sid], reverse=True)
            posSym = [{ 'Id':sid, 'Name':id_name_dict[sid] } for sid in sid_list]
            allSym = [{ 'Id':sid, 'Name':id_name_dict[sid], 'Weight': sid_weight_dict[sid] } for sid in sid_list]
            json_out["Results"] = dict()
            json_out["Results"]['PosSym'] = posSym[:10]
            json_out["Results"]['AllSym'] = allSym
            json_out["Results"]['PosDis'] = posDis
            json_out["Results"]['PosDep'] = posDep
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    '''
    # 根据quyiyuan的API
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            sids = input_dict['Ids']
            # not_sids = set(input_dict.get('NotIds',[]))
            # gender = input_dict.get('Gender',None)
            id_name_dict = json.load(open('/var/www/djangoapi/file/kgInterface/quyiyuan_id_name_dict.json'))
            symptom_disease_dict = json.load(open('/var/www/djangoapi/file/kgInterface/quyiyuan_symptom_disease_dict.json'))
            accompany_symptom_dict = json.load(open('/var/www/djangoapi/file/kgInterface/quyiyuan_accompany_symptom_dict.json'))
            disease_symptom_dict = dict()
            for sid,did_dict in symptom_disease_dict.items():
                for did in did_dict:
                    try:
                        disease_symptom_dict[did].add(sid)
                    except:
                        disease_symptom_dict[did] = set([sid])
            did_weight_dict = dict()
            not_did_set = set()
            # for sid in not_sids:
            #     for did in symptom_disease_dict.get(sid,[]):
            #         not_did_set.add(did)
            sid_weight_dict = dict()
            for sid in sids:
                for did,score in symptom_disease_dict[sid].items():
                    try:
                        did_weight_dict[did] += score
                    except:
                        did_weight_dict[did] = score
                    if len(sids) == 1:
                        for sid in disease_symptom_dict[did]:
                            if sid in sids:
                                continue
                            try:
                                sid_weight_dict[sid] += 1
                            except:
                                sid_weight_dict[sid] = 1
            did_list = sorted(did_weight_dict.keys(), key=lambda did:did_weight_dict[did], reverse=True)
            posDis = [{ 'department':[],'Id':did,'Name':id_name_dict[did] } for did in did_list]
            sid_list = sorted(sid_weight_dict.keys(), key=lambda sid:sid_weight_dict[sid], reverse=True)
            posSym = [{ 'Id':sid, 'Name':id_name_dict[sid], 'Weight': sid_weight_dict[sid] } for sid in sid_list]

            json_out["Results"] = dict()
            json_out["Results"]['PosSym'] = posSym[:15]
            json_out["Results"]['PosDis'] = posDis[:20]
            json_out["Results"]['PosDep'] = [] # posDep
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")



def get_symptom_id_2(request):  # get symptom id to find medicine
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            sname = input_dict['Name']
            with open('/var/www/djangoapi/file/symptom_drug/symptom_name_id_dict.json','r') as f:
                symptom_name_id_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/sid_didlist_dict.json','r') as f:
                sid_midlist_dict = json.loads(f.read())
            name_score_dict = { name:fuzz.partial_ratio(name,sname) for name in symptom_name_id_dict}
            name_list = sorted(name_score_dict.keys(), key=lambda name:name_score_dict[name], reverse=True)
            results = [ { 'Name':name, 'Id':symptom_name_id_dict[name] } for name in name_list if symptom_name_id_dict[name] in sid_midlist_dict]
            # results = [ { 'Name':name, 'Id':symptom_name_id_dict[name] } for name in name_list ]
            json_out["Return"] = 0
            json_out["num"] = len(symptom_name_id_dict)
            json_out["Results"] = results[:20]
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

def get_symptom_medcine(request):  # get possible disease of symptom 
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            sids = input_dict['Sids']
            # sids_not = input_dict['NotSids']
            tids = input_dict['Tids']
            # tids_not = input_dict['NotTids']
            # age  = input_dict['Age']
            with open('/var/www/djangoapi/file/symptom_drug/symptom_id_name_dict.json','r') as f:
                symptom_id_name_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/drug_id_name_dict.json','r') as f:
                medicine_id_name_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/taboo_id_name_dict.json','r') as f:
                taboo_id_name_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/sid_didlist_dict.json','r') as f:
                sid_midlist_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/did_tidlist_dict.json','r') as f:
                mid_tidlist_dict = json.loads(f.read())
            with open('/var/www/djangoapi/file/symptom_drug/did_sidlist_dict.json','r') as f:
                mid_sidlist_dict = json.loads(f.read())
            # posMed = [{ 'Name':medicine_id_name_dict[mid], 'Id':mid } for sid in sids for mid in sid_midlist_dict[sid] ]
            for tid,name in taboo_id_name_dict.items():
                break
                if tid[1] == '2':
                    name = '患有--' + name
                elif tid[1] == '3':
                    name = '过敏--' + name
                elif tid[1] == '5':
                    name = '同时服用--' + name
                taboo_id_name_dict[tid] = name


            midset = set()
            for sid in sids:
                for mid in sid_midlist_dict[sid]:
                    if len(set(mid_tidlist_dict[mid]) & set(tids)) == 0:
                        # mname = medicine_id_name_dict[mid]
                        midset.add(mid)
            posMed = []
            for mid in midset:
                mname = medicine_id_name_dict[mid]
                posMed.append({ 'Id':mid,'Name':mname })
            # json_out["Results"]['PosSym'] = posSym

            tid_num_dict = { }
            for med in posMed:
                mid = med['Id']
                for tid in mid_tidlist_dict[mid]:
                    try:
                        tid_num_dict[tid] += 1
                    except:
                        tid_num_dict[tid] = 1
            tid_list = sorted(tid_num_dict.keys(), key = lambda tid:abs(tid_num_dict[tid]*2 - len(posMed)))
            tid_list = [tid for tid in tid_list if tid_num_dict[tid] != len(posMed)]
            posTaboo = [{ 'Id':tid, 'Name': taboo_id_name_dict[tid] } for tid in tid_list ]

            sidset = set()
            for med in  posMed:
                mid = med['Id']
                for sid in mid_sidlist_dict[mid]:
                    sidset.add(sid)
            sidset = sidset - set(sids)
            posSym= [{ 'Id':sid, 'Name':symptom_id_name_dict[sid] } for sid in sidset ]

            json_out["Results"] = OrderedDict()
            num = 20


            # 
            posMedNew = []
            mednameset = set()
            for med in posMed:
                medname = med['Name']
                if medname not in mednameset:
                    mednameset.add(medname)
                    posMedNew.append(med)
            json_out["Results"]['PosMed'] = posMedNew
            json_out["Results"]['PosSym'] = posSym[:10]
            json_out["Results"]['PosTaboo'] = posTaboo[:10]
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

################################################################################

################################################################################

