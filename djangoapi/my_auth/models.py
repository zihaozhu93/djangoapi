from django.db import models
# from mongoengine import Document, StringField
# Create your models here.
class User(models.Model):
    # mongodb fs
    # Username = StringField(max_length=8, required=True)
    # Password = StringField(max_length=8, required=True)
    # Token = StringField()
    Username = models.CharField(max_length=8, unique=True, db_index=True)
    Password = models.CharField(max_length=8, unique=False)
    Token = models.CharField(max_length=255, unique=True, db_index=True)
