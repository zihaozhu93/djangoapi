# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.models import User
import json
import traceback
import time
import jwt


def gen_token(uname,upwd):
    claims = {}
    claims['username'] = uname
    claims['upwd'] = upwd
    # claims['time'] = time.time()
    token = jwt.encode(claims,'xjtudlc',algorithm='HS256')
    return token


@csrf_exempt
def register(request):
    if request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            login_user = input_dict['username']
            login_pwd = input_dict['password']
            user_info = User.objects.filter(Username=login_user)
            if not user_info:
                    # no such user
                    n_user = User()
                    token = gen_token(login_user,login_pwd)
                    n_user.Username = login_user
                    n_user.Password = login_pwd
                    n_user.Token = token
                    n_user.save()
                    json_out['return'] = 0
                    json_out['status'] = 'register success'
                    json_out['token'] = token
            else:
                json_out['return'] = 1
                json_out['status'] = 'user already exists'
        except Exception, err:
            traceback.print_exc()
            json_out['return'] = 1
        return HttpResponse(json.dumps(json_out),content_type="application/json")
    

@csrf_exempt
def login(request):
    if request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            login_user = input_dict['username']
            login_pwd = input_dict['password']
            user_info = User.objects.get(Username=login_user)
            if user_info:
                if login_pwd == user_info.Password:
                    # login success
                    token = gen_token(login_user,login_pwd)
                    user_info.Token = token
                    user_info.save()
                    json_out['return'] = 0
                    json_out['status'] = 'login success'
                    json_out['token'] = token
                else:
                    json_out['return'] = 1
                    json_out['status'] = 'wrong user password'
            else:
                json_out['return'] = 1
                json_out['status'] = 'No such user'
        except Exception, err:
            traceback.print_exc()
            json_out['return'] = 1
        return HttpResponse(json.dumps(json_out),content_type="application/json")
   
         
def auth_required(view):
    '''verify the usr token
    :param
    :return
    '''
    def decorators(request, *args, **kwargs):
        # input_dict = json.loads(request.GET["q"])
        json_out = {}
        if 'HTTP_TOKEN' in request.META:
            token = request.META['HTTP_TOKEN']
            try:
                claims = jwt.decode(token,'xjtudlc')
                user_info = User.objects.filter(Username=claims['username']).values()[0]
                if token == user_info['Token']:
                    return view(request, *args, **kwargs)
                else:
                    json_out['return'] = 1
                    json_out['status'] = 'user token verify failed, please relogin'
            except:
                json_out['return'] = 1
                json_out['status'] = 'invalid token'
        else:
            json_out['return'] = 1
            json_out['status'] = 'token missing'
        return HttpResponse(json.dumps(json_out),content_type="application/json")
    return decorators
