from django.db import models

# Create your models here.
class Records(models.Model):
        PID = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        DD = models.DateField(null=True,db_index=False,unique=False)
        FID = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        FVAL = models.FloatField(null=True,db_index=False,unique=False)

class Demograph(models.Model):
        PID = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        GENDER = models.CharField(max_length=4,blank=False,null=False,db_index=False,unique=False)
        YOB = models.IntegerField(blank=False,null=False,db_index=True,unique=False)

class RankPatients(models.Model):
        pid = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        simPid = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        similarity = models.FloatField(null=True,db_index=False,unique=False)
        simLevel = models.IntegerField(blank=False,null=False,db_index=True,unique=False)
        cohort = models.IntegerField(null=True,db_index=False,unique=False)

class RiskPrediction(models.Model):
        CID     = models.IntegerField(null=False,blank=False,unique=False,db_index=True)
        PID     = models.IntegerField(null=False,blank=False,unique=False,db_index=True)
        RiskScore = models.FloatField(null=False,blank=False,unique=False,db_index=True)
        Diseased = models.IntegerField(null=False,blank=False,unique=False,db_index=True)
        class Meta:
               unique_together = (("CID","PID"),)

class FeatureImportance(models.Model):
        CID     = models.IntegerField(null=False,blank=False,unique=False,db_index=True)
        FID     = models.IntegerField(null=False,blank=False,unique=False,db_index=True)
        Importance = models.FloatField(null=False,blank=False,unique=False,db_index=True)
        class Meta:
                unique_together = (("CID","FID"),)
