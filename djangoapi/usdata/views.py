# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.views import auth_required
import json
import csv
import traceback
from datetime import date
import time
from usdata.models import Records, Demograph, RankPatients, RiskPrediction, FeatureImportance
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
# 007 needs the following imports
import logging
import os
import os.path
import time
from collections import OrderedDict
from operator import itemgetter
import copy


##########################################################################
# ycc
##########################################################################
dobject = {'RiskPrediction': RiskPrediction,
           'FeatureImportance': FeatureImportance}
cid_fid_impo_all = {}
with open('/var/www/djangoapi/file/diag_list.json', 'r') as f:
    diag_list = json.loads(f.read())
medication_set = set([])
with open('/var/www/djangoapi/file/cohort_list.json', 'r') as f:
    cohort_list = json.loads(f.read())

##########################################################################
# setup the log
flog = '/tmp/myapp.log'
if os.path.isfile(flog):
    os.remove(flog)

logging.basicConfig(filename=flog, level=logging.INFO)


useless_list = ["", u"", None, False, " ", "  ", "   ", "    ", "     "]

csv_ref = csv.reader(open(
    "/var/www/djangoapi/file/merged_ref.csv", 'rb'), delimiter='\t', quotechar='"')
ref = {}
for row in csv_ref:
    ref[row[0]] = {}
    if row[3] == "DIAGNOSIS" and row[2] == "ProblemList":
        ref[row[0]]["FTYPE"] = "DIAGNOSIS"
        ref[row[0]]["ICD"] = row[1]
        ref[row[0]]["NAMEEN"] = row[6]
        ref[row[0]]["NAMECN"] = unicode(row[7])
        ref[row[0]]["HCC"] = row[10]
        ref[row[0]]["HCC_DES"] = row[11]
        ref[row[0]]["HIERARCHY"] = row[12]
    elif row[3] == "Lab":
        ref[row[0]]["FTYPE"] = "LAB"
        ref[row[0]]["NAMEEN"] = row[4]
        ref[row[0]]["NAMECN"] = unicode(row[5])
        ref[row[0]]["GENDER"] = row[6]
        if row[7].isalpha() == False:
            ref[row[0]]["CLOW"] = float(row[7])
        if row[8].isalpha() == False:
            ref[row[0]]["LOW"] = float(row[8])
        if row[9].isalpha() == False:
            ref[row[0]]["HIGH"] = float(row[9])
        if row[10].isalpha() == False:
            ref[row[0]]["CHIGH"] = float(row[10])
        if row[11] != 'NULL':
            ref[row[0]]["UNIT"] = row[11]
    elif row[3] == "Medication":
        ref[row[0]]["FTYPE"] = "MEDICATION"
        ref[row[0]]["SUBFTYPE"] = row[2]
        ref[row[0]]["NAMEEN"] = row[4]
        ref[row[0]]["NAMECN"] = unicode(row[5])
        ###
        medication_set.add(int(row[0]))
    elif row[3] == "PROCEDURE":
        ref[row[0]]["FTYPE"] = "PROCEDURE"
        ref[row[0]]["CPTCODE"] = row[4]
        ref[row[0]]["NAMEEN"] = row[5]
        ref[row[0]]["NAMECN"] = unicode(row[6])
        ref[row[0]]["RVU"] = row[7]
    elif row[3] == "cpt-facility":
        ref[row[0]]["FTYPE"] = "CPT-FACILITY"
        ref[row[0]]["NAMEEN"] = row[4]
        ref[row[0]]["NAMECN"] = unicode(row[5])
    elif row[3] == "Facility":
        ref[row[0]]["FTYPE"] = "FACILITY"
        ref[row[0]]["NAMEEN"] = row[4]
        ref[row[0]]["NAMECN"] = unicode(row[5])
    elif row[3] == "Doctor":
        ref[row[0]]["FTYPE"] = "DOCTOR"
        ref[row[0]]["LASTNAME"] = row[4]
        ref[row[0]]["FIRSTNAME"] = row[5]
        ref[row[0]]["DIMID"] = row[6]
        ref[row[0]]["LICENSENO"] = row[7]
        ref[row[0]]["DIVISIONNAME"] = row[9]
        ref[row[0]]["LOCATIONNAME"] = row[10]
        ref[row[0]]["REGION"] = row[11]
        ref[row[0]]["CITY"] = row[14]
        ref[row[0]]["STATEZIP"] = row[15][1:]
    else:
        ref[row[0]] = row

##########################################################################
        # reference time dictionary
##########################################################################
with open('/var/www/djangoapi/file/cohort_fid.json', 'r') as f:
    cohort_maps = json.loads(f.read())

##########################################################################
# hierarchy values translated to Chinese
##########################################################################
hierarchy_file = open("/var/www/djangoapi/file/hierarchy_2_chinese.json", 'rb')
hierarchy_2_chinese_dict = json.load(hierarchy_file)
hierarchy_file.close()


def des_usdata_tab(request):
    if request.method == "GET":
        json_out = {}
        json_out["Records"] = {}
        json_out["Records"]["Col_Names"] = Records._meta.get_all_field_names()
        json_out["Records"]["Col_Count"] = len(
            json_out["Records"]["Col_Names"])
        json_out["Demograph"] = {}
        json_out["Demograph"][
            "Col_Names"] = Demograph._meta.get_all_field_names()
        json_out["Demograph"]["Col_Count"] = len(
            json_out["Demograph"]["Col_Names"])
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def records_op(request):
    if request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            u = Records()
            u.PID = input_dict["PID"]
            dd = input_dict["DD"]
            if len(dd) > 1:
                u.DD = date(int(dd[0:4]), int(dd[4:6]), int(dd[6:]))
            u.FID = input_dict["FID"]
            u.FVAL = input_dict["FVAL"]
            u.save()
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def demograph_op(request):
    if request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            u = Demograph()
            u.PID = int(input_dict["PID"])
            u.GENDER = input_dict["GENDER"]
            u.YOB = int(input_dict["YOB"])
            u.save()
            json_out["Return"] = 0
        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


class patient_record:

    def __init__(self):
        self.medication = {}
        self.diagnosis = {}
        self.lab = {}
        self.procedure = {}
        self.medication["Records"] = []
        self.diagnosis["Records"] = []
        self.lab["Records"] = []
        self.procedure["Records"] = []
        self.MinDate = 0


def __lab_check(fv, labdic):
    fv = float(fv)
    if 'CLOW' in labdic:
        clow = float(labdic['CLOW'])
    else:
        clow = -float('Inf')

    if 'LOW' in labdic:
        low = float(labdic['LOW'])
    else:
        low = -float('Inf')

    if 'HIGH' in labdic:
        high = float(labdic['HIGH'])
    else:
        high = float('Inf')

    if 'CHIGH' in labdic:
        chigh = float(labdic['CHIGH'])
    else:
        chigh = float('Inf')

    if fv <= clow:
        return -2
    elif clow < fv <= low:
        return -1
    elif high <= fv < chigh:
        return 1
    elif fv >= chigh:
        return 2
    else:
        return 0
###############################
# zihao


def getReftime(fid_dd_list, cohort_index):
    time_list = []              # store the fid datetime
    for val in fid_dd_list:     # fid_dd_list: A list of fid:datetime dict
        if val.keys()[0] not in cohort_maps[cohort_list[cohort_index]]:
            continue
        else:
            time_list.append(val.values()[0])
    time_list.sort()
    return time_list[0]


@csrf_exempt
@auth_required
def get_sim_patients(request):
    if request.method == "GET":
        json_out = {}
        try:
            # input_dict = json.loads(request.body)
            input_dict = json.loads(request.GET["q"])
            query_pid = unicode(str(input_dict["PID"]), "utf-8")
            query_num = unicode(str(input_dict["TOP"]), "utf-8")
            sim_patients = RankPatients.objects.filter(
                pid=int(query_pid), simLevel__lte=int(query_num)).values()
            cohorts_label = sim_patients.values("cohort").distinct()
            query_pid_fids = {}
            query_pid_fids[query_pid] = []
            for val in Records.objects.filter(PID=int(query_pid)).values():
                query_fid_dd = {}
                if val['DD'] in useless_list:
                    continue
                else:
                    query_fid_dd[val['FID']] = val['DD'].strftime('%Y-%m-%d')
                query_pid_fids[query_pid].append(query_fid_dd)

            sim_pids = [val['simPid'] for val in sim_patients.values("simPid")]
            sim_fids = {}
            for val in sim_pids:
                sim_fids[val] = []
            for val in Records.objects.filter(PID__in=sim_pids).values():
                fid_dd = {}
                if val['DD'] in useless_list:
                    continue
                else:
                    fid_dd[val['FID']] = val['DD'].strftime('%Y-%m-%d')
                sim_fids[val['PID']].append(fid_dd)

            for val in cohorts_label:
                json_out["cohort " + str(val["cohort"])] = {}
                json_out["cohort " + str(val["cohort"])]["Ref_time"] = getReftime(
                    query_pid_fids[query_pid], val["cohort"])
                json_out["cohort " + str(val["cohort"])]["Count"] = 0
                json_out["cohort " + str(val["cohort"])]["Records"] = []

            for idx, patient in enumerate(sim_patients):
                cohortname = "cohort " + str(patient['cohort'])
                records = {}
                records["simPid"] = patient['simPid']
                records["similarity"] = patient['similarity']
                records["level"] = patient['simLevel']
                records["Ref_time"] = getReftime(sim_fids[patient['simPid']],
                                                 patient['cohort'])
                json_out[cohortname]["Count"] += 1
                json_out[cohortname]["Records"].append(records)
            json_out["Return"] = 0

        except:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")

#############################


#########################
# Shiyu's code
def _get_similar_patients(query_pid, k=10):
    '''
    This helper function returns the top k similar patient
    Inputs:
        query_id -- the query PID
        k -- the length of return patient
    Outputs: 
        patients -- a dictionary: key -- the cohort idx, value -- a list of k patients
    '''
    similar_patients = RankPatients.objects.all().filter(
        pid=int(query_pid)).order_by('-simLevel').reverse().values()
    # check how many cohort the query patient has
    num_cohort = _get_num_cohort(similar_patients)
    # construct similarity data of different cohort
    patients = {}
    for idx, similar_patient in enumerate(similar_patients):
        if idx >= k * num_cohort:
            break
        cohort = similar_patient['cohort']
        if cohort in patients:
            patients[cohort].append(similar_patient)
        else:
            patients[cohort] = [similar_patient]
    return patients, num_cohort


def _get_num_cohort(data):
    '''
    This function returns the number of cohort the query patient has
    Inputs:
        data -- a list of dictionary
        e.g. [{'cohort': 1L, 'similarity': 0.125, 'pid': 17185L, 'simLevel': 1L, 'simPid': 345944300L, u'id': 1L}]
    Outputs:
        num_cohort
    '''
    num_cohort = 0
    for idx, datum in enumerate(data):
        next_datum = data[idx + 1]
        if datum['simLevel'] != next_datum['simLevel']:
            break
    num_cohort = idx + 1
    return num_cohort


def _get_detailed_data(pid, gender, records, cohort):
    '''
    This function is used to aquire patient data from database
    Inputs: 
        pid -- the pid number
        records -- a list of dictionary, contains all records for a single patient
        cohort -- the cohort number, this is used to compute the reference time
    Outputs:
        sequences -- the event sequences including diagnosis and medication
        reference_time -- the reference time for sepecific pid
    '''
    reference_time = None
    sequences = []
    for record in records:
        fid = record['FID']
        dd = record['DD']
        # This is very ugly, everybody should be caution, some data DD field is
        # none

        if dd:
            dd = dd.strftime('%Y-%m-%d')
        else:
            continue

        detail_record = None
        if str(fid).strip() in ref:
            # the record could be "diagnosis", "medication" or "procedure"
            detail_record = copy.copy(ref[str(fid).strip()])
            if isinstance(detail_record, dict):
                if detail_record['FTYPE'] == "DIAGNOSIS":
                    # check whether reference time have been computed
                    if (not(reference_time)) and (int(fid) in cohort_maps[cohort_list[cohort]]):
                        reference_time = dd
                    detail_record['FID'] = str(fid)
                    detail_record['DD'] = dd
                    # zihao adds hierarchy_2_chinese_
                    if detail_record['HIERARCHY'] in hierarchy_2_chinese_dict.keys():
                        detail_record['HIERARCHY'] = hierarchy_2_chinese_dict[
                            detail_record['HIERARCHY']]
                    ##
                    sequences.append(detail_record)
        else:
            # meaning it is lab
            if str(fid).strip() + u"A" in ref:
                fid = str(fid) + u"A"
            elif (str(fid).strip() + u"F" in ref) or (str(fid).strip() + u"M" in ref):
                fid = str(fid).strip() + gender.strip()
            else:
                continue
            detail_record = copy.copy(ref[fid])
            detail_record['FID'] = str(fid)
            detail_record['DD'] = dd
            sequences.append(detail_record)

    if not(reference_time):
        logging.info('=======\n no reference time for: %s', pid)
        raise ValueError('Could not find any reference time for paitents')

    return sequences, reference_time


def _create_datum(pid, similarity, demographic, sequences, reference_time):
    '''
    This fucntion returns the datum for a single patient in a desired format
    Inputs:

    Outputs:
          datum
    '''
    similarity_pid = similarity['simPid']
    demographic_pid = demographic['PID']
    if (pid != similarity_pid) or (pid != demographic_pid):
        # logging.info('input pid: %s', pid)
        # logging.info('similarity pid: %s', similarity_pid)
        # logging.info('demographic pid: %s', demographic_pid)
        raise ValueError('mismatched pid in datum creation.')

    datum = {}
    datum['PID'] = pid
    datum['SimValue'] = similarity['similarity']
    datum['Rank'] = similarity['simLevel']
    datum['Gender'] = demographic['GENDER']
    datum['YOB'] = demographic['YOB']
    datum['Sequences'] = sequences
    datum['RefTime'] = reference_time
    return datum


def _get_all_patient_data(patient_similarities, cohort):
    '''
    This function 
    Inputs: 
        patients_information -- a list of dictionary, contains all 
        similarity information of top k patients within a single cohort
        cohort -- the cohort number of this list of patient
    Outputs:

    '''
    # patient_ids = [similarity['simPid'] for similarity in patient_similarities]
    patient_similarity_map = {similarity[
        'simPid']: similarity for similarity in patient_similarities}
    patient_ids = patient_similarity_map.keys()

    # this returns all the data for every patients
    # patient_records = Records.objects.all().filter(PID__in=patient_ids).order_by('PID').values()
    patient_records = Records.objects.all().filter(
        PID__in=patient_ids).order_by('PID', 'DD').values()
    patient_demographics = Demograph.objects.all().filter(
        PID__in=patient_ids).order_by('PID').values()

    pids = []
    data = []
    for idx, record in enumerate(patient_records):
        pid = record['PID']
        # check if this is a new patient
        if pid not in pids:
            if idx == 0:
                records = []
                pids.append(pid)
                records.append(record)
                continue

            demographic = patient_demographics[len(pids) - 1]
            gender = demographic['GENDER']
            if pids[-1] != demographic['PID']:
                raise ValueError('PID mismatches.')
            sequences, reference_time = _get_detailed_data(
                pids[-1], gender, records, cohort)
            similarity = patient_similarity_map[pids[-1]]
            datum = _create_datum(
                pids[-1], similarity, demographic, sequences, reference_time)
            data.append(datum)

            pids.append(pid)
            records = []
        records.append(record)

    # the end of loop, we also need to perform
    demographic = patient_demographics[len(pids) - 1]
    gender = demographic['GENDER']
    if pids[-1] != demographic['PID']:
        raise ValueError('PID mismatches.')
    sequences, reference_time = _get_detailed_data(
        pids[-1], gender, records, cohort)
    similarity = patient_similarity_map[pids[-1]]
    datum = _create_datum(pids[-1], similarity,
                          demographic, sequences, reference_time)
    data.append(datum)

    # sorting data based on the field of rank in ascending order
    data_sorted = sorted(data, key=itemgetter('Rank'))
    return data_sorted


@csrf_exempt
@auth_required
def get_similar_patients_API(request):
    '''
    This is the API to get the data for similar patients
    Inputs:
        Request should contains:
            PID -- in string format
            TOP -- in string format
    '''
    if request.method == "GET":
        json_out = {}
    try:
        # input_dict = json.loads(request.body)
        input_dict = json.loads(request.GET["q"])
        query_pid = unicode(str(input_dict["PID"]), "utf-8")
        k = int((input_dict["TOP"]))
        cohort_similairty_dict, num_cohort = _get_similar_patients(
            query_pid, k)
        data = []
        for cohort_idx in cohort_similairty_dict:
            cohort_data = {}
            cohort_name = cohort_list[cohort_idx]
            patient_similarities = cohort_similairty_dict[cohort_idx]
            similarities = _get_all_patient_data(
                patient_similarities, cohort_idx)
            cohort_data['CohortIdx'] = cohort_idx
            cohort_data['CohortName'] = cohort_name
            cohort_data['Similarities'] = similarities
            data.append(cohort_data)
        json_out['NumCohort'] = num_cohort
        json_out['Data'] = data
        json_out['Return'] = 1
    except:
        traceback.print_exc()
        json_out["Return"] = 0
    return HttpResponse(json.dumps(json_out), content_type="application/json")


#########################

@csrf_exempt
@auth_required
def get_patient_data(request):
    if request.method == "GET":
        json_out = {}
        # this is a set, to keep track the unique fid for each person record
        fid_set = set([])
        try:
            # input_dict = json.loads(request.body)
            input_dict = json.loads(request.GET["q"])
            pid = unicode(str(input_dict["PID"]), "utf-8")
            patient_rec = Records.objects.filter(PID=int(pid)).values()
            records_count = len(patient_rec)
            count_medication = 0
            count_diagnosis = 0
            count_lab = 0
            count_procedure = 0
            total_record = patient_record()
            for idx, record in enumerate(patient_rec):
                fid = record['FID']
                dd = record['DD']
                fval = record['FVAL']
                if str(fid).strip() in ref:
                    # corresponding to the record of "diagnosis", "medication"
                    # or "procedure"
                    detail_record = ref[str(fid).strip()]
                    if isinstance(detail_record, dict):
                        # logging.info('In there: %s, %s, %s', idx, fid, detail_record['FTYPE'])
                        if detail_record['FTYPE'] == "DIAGNOSIS":
                            detail_record['DD'] = dd.strftime('%Y-%m-%d')
                            detail_record['FVAL'] = fval
                            total_record.diagnosis[
                                'Records'].append(detail_record)
                            count_diagnosis += 1
                            # logging.info('%s,%s', type(detail_record['DD']), detail_record)
                        elif detail_record['FTYPE'] == "MEDICATION":
                            detail_record['DD'] = dd.strftime('%Y-%m-%d')
                            detail_record['FVAL'] = fval
                            total_record.medication[
                                'Records'].append(detail_record)
                            count_medication += 1
                        elif detail_record['FTYPE'] == "PROCEDURE":
                            detail_record['DD'] = dd.strftime('%Y-%m-%d')
                            detail_record['FVAL'] = fval
                            total_record.procedure[
                                "Records"].append(detail_record)
                            count_procedure += 1
                        else:
                            records_count -= 1
                    else:
                        # DIAGNOSIS with other format
                        records_count -= 1
                else:
                    # this corresponding to "lab"
                    # case 1: fid + "A"
                    # logging.info('NOT: %s, %s, %s',idx, fid, detail_record['FTYPE'])
                    if str(fid).strip() + u"A" in ref:
                        # for both man and woman
                        fid = str(fid) + u"A"
                    elif (str(fid).strip() + u"F" in ref) or (str(fid).strip() + u"M" in ref):
                        # this is a record depends on the gender
                        # check the gender info for this person
                        patient_info = Demograph.objects.filter(
                            PID=int(pid)).values()
                        gender = patient_info[0]['GENDER']
                        logging.info('%s, %s', fid, gender)
                        fid = str(fid).strip() + gender.strip()
                    else:
                        records_cound -= 1
                        continue
                    detail_record = ref[fid]
                    lab_normal = __lab_check(fval, detail_record)
                    # logging.info('%s, %s, %s', idx, fid, lab_normal)
                    detail_record['DD'] = dd.strftime('%Y-%m-%d')
                    detail_record['FVAL'] = fval
                    detail_record['LAB_NORMAL'] = str(lab_normal)
                    total_record.lab['Records'].append(detail_record)
                    count_lab += 1

            # check condition:
            if count_diagnosis + count_medication + count_procedure + count_lab != records_count:
                raise ValueError('DATA Parsing wrong')

            total_record.diagnosis['Count'] = count_diagnosis
            total_record.medication['Count'] = count_medication
            total_record.procedure['Count'] = count_procedure
            total_record.lab['Count'] = count_lab
            # logging.info('%s', total_record.diagnosis)
            # logging.info('%s, %s, %s, %s',idx, fid, detail_record,type(detail_record))
            json_out["DIAGNOSIS"] = total_record.diagnosis
            json_out["MEDICATION"] = total_record.medication
            json_out["PROCEDURE"] = total_record.procedure
            json_out["LAB"] = total_record.lab
            json_out["Records_Count"] = records_count
            json_out["Return"] = 1
            # traceback.print_exc()
            json_out["LAB"] = total_record.lab
            json_out["Records_Count"] = records_count
            json_out["Return"] = 1
            # traceback.print_exc()
        except:
            traceback.print_exc()
            json_out["Return"] = 0
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def risk_pred_op(request):
    if request.method == "GET":
        json_out = {}
        try:
            json_out["1"] = 0
            input_dict = json.loads(request.GET["q"])
            json_out["2"] = 0
            input_dict = json.loads(request.body)
            json_out["3"] = 0
            table = input_dict['Table']
            filters = input_dict['Filter']
            json_content = {}
            record_info = 'record_info=' + table + '.objects'
            for key in filters:
                record_info = record_info + \
                    '.filter(' + key + + '=' + input_dict[key] + ')'
            exec(record_info)
            if record_info:
                record_info = record_info.values()[0]
                for key in record_info.keys():
                    if record_info[key] not in useless_list:
                        json_content[key] = record_info[key]
                json_out["Results"] = json_content
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'null'
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table']
            input_dict.pop('Table')
            n = dobject[table]()
            for key in input_dict:
                # print key
                setattr(n, key, input_dict[key])
            n.save()
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_risk_score(request):
    if request.method == "GET":
        json_out = {}
        try:
            cid_score_list = []
            input_dict = json.loads(request.GET["q"])
            pid = input_dict['Pid']
            record_info = RiskPrediction.objects.filter(PID=pid)
            if len(record_info):
                for rec in record_info.values():
                    cid = rec['CID']
                    cid = diag_list[cid]
                    score = rec['RiskScore']
                    disease = rec['Diseased']
                    cid_score_dict = {}
                    cid_score_dict['disease'] = cid
                    cid_score_dict['risk_score'] = score
                    cid_score_dict['is_diseased'] = disease
                    cid_score_list.append(cid_score_dict)
                json_out["Return"] = 0
                json_out["Results"] = cid_score_list
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'null'
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_feature_importance(request):
    if request.method == "GET":
        json_out = {}
        try:
            cid_fid_impo_pid = {}
            cid_fid_impo_new = {}
            input_dict = json.loads(request.GET["q"])
            pid = input_dict['Pid']
            record_info = Records.objects.filter(PID=pid)
            fid_set = set([rec['FID'] for rec in record_info.values()])
            record_info = RiskPrediction.objects.filter(PID=pid)
            if len(record_info):
                for rec in record_info.values():
                    cid = rec['CID']
                    cid_fid_impo_pid[cid] = {}
                    if cid not in cid_fid_impo_all:
                        cid_fid_impo_all[cid] = {}
                        fid_impo_info = FeatureImportance.objects.filter(
                            CID=cid)
                        for fid_impo_rec in fid_impo_info.values():
                            fid = fid_impo_rec['FID']
                            impo = fid_impo_rec['Importance']
                            cid_fid_impo_all[cid][fid] = impo
                    #cid_fid_impo_pid[cid]['num'] = len(fid_impo_info)
                    for fid in fid_set:
                        try:
                            if cid_fid_impo_all[cid][fid] > 0.001:
                                cid_fid_impo_pid[cid][
                                    fid] = cid_fid_impo_all[cid][fid]
                        except:
                            pass
                    cid_fid_impo_pid[cid] = cid_fid_impo_pid[cid].items()
                    #cid_fid_impo_pid[cid] = OrderedDict(sorted(cid_fid_impo_pid[cid],cmp=(lambda x,y:1 if x[1]<y[1] else -1)))
                    cid_fid_impo_pid[cid] = dict(
                        sorted(cid_fid_impo_pid[cid], cmp=(lambda x, y: 1 if x[1] < y[1] else -1)))
                # continue
                for cid in cid_fid_impo_pid:
                    cid_new = diag_list[cid]
                    cid_fid_impo_new[cid_new] = []
                    ans = [v for k, v in cid_fid_impo_pid[cid].items()]
                    ans = sum(ans)
                    for fid in cid_fid_impo_pid[cid]:
                        try:
                            fid_new = ref[str(fid)]['NAMECN']
                        except:
                            fid_new = fid
                            pass
                        d = {}
                        d[fid_new] = cid_fid_impo_pid[cid][fid] / ans
                        cid_fid_impo_new[cid_new].append(d)
                for cid in cid_fid_impo_new:
                    cid_fid_impo_new[cid] = sorted(
                        cid_fid_impo_new[cid], key=lambda s: s.values()[0], reverse=True)

                json_out["Return"] = 0
                json_out["Results"] = cid_fid_impo_new
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'null'
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


def get_ref_time(fid_list, cid):
    fid_set = set([int(x) for x in cohort_maps[cohort_list[cid]]])
    for fid, did in fid_list:
        if int(fid) in fid_set:
            return did
    return 0

    pass


@csrf_exempt
@auth_required
def get_sim_pat_med(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            pid = int(input_dict['PID'])
            num = 3
            if 'TOP' in input_dict:
                num = int(input_dict['TOP'])
            cid_pid_set = {}
            pid_cid_set = {}
            pid_sim_dict = dict()
            pid_yob_dict = dict()
            pid_rank_dict = dict()
            pid_gender_dict = dict()
            # cid_rec_dict = {}
            data = []
            sim_pid_info = RankPatients.objects.filter(
                pid=pid).order_by('simLevel').values()
            if len(sim_pid_info):
                for i, rec in enumerate(sim_pid_info):
                    cid = rec['cohort']
                    simpid = rec['simPid']
                    simvalue = rec['similarity']
                    rank = rec['simLevel']
                    pid_sim_dict[simpid] = simvalue
                    pid_rank_dict[simpid] = rank
                    try:
                        pid_cid_set[simpid].add(cid)
                    except:
                        pid_cid_set[simpid] = set([cid])
                    try:
                        cid_pid_set[cid].add(simpid)
                    except:
                        cid_pid_set[cid] = set([simpid])
                pid_info = Demograph.objects.all().filter(
                    PID__in=pid_sim_dict).order_by('PID').reverse().values()
                for rec in pid_info:
                    yob = str(rec['YOB'])
                    # yob = 'YOB'
                    pid = rec['PID']
                    gender = rec['GENDER']
                    pid_yob_dict[pid] = yob
                    pid_gender_dict[pid] = gender
                for cid in cid_pid_set:
                    if cid > 10:
                        continue
                    cohort_dict = {}
                    cohort_dict['CohortIdx'] = cid
                    # json_out['cid']=cid
                    cohort_dict['CohortName'] = diag_list[cid]
                    similarities = []
                    for pid in sorted(list(cid_pid_set[cid]), key=lambda s: pid_sim_dict[s], reverse=True):
                        patient_dict = OrderedDict()
                        patient_dict['PID'] = pid
                        patient_dict['SimValue'] = pid_sim_dict[pid]
                        patient_dict['YOB'] = pid_yob_dict[pid]
                        # patient_dict['YOB'] = ''
                        patient_dict['Rank'] = pid_rank_dict[pid]
                        patient_dict['Gender'] = pid_gender_dict[pid]
                        sequences = []
                        rec_info = Records.objects.all().filter(PID=pid).order_by('PID', '-DD').values()
                        fid_list = []
                        for rec in rec_info:
                            record_dict = {}
                            record_dict['HIERACHY'] = 'MEDICATION'
                            record_dict['FTYPE'] = 'MEDICATION'
                            fid = rec['FID']
                            record_dict['FID'] = str(fid)
                            did = str(rec['DD'])
                            fid_list.append([fid, did])
                            record_dict['HCC'] = ''
                            record_dict['HCC_DES'] = ''
                            try:
                                record_dict['NAMECN'] = ref[str(fid)]['NAMECN']
                            except:
                                record_dict['NAMECN'] = ''
                            try:
                                record_dict['NAMEEN'] = ref[str(fid)]['NAMEEN']
                            except:
                                record_dict['NAMEEN'] = ''
                            record_dict['DD'] = did
                            record_dict['ICD'] = ''
                            if fid in medication_set:
                                sequences.append(record_dict)
                        reftime = get_ref_time(fid_list, cid)
                        if reftime == 0:
                            continue
                        sequences = [
                            x for x in sequences if x['DD'] >= reftime]
                        sequences = sorted(sequences, key=lambda d: d['DD'])
                        patient_dict['RefTime'] = reftime
                        patient_dict['Sequences'] = sequences
                        similarities.append(patient_dict)
                        if len(similarities) >= num:
                            break
                    cohort_dict['Similarities'] = similarities
                    data.append(cohort_dict)
            json_out["Data"] = data
            json_out["Return"] = 0
            json_out["NumCohort"] = len(data)
        except Exception, err:
            traceback.print_exc()
            # json_out["Result"] = cid_rec_dict
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def get_patients(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            cid = input_dict['Cid']
            rec_info = RiskPrediction.objects.filter(
                CID=cid, Diseased=1).values('PID')
            pid_list = list(set([rec['PID'] for rec in rec_info]))[:10]
            json_out["Return"] = 0
            json_out["Results"] = pid_list
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
@auth_required
def modify_cohort(request):
    '''
    Just using for update some cohort fields in table rankpatients.
    '''
    if request.method == "PUT":
        json_out = {}
        try:
            # input_dict = json.loads(request.body)
            table = 'RankPatients'
            xid = '11'
            n_xid = '7'
            n = RankPatients.objects.get(cohort=xid)
            if n:
                setattr(n, 'cohort', n_xid)
                n.save()
                json_out["Return"] = 0
                json_out['Left'] = len(record_info) - 1
            else:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} doesn\'t exist.'.format(
                    table, xid)
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
