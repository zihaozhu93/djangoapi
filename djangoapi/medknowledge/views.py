# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from my_auth.views import auth_required
from medknowledge.models import Disease, Symptom, Lab, Medication, Clinicalpath, Medicare, Clinicalg, Evidence, Video, Otherres, Research, Zhongyi
from medknowledge.models import Fsfile, Content
import json
import traceback
import mongoengine
import msgpack

# from medknowledgedict import medknowledgeDict
from StringIO import StringIO
from collections import OrderedDict
from time import time

from django.conf import settings
es = settings.ES

useless_list = ["", u"", None, False, " "]
Names = locals()
# read file
with open('/var/www/djangoapi/file/medknowledge-dict.json', 'r') as f:
    medknowledgeDict = json.loads(f.read())
    for cls, clsDict in medknowledgeDict.items():
        for k, v in clsDict['dict'].items():
            medknowledgeDict[cls]['dict'][v] = k
            medknowledgeDict[cls]['dict'][v+'Pinyin'] = k+'Pinyin' 
            medknowledgeDict[cls]['dict']['-' + v] = '-' + k
### global variables
pureSet = set(['Ename', 'Icd10', 'Xid',
            '-Ename', '-Icd10', '-Xid'])



def get_content(content):
    if content in useless_list:
        return ''
    return content.split(u'\t')[-1]
    # return content


def get_sortkey(key):
    if key in pureSet:
        return key
    return key + 'Pinyin'


def is_en(s):
    for c in s:
        if ord(c) > 128:
            return False
    return True


def get_filterkey(key, value):
    if not is_en(value) or key in pureSet:
        return key
    return key + 'Pinyin'

###############################################################################
# zzh writes

@csrf_exempt
# @auth_required
def Fsfile_op(request):
    '''
    Args:
    This api makes operations on large files such as images, pdfs or long text
    those stored in mongodb gridfs.
    GET method:
    input: Fid
    outout: iamge or pdfs returns in stream format
    POST method:
    input: Fid, binary stream
    output: 0 if succeeds
    '''
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            if 'Iid' in input_dict:
                input_dict['Table'] = 'Image'
                input_dict['Id'] = input_dict['Iid']
            elif 'Pid' in input_dict: 
                input_dict['Table'] = 'Pdf'
                input_dict['Id'] = input_dict['Pid']
            elif 'Vid' in input_dict: 
                input_dict['Table'] = 'Video'
                input_dict['Id'] = input_dict['Vid']
            table = input_dict['Table']
            file_info = Fsfile.objects(Fid=input_dict['Id']).first()
            if file_info is None:
                json_out["Return"] = 1
                json_out["Results"] = 'File ' + \
                    input_dict["Id"] + ' doesn\'t  exist'
                return HttpResponse(
                    json.dumps(json_out),
                    content_type="application/json")
            else:
                myfile = file_info.fsfile.read()
                if table == 'Pdf':
                    response = HttpResponse(
                        myfile, content_type='application/octet-stream')
                    response['Content-Disposition'] = 'attachment;filename=' + \
                        file_info.Fid + '.pdf'
                elif table == 'Image':
                    response = HttpResponse(
                        myfile, content_type='application/octet-stream')
                    response['Content-Disposition'] = 'attachment;filename=' + \
                        file_info.Fid + '.jpg'
                else:
                    start = 0
                    end = len(myfile)
                    if request.META.has_key('HTTP_RANGE'): 
                        httprange = request.META['HTTP_RANGE']
                        [st,en] = httprange.split('=')[1].split('-')
                        start = int(st)
                        if len(en.strip()):
                            end = int(en)
                    if start == 0:
                        response = HttpResponse(myfile[start:end],content_type='video/mp4')
                    else:
                        response = HttpResponse(myfile[start:end],'video/mp4',206)
                    response['Content-Range'] = 'bytes '+str(start)+'-' + str(end-1) + '/'+ str(end)
                    response['Content-Length'] = str(end- start)
                    response['Accept-Ranges'] = 'bytes'
                    response['Content-Disposition'] = 'attachment;filename='+input_dict['Id'] + '.flv'
                return response
        except:
            traceback.print_exc()
            json_out["Return"] = 1
            return HttpResponse(
                json.dumps(json_out),
                content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = msgpack.loads(request.body)
            file_info = Fsfile.objects(Fid=input_dict["Id"]).first()
            if file_info:
                json_out["Return"] = 1
                json_out["Results"] = 'File' + \
                    input_dict["Id"] + '   already exists'
            else:
                n = Fsfile()
                n.Fid = input_dict['Id']
                n.fsfile = input_dict['Content']
                n.save()
                json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")
    elif request.method == "DELETE":
        json_out = {}
        try:
            input_dict = msgpack.loads(request.body)
            file_info = Fsfile.objects(Fid=input_dict["Id"]).first()
            if file_info:
                # n = image_info[0]
                # w.delete_file(n.Fid)
                # n.delete()
                file_info.delete()
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'File' + \
                    input_dict["Id"] + '   doesn\'t exist'
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")



@csrf_exempt
@auth_required
def disease_tab(request):
    if request.method == "GET":
        json_out = {}
        json_out["Disease"] = {}
        json_out["Disease"]["Col_Names"] = Disease._meta.get_all_field_names()
        json_out["Disease"]["Col_Count"] = len(json_out["Disease"][
            "Col_Names"])
        json_out["Symptom"] = {}
        json_out["Symptom"]["Col_Names"] = Symptom._meta.get_all_field_names()
        json_out["Symptom"]["Col_Count"] = len(json_out["Symptom"][
            "Col_Names"])
        json_out["Lab"] = {}
        json_out["Lab"]["Col_Names"] = Lab._meta.get_all_field_names()
        json_out["Lab"]["Col_Count"] = len(json_out["Lab"]["Col_Names"])
        json_out["Medication"] = {}
        json_out["Medication"][
            "Col_Names"] = Medication._meta.get_all_field_names()
        json_out["Medication"]["Col_Count"] = len(json_out["Medication"][
            "Col_Names"])
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")


@csrf_exempt
# @auth_required
def table_op(request):
    if request.method == "GET":
        json_out = OrderedDict()
        try:
            input_dict = json.loads(request.GET["q"])
            table = input_dict['Table'].title()
            keyMapDict = medknowledgeDict[table]['dict']
            keyList = medknowledgeDict[table]['tablelist']
            contentlist = set(medknowledgeDict[table]['contentlist'])
            json_table = OrderedDict()
            json_content = OrderedDict()
            xid = input_dict['Id']
            fid = xid
            if table == 'Medicare':
                fid = 'me' + fid[1:]
            record_info = 'record_info={:s}.objects.filter(Xid=xid)'.format(
                table)
            content_info = Content.objects(Fid=fid).first()
            exec(record_info)
            if record_info and content_info:
                # mysql
                record_info = record_info.values()[0]
                for key in keyList[2:]:
                    keycn = keyMapDict[key]
                    json_table[keycn] = get_content(record_info[key])
                # mongo
                content_list = json.loads(content_info.Content)
                for d in content_list:
                        for k, v in d.items():
                            # if k == 'temp':
                            #     json_content[''] = v
                            # elif type(v) is unicode:
                            if type(v) is unicode:
                                json_content[k] = [[0, v]]
                                # json_content[k] = v
                                pass
                            else:
                                json_content[k] = v

                json_out["Results"] = OrderedDict()
                json_out["Results"]['Type'] = table
                json_out["Results"]['Name'] = record_info['Name']
                if 'pdf' in contentlist:
                    json_out["Results"]['Pdf'] = xid  # 'pdf/{:s}/'.format(xid)
                if table == 'Video':
                    # 'video/{:s}/'.format(xid)
                    json_out["Results"]['Video'] = xid
                if table == 'Zhongyi':
                    if xid == 'z0000000':
                        pass
                    elif 'overview' in content_list[0]:
                        json_out["Results"][
                            'Overview'] = json_content['overview']
                        # content_list = content_list[1:]
                        json_content.pop('overview')
                    else:
                        json_out["Results"]['Overview'] = {}
                    json_out["Results"]['Id'] = record_info['Xid']
                    json_out["Results"]['Class'] = json_table
                    json_out["Results"]['Table'] = { }
                    json_out["Results"]['Content'] = json_content
                else:
                    json_out["Results"]['Id'] = record_info['Xid']
                    json_out["Results"]['Table'] = json_table
                    json_out["Results"]['Content'] = json_content
                # json_out["Results"]['Content'] = content_list
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'Id doesn\'t exist'
                json_out["Results"] = [bool(record_info), bool(content_info)]
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table'].title()
            keyMapDict = medknowledgeDict[table]['dict']
            # xid=input_dict['Mysql']['Id']
            xid = input_dict['Id']
            fid = xid
            if table == 'Medicare':
                fid = 'me' + fid[1:]
            record_info = 'record_info={:s}.objects.filter(Xid=xid)'.format(
                table)
            exec(record_info)
            if record_info:
                json_out["Return"] = 1
                json_out["Results"] = '{:s}_{:s} already exists'.format(table,
                                                                        xid)
            else:
                # mysql
                n = 'n = {:s}()'.format(table)
                exec(n)
                for key, value in input_dict.items():
                    if input_dict[
                            key] not in useless_list and key in keyMapDict:
                        keyen = keyMapDict[key]
                        setattr(n, keyen, value)
                # mongo
                m = Content()
                m.Fid = fid
                m.Content = input_dict['Content']
                # print type(input_dict['Content'])
                # print type(file2write.getvalue())
                # m.fsfile = input_dict['Content']
                # save at the same time
                m.save()
                try:
                    n.save()
                    json_out["Return"] = 0
                except:
                    m.delete()
                    json_out["Return"] = 1
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")

    elif request.method == "PUT":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table'].title()
            keyMapDict = medknowledgeDict[table]['dict']
            # xid=input_dict['Mysql']['Id']
            xid = input_dict['Id']
            fid = xid
            if table == 'Medicare':
                fid = 'me' + fid[1:]
            record_info = 'record_info={:s}.objects.filter(Xid=xid)'.format(
                table)
            exec(record_info)
            if record_info:
                # mysql
                n = record_info[0]
                for key, value in input_dict.items():
                    if input_dict[
                            key] not in useless_list and key in keyMapDict:
                        keyen = keyMapDict[key]
                        # print keyen
                        setattr(n, keyen, value)
                n.save()
            content_info = Content.objects(Fid=fid).first()
            if content_info:
                content_info.Content = input_dict['Content']
                content_info.save()
            if record_info == None and content_info == None:
                json_out["Return"] = 1
                json_out["Results"] = 'Id doesn\'t exist'
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")

    elif request.method == "DELETE":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            table = input_dict['Table'].title()
            xid = input_dict['Id']
            fid = xid
            content_info = Content.objects(Fid=fid).first()
            if content_info:
                content_info.delete()
            content_info = Content.objects(Fid=fid).first()
            if content_info:
                json_out['xid'] = content_info.Fid
            if table == 'Medicare':
                fid = 'me' + fid[1:]
            record_info = 'record_info={:s}.objects.filter(Xid=xid)'.format(
                table)
            exec(record_info)
            if record_info:
                record_info[0].delete()

            if record_info:
                record_info[0].delete()
                json_out["Return"] = 0
            elif content_info == None:
                # json_out["Return"] = 2
                json_out["Return"] = 1
                json_out["Results"] = 'Id doesn\'t exist'
            else:
                json_out["Return"] = 3
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
# @auth_required
def table_list(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            table = input_dict['Table'].title()
            start = int(input_dict["Start"])
            try:
                offset = int(input_dict["Offset"])
            except:
                offset = int(input_dict["End"]) - start
            keyMapDict = medknowledgeDict[table]['dict']
            keyList = medknowledgeDict[table]['tablelist']
            if 'SortItem' not in input_dict:
                input_dict['SortItem'] = u''
            if len(input_dict['SortItem'].strip()):
                sortItem = get_sortkey(keyMapDict[input_dict['SortItem']])
            else:
                sortItem = 'NamePinyin'
            if 'Sort_item' in input_dict:
                sortItem = input_dict['Sort_item'].title()
            mysql_sel = table + '.objects'
            if 'Filter' in input_dict:
                for k, v in input_dict['Filter'].items():
                    if len(v.strip()) == 0:
                        continue
                    v = ''.join(v.split())
                    ken = get_filterkey(keyMapDict[k], v)
                    if table == 'Zhongyi' and v == 'overview':
                        # print k,v
                        ken = k.title()
                    mysql_sel = mysql_sel + \
                        '.filter(' + ken + '__icontains="' + v + '")'
            id_count = 'id_count=' + mysql_sel + '.count()'
            # print id_count
            # print 
            # print null_count
            exec(id_count)
            if '-' not in sortItem:
                null_count = 'null_count=' + mysql_sel + '.filter('+sortItem.replace('-','')+'=None)' + '.count()'
                exec(null_count)
            else:
                null_count = 0
            # json_out['null_count'] = null_count
            multiple_sortItem = {
                'Yjks': '"-Ejks"',
                'Yjbw': '"-Ejbw"',
                '-Yjks': '"-Ejks"',
                '-Yjbw': '"-Ejbw"'
            }
            if sortItem in multiple_sortItem:
                order_by = '.order_by({:s},{:s}).values()'.format(
                    sortItem, multiple_sortItem[sortItem])
            else:
                # order_by = '.order_by_lower(sortItem).values()'
                order_by = '.order_by(sortItem).values()'
                # order_by = '.order_by(Lower(sortItem)).values()'
                # order_by = '.order_by(Lower(sortItem).desc()).values()'
            records = 'records = ' + mysql_sel + \
                order_by + '[null_count + start:min(null_count+start+offset,id_count)]'
            # json_out['mysql'] = records
            # json_out['sortitem'] = sortItem
            exec(records)
            records_list = [records]
            if '-' not in sortItem and null_count + start + offset  > id_count:
                null_records = 'null_records = ' + mysql_sel + '.filter('+sortItem.replace('-','')+'=None).values()[:min(null_count,null_count+start+offset - id_count )]' 
                exec(null_records)
                records_list.append(null_records)
            results = []
            # results_null = []
            sortItem = sortItem.replace('-', '')
            # result_length = min(id_count - start,offset)
            # pre_start_length = 0
            for records in records_list:
                for record_info in records:
                    if table == 'Zhongyi':
                        record_list = []
                        for key in keyList:
                            if key == 'Xid':
                                keycn = 'Zid'
                            else:
                                keycn = key
                            record_list.append(
                                {keycn: get_content(record_info[key])})
                            # record_dict[keycn] = get_content(record_info[key])
                        results.append(record_list)
                    else:
                        record_dict = OrderedDict()
                        for key in keyList:
                            keycn = key
                            keycn = medknowledgeDict[table]['dict'][key]
                            record_dict[keycn] = get_content(record_info[key])
                        results.append(record_dict)
                    continue
            if len(results) == 0:
                record_dict = OrderedDict()
                for key in keyList:
                    keycn = key
                    keycn = medknowledgeDict[table]['dict'][key]
                    record_dict[keycn] = ''
                results.append(record_dict)
            json_out["Results"] = results
            if table == 'Zhongyi':
                json_out["Total_Zhongyi_Count"] = id_count
                json_out["Zhongyi_Count"] = len(json_out["Results"])
            else:
                json_out["TotalCount"] = id_count
                json_out["Count"] = len(json_out["Results"])
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(
            json.dumps(json_out),
            content_type="application/json")


def pinyinfenge(s):
    s = s.lower()
    yunmu = ['a', 'o', 'i', 'e', 'u']
    shengmu = ['b', 'c', 'd', 'f', 'h', 'j', 'k', 'l',
               'm', 'p', 'q', 'r', 's', 't', 'w', 'x', 'y', 'z']
    pinyin = ''
    vis = 0
    for i in range(len(s)):
        if vis:
            vis = 0
            continue
        if s[i] in shengmu:
            pinyin = pinyin + ' ' + s[i:i + 2]
            vis = 1
        elif s[i] == 'g':
            if s[i - 1] != 'n':
                pinyin = pinyin + ' ' + s[i:i + 2]
                vis = 1
            else:
                pinyin = pinyin + s[i]
        elif s[i] == 'n':
            if s[i - 1] not in yunmu:
                pinyin = pinyin + ' ' + s[i:i + 2]
                vis = 1
            else:
                pinyin = pinyin + s[i]
        else:
            pinyin = pinyin + s[i]
    return pinyin


@csrf_exempt
@auth_required
def table_search(request):
    if request.method == "GET":
        json_out = {}

        search_dict = {
            'Disease': ['Gs', 'Lcbx', 'By', 'Zl', 'Did'],
            'Medication': ['Syz', 'Ylzy', 'Yfyl', 'Blfy', 'Ybsm', 'Mid'],
            'Lab': ['Gs', 'Yl', 'Sj', 'Lid'],
            'Symptom': ['Zs', 'Zzqy', 'Zzxs', 'Yjbw', 'Knjb'],
            'Clinicalpath': ['Class', 'Content'],
            'Medicare': ['Syz', 'Ylzy', 'Zysx', 'Ybsm', 'Ybbxxzlb', 'Ybbxjx']
        }

        try:
            input_dict = json.loads(request.GET["q"])
            query_string = input_dict['QueryString']
            escape_char = '\\/!(){}[]^~*?:'
            for i in range(len(escape_char)):
                query_string = query_string.replace(
                    escape_char[i], '\\' + escape_char[i])
            start_point = input_dict["Start"]
            end_point = start_point + input_dict["Offset"]
            query_pinyin = ''
            try:
                query_pinyin = query_string + ' ' + pinyinfenge(query_string)
            except:
                traceback.print_exc()
            query_size = 200
            # json_out['body'] = {'size': query_size, 'query': {"query_string": {
            #     'fields': ['Name', 'Ename', 'Oname', 'Firstletter'], "query":  query_string}}}

            res1 = es.search(index='medknowledge', doc_type='search',
                             body={'size': query_size, 'query': {"query_string": {'fields': ['Name', 'Ename', 'Oname', 'Firstletter'], "query":  query_string}}})
            res2 = es.search(index='medknowledge', doc_type='search',
                             body={'size': query_size,  'query': {"query_string": {'fields': ['Pinyin', 'Firstletterspace'], "query":  query_pinyin}}})
            res3 = es.search(index='medknowledge', doc_type='search', body={'size': query_size, 'query': {
                             "bool": {"must": {"query_string": {"query": query_string}}}}})
            res4 = es.search(index='medknowledge', doc_type='search',
                             body={'size': query_size, 'query': {"match_phrase": {"Firstletterspace": {"query": ' '.join(query_string) + ' '}}}})
            res5 = es.search(index='medknowledge', doc_type='search',
                             body={'size': query_size, 'query': {"query_string": {'fields': ['Did', 'Sid', 'Mid', 'Lid'], "query":  query_string}}})
            answers = res1['hits']['hits'] + res2['hits']['hits'] + \
                res3['hits']['hits'] + \
                res4['hits']['hits'] + res5['hits']['hits']
            for i, answer in enumerate(answers):
                try:
                    firstletter = answer['_source']['Firstletter']
                    if firstletter.startswith(query_string.lower().strip()):
                        answers[i][
                            '_score'] += min(4, 0.25 * pow(2.0, len(query_string.strip())))
                except:
                    pass

            nodegree_n = 5
            answers = sorted(answers, cmp=(lambda x, y: 1 if x[
                             '_score'] < y['_score'] else -1))
            try:
                answers[nodegree_n:] = sorted(answers[nodegree_n:], cmp=(
                    lambda x, y: 1 if x['_source']['Weight'] < y['_source']['Weight'] else -1))
            except:
                pass
            new_answers = []
            id_set = set([])
            for i, answer in enumerate(answers):
                tempd = answer['_source']
                table = tempd['Table']
                new_answer = OrderedDict()
                new_answer['Con'] = OrderedDict()
                new_answer['Var'] = OrderedDict()
                for k in search_dict[table]:
                    if k in tempd and tempd[k] not in useless_list:
                        new_answer['Var'][k] = tempd[k]
                    if len(new_answer['Var']) > 1:
                        break
                if len(new_answer) == 0:
                    continue
                ansid = tempd[table[0] + 'id']
                new_answer['Con']['Id'] = ansid
                new_answer['Con']['Table'] = table
                new_answer['Con']['Name'] = tempd['Name']
                ansid = table + ansid
                if ansid not in id_set:
                    id_set.add(ansid)
                    new_answers.append(new_answer)
            json_out["Results"] = new_answers[start_point:end_point]
            json_out["Count"] = len(new_answers)
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            es.index(index="medknowledge", doc_type="search",
                     id=input_dict['id'], body=input_dict['body'])
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")


@csrf_exempt
# @auth_required
def name_search(request):
    if request.method == "GET":
        json_out = {}
        try:
            input_dict = json.loads(request.GET["q"])
            query_string = input_dict['QueryString']
            escape_char = '\\/!(){}[]^~*?:'
            for i in range(len(escape_char)):
                query_string = query_string.replace(
                    escape_char[i], '\\' + escape_char[i])

            query_pinyin = ''
            try:
                query_pinyin = pinyinfenge(query_string)
            except:
                pass
            res1 = es.search(index='medknowledge', doc_type='auto',
                             body={'query': {"query_string": {'fields': ['Name', 'Firstletter'], "query":  query_string}}})
            res2 = es.search(index='medknowledge', doc_type='auto',
                             body={'query': {"query_string": {'fields': ['Pinyin'], "query":  query_pinyin}}})
            res3 = es.search(index='medknowledge', doc_type='auto',
                             body={'query': {"match_phrase": {"Firstletterspace": {"query": ' '.join(query_string) + ' '}}}})
            answers = res1['hits']['hits'] + \
                res2['hits']['hits'] + res3['hits']['hits']
            # print len(res3['hits']['hits'])
            # print len(answers)
            for i, answer in enumerate(answers):
                try:
                    firstletter = answer['_source']['Firstletter']
                    if firstletter.startswith(query_string.lower().strip()):
                        answers[i][
                            '_score'] += min(4, 0.25 * pow(2.0, len(query_string.strip())))
                except:
                    pass
            answers = sorted(answers, cmp=(lambda x, y: 1 if x[
                             '_score'] < y['_score'] else -1))
            name_list = []
            nodegree_n = 3
            answers[nodegree_n:] = sorted(answers[nodegree_n:], cmp=(
                lambda x, y: 1 if x['_source']['Weight'] < y['_source']['Weight'] else -1))
            for answer in answers:
                name = answer['_source']['Name']
                if name not in name_list:
                    name_list.append(name)
                    # print name,answer['_score']
            if len(answers):
                json_out["Results"] = name_list[:10]
                json_out["Return"] = 0
            else:
                json_out["Return"] = 1
                json_out["Results"] = 'There is not matched items'
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
    elif request.method == "POST":
        json_out = {}
        try:
            input_dict = json.loads(request.body)
            es.index(index="medknowledge", doc_type="auto",
                     id=input_dict['id'], body=input_dict['body'])
            json_out["Return"] = 0
        except Exception, err:
            traceback.print_exc()
            json_out["Return"] = 1
        return HttpResponse(json.dumps(json_out), content_type="application/json")
# 202.117.54.88:8000/auto/?q={"Query_string":"gaoxueya"}

