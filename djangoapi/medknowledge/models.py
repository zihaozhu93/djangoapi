from django.db import models
from mongoengine import Document, StringField, FileField

class Disease(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Icd10 = models.CharField(max_length=32, null=True, blank=True, unique=False, db_index=True)
    Ename = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    Oname = models.CharField(max_length=2048, null=True, blank=True, unique=False, db_index=False)
    OnamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Dclass = models.CharField(max_length=64, null=True, blank=True, unique=False, db_index=True)
    DclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Symptom(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Yjbw = models.CharField(max_length=128, null=True, blank=False, unique=False, db_index=True)
    YjbwPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Ejbw = models.CharField(max_length=128, null=True, blank=False, unique=False, db_index=True)
    EjbwPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Yjks = models.CharField(max_length=128, null=True, blank=False, unique=False, db_index=True)
    YjksPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Ejks = models.CharField(max_length=128, null=True, blank=False, unique=False, db_index=True)
    EjksPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Medication(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Ename = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    Oname = models.CharField(max_length=2048, null=True, blank=True, unique=False, db_index=False)
    OnamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Fclass = models.CharField( max_length=128, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Lab(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Ename = models.CharField(max_length=64, null=True, blank=True, unique=False, db_index=True)
    Oname = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    OnamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Fclass = models.CharField( max_length=128, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Clinicalpath(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=True, blank=True, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Year = models.CharField(max_length=64, null=True, blank=True, unique=False, db_index=True)
    YearPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    ClassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Medicare(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=256, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Ename = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    Oname = models.CharField(max_length=256, null=True, blank=True, unique=False, db_index=True)
    OnamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Fclass = models.CharField( max_length=128, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Ybbxjx = models.CharField( max_length=256, null=True, blank=True, unique=False, db_index=True)
    YbbxjxPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Research(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Ename = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    Oname = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    OnamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Fclass = models.CharField(max_length=32, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Icd10 = models.CharField(max_length=32, null=True, blank=True, unique=False, db_index=True)


class Video(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Fclass = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Clinicalg(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Fclass = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Evidence(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Fclass = models.CharField( max_length=128, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Jbyj = models.CharField(max_length=128, null=True, blank=True, unique=False, db_index=True)
    JbyjPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Zz = models.CharField(max_length=1024, null=True, blank=True, unique=False, db_index=False)
    ZzPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Cc = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    CcPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Otherres(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Fclass = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    FclassPinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)


class Zhongyi(models.Model):
    Xid = models.CharField(max_length=8, null=False, blank=False, unique=True, db_index=True)
    Name = models.CharField(max_length=255, null=False, blank=False, unique=False, db_index=True)
    NamePinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    ###
    Class1 = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class1Pinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class2 = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class2Pinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class3 = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class3Pinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class4 = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class4Pinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class5 = models.CharField( max_length=255, null=True, blank=True, unique=False, db_index=True)
    Class5Pinyin = models.CharField(max_length=255, null=True, blank=True, unique=False, db_index=True)



class Fsfile(Document):
    # mongodb fs
    Fid = StringField(max_length=8, required=True)
    fsfile = FileField()

class Content(Document):
    Fid = StringField(max_length=8, required=True)
    Content = StringField()
